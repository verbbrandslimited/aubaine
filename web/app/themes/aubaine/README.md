# Aubaine theme

## Getting started

### Set up the virtual host

Please replace any lines that show `{handle}` with `aubaine`.

Set up your environment by creating the virtual host. For local machines
running on the Hex stack simply run the checkout shell script in the tools
repository. You will be prompted to type in the handle name:

    sudo sh ~/Sites/tools/checkout.sh

### Install WordPress

The next step is to install the latest version of WordPress. We can do this
quickly and easily using another shell script in the tools repository. Again,
you will be prompted to type in the WordPress handle name. Ensure you change
directory into the virtual host as the shell script installs WordPress in the
current directory:

    cd ~/Sites/aubaine
    sh ~/Sites/tools/wordpress.sh

### Download and install the Aubaine theme

Next, change directory into the theme directory:

    cd wp-content/themes/aubaine

To clone the skeleton theme repository, run the command below:

    git clone git@github.com:hex-digital/aubaine.git .

Ensure the line ends with a . so it does not create an extra folder.

### Install dependencies

Now we have the codebase, we'll need to install our dependencies. To install
Node.js dependencies using Node Package Manager (npm), simply run:

    npm install

This command will read the package.json file in the current directory and
install all the dependencies listed for this project.

Next we will install our bower components for front end libraries such as
jQuery, Modernizr and InuitCSS.

    bower install

>If bower has not already been installed, you'll first need to run:
>
>*npm install -g bower*

### Plugins and uploads

Ask a lead developer to download the latest `/plugins` and `/uploads` directories
for you. Once added, double check all required plugins are enabled.

### ACF

First get the migrations executable.

    curl -o acf-migrations.phar https://raw.githubusercontent.com/hex-digital/acf-migrations/master/acf-migrations.phar

To generate the ACF `export.php` code, run

    php acf-migrations.phar -t ~/Sites/aubaine/wp-content/themes/aubaine

### Permalink setup

The Aubaine website uses a custom URL structure. In the Wordpress backend, navigate to
the permalinks settings page `Settings -> Permalinks`.

Check `Custom Structure` and add the following in the input field.

    /blog/%postname%/

Then the following in `Category base` and `Tag base` respectively

    blog/category
    blog/tag

### Adding the database

Ask a lead developer to download the latest `SQL` file dump file of the production
server.

In a terminal window, login to your local mysql server.

    mysql -u <username> -p

Then make sure you are using the Aubaine database.

    use aubaine_local

Now import the database.

    source /path/to/sqlfile

Once that is complete, exit mysql.

    exit

You'll now need to change the url paths within the newly added database as it
will still be https://aubaine.co.uk. Run

    sudo sh ~/Sites/tools/change-url.sh

When prompted, enter the following:

    Database name: aubaine_local
    Old URL: https://aubaine.co.uk
    New URL: http://aubaine.local

## Contributing

Before making any updates to this repository, first you will need to checkout a
new branch based on the update you're carrying out. It will fall into one of
the following categories:

* Feature
* Change
* Hotfix

Simply create your branch name with one of these categories prefixed. As an
example, let's say we needed to create a new feature. Firstly, we'll need to
checkout the base branch. In this case, this would be the development branch.
In other cases, this may be the production branch if we're fixing a bug and
work is currently being carried out on the development branch that hasn't been
deployed yet:

    git checkout development

Then we can go ahead and create our new branch:

    git checkout -b feature-1

Where 1 would relate to the ID of a specific task via the Hex Reporting
Platform. Once you've completed your changes, you can go ahead and push your
branch to GitHub:

    git push origin feature-1

Before we can merge this back into the base branch, we need to submit a
pull-request. You can do this via [GitHub](https://github.com) but we can also
request this via the command line using `hub`.

Then, go ahead and submit the pull-request like so:

    hub pull-request -b development -m "My pull-request description"

Once approved, the request will be merged and the branch deleted.

## Gulp.js

### Running the default gulp task

When creating or changing images, Sass or JavaScript files, you must ensure your
changes are being made in the resource directory. You can run Gulp.js to
compile all the Sass, minify the JavaScript and compress the images. Simply run:

    gulp

via the command line to run through the compiling process. After the process is
complete, the script will finish with a watch task and continue to watch changed
files for compilation.

>If gulp gives an error similar to `Error: Cannot find module './../../../credentials.json'`
>then run the following code: `echo "{}" > ./../../../credentials.json`

### Updating dependencies

When updating or adding any new npm or bower dependencies, be sure to add the
`--save` flag to the command. For example, if we wanted to install the
`gulp-cssnano` npm dependency, we should run:

    npm install --save gulp-cssnano

Doing this will update the relevant json file (for Bower, the bower.json file
and for npm, the package.json file).

### Live Reload

When using Gulp, you'll notice in the gulpfile we have the live reload
plugin available. To take advantage of this and speed up your workflow,
install the Live Reload Chrome extensions via this link:

https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=en-US

Once activated, this extension will automatically reload a page for you upon saving and
compiling a file.
