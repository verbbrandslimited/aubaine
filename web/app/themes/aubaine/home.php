<?php
/**
 * The template for displaying all blog posts.
 *
 * @package Hex Digital
 * @subpackage Aubaine
 * @since 2017
 *
 */

get_header() ?>

<div class="o-wrapper">

    <?php $current_panel = 1 ?>
    <?php $panel_groups = [2,3,2] ?>
    <?php $last_panel = false ?>
    
    <?php foreach ( $panel_groups as $panel_count ) : ?>
        <?php if ( $last_panel ) break ?>
        <section class="o-layout-block">
            <?php for ( $i = 0; $i < $panel_count; $i++ ) : ?>
                <?php if ( have_posts() && false === $last_panel ) : ?>
                    <?php the_post() ?>
                    <?php $panel = generate_blog_panel_array( $current_panel ) ?>
                <?php else : ?>
                    <?php $panel = generate_empty_blog_panel_array( $current_panel ) ?>
                    <?php $last_panel = true ?>
                <?php endif ?>
                <?php include get_partials_directory_uri() . '/panel.php' ?>
                <?php $current_panel++ ?>
            <?php endfor ?>
        </section>
    <?php endforeach ?>

    <div class="c-pagination">
        <?= paginate_links( [ 'prev_next' => false ] ) ?>
    </div>

</div>

<?php get_footer() ?>
