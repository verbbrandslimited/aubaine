<form id="fishbowl-subscribe" class="subscribe-page--form c-form" action="<?php echo esc_attr( "http://${args['hostname']}.fbmta.com/members/subscribe.aspx" ); ?>">
  <p>Fill in your details below to enjoy 20% off your next visit, birthday treats and exclusive offers.</p>
  <p><em>NOTE: Fields marked with an asterisk (*) are required.</em></p>
  <div class="input-wrapper">
    <input type="text" class="c-form__input" name="FirstName" id="subscribe-FirstName" placeholder="<?php echo esc_attr( __( 'First Name*', 'aubaine') ); ?>">
  </div>

  <div class="input-wrapper">
    <input type="text" class="c-form__input" name="LastName" id="subscribe-LastName" placeholder="<?php echo esc_attr( __( 'Last Name*', 'aubaine') ); ?>">
  </div>

  <div class="input-wrapper">
    <input type="email" class="c-form__input" name="EmailAddress" id="subscribe-EmailAddress" id="EmailAddress" maxlength="200" placeholder="E-mail Address*">
  </div>

  <div id="subscribe-birthdate-container" class="c-form__field c-form__field--color-white input-wrapper u-margin-bottom-none">
    <div class="custom-placeholder">
      <input type="text" class="c-form__input u-text-center" name="Birthdate" id="subscribe-Birthdate" placeholder="Date of Birth* (DD/MM) so we can send you a treat">
      <span class="custom-placeholder__placeholder">
        Date of Birth* (DD/MM) <i>so we can send you a treat</i>
      </span>
    </div>
  </div>

  <div class="input-wrapper js-dropdown">
    <select name="StoreCode" id="subscribe-StoreCode" class="c-form__select" title="Your Local Aubaine">
      <option value=""><?php echo esc_html( __( 'Your Local Aubaine*', 'aubaine' ) ); ?></option>
      <option value="SLF">Aubaine Selfridges</option>
      <option value="MLB">Aubaine Marylebone</option>
      <option value="MFR">Aubaine Dover Street</option>
      <option value="KHS">Aubaine Kensington</option>
      <option value="IFR">Aubaine Institut Français</option>
      <option value="HYD">Aubaine Hyde Park</option>
      <option value="HDN">Aubaine Heddon Street</option>
      <option value="GEN">Aubaine - General</option>
      <option value="CVG">Aubaine Deli </option>
      <option value="BRM">Aubaine Brompton Cross</option>
      <option value="BGC">Aubaine Broadgate Circle</option>
    </select>
  </div>

  <div class="input-wrapper">
    <label for="IsEmployee">
      <input type="checkbox" name="IsEmployee" id="subscribe-IsEmployee" value="true">
      <?php echo esc_html( __( 'Are you an Aubaine employee?', 'aubaine' ) ); ?>
    </label>
  </div>

  <div class="input-wrapper">
    <label for="Privacy">
      <input type="checkbox" name="Privacy" id="subscribe-Privacy" value="true">
      <span>I agree to the storing and processing of my data as outlined by Aubaine’s <a href="<?= home_url('/privacy-policy'); ?>">Privacy Policy</a></span>
    </label>
  </div>

  <div class="input-wrapper">
    <label for="ConfirmSubscribe">
      <input type="checkbox" name="ConfirmSubscribe" id="subscribe-ConfirmSubscribe" value="true">
      <?php echo esc_html( __( 'Yes please, I’d like to hear about the latest news from Aubaine', 'aubaine' ) ); ?>
    </label>
  </div>

  <input type="hidden" name="GDPR_ReOptIn" id="subscribe-GDPR_ReOptIn" value="false">

  <?php do_action( 'fishbowl_subscribe_before_submit', $args ); ?>

  <button type="submit" class="c-btn c-btn--light"><?php echo esc_html( __( 'Sign up Now', 'aubaine' ) ); ?></button>
</form>