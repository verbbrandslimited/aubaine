function Peek() {
    var self = this;
    /**
     * Initialise the object
     */
    function init() {
        instantiateObjects();
        setInitialPageHref();
        addHandlers();
    }
    /**
     * Set up the initial classes to use across the peek component
     */
    function instantiateObjects() {
        self.peekObject = 'js-peek';
        self.peekVisible = 'c-peek--visible';
        self.peekPage = 'c-peek__page';
        self.peekPageActive = 'c-peek__page--active';
        self.peekLinkParent = 'js-peek-link';
        self.peekLink = self.peekLinkParent + ' a';
        self.peakCloseButton = 'c-peek__close';
        self.hamburger = 'c-hamburger';
        self.hamburgerModifier = 'c-hamburger--active c-hamburger--peek-close ' + self.peakCloseButton;
        self.navbar = 'c-navbar';
        self.navbarModifier = 'c-navbar--menu c-navbar--peek';
        self.html = 'html';
        self.htmlFixed = 'is-fixed';
        self.activeLink = 'c-peek__active-link';
    }
    /**
     * Sets the href of page on load
     */
    function setInitialPageHref() {
        self.initialPage = window.location.href;
    }
    /**
     * Bind event handlers for the peek component
     */
    function addHandlers() {
        $(document).on('click.peek', '.' + self.peekLink, function(event) {
            if (!peekPageExists($(this).attr('href'))) return true;
            event.preventDefault();
            closePeekPage();
            
            if ($(this).attr('data-ot') && (ot = $('[data-value="' + $(this).attr('data-ot') + '"]')).length) {
	            ot[0].click();
            }
            
            openPeek($(this).attr('href'));
            updateUrl($(this).attr('href'));
        });
        // The following click is set to the document as the hamburger doesn't
        // have the peek close class on load. It's added once a peek page is open
        $(document).on('click.peek', '.' + self.peakCloseButton, function() {
            closeAll();
            updateUrl(self.initialPage);
        });
        $(document).on('keyup.peek', function(event) {
            if (event.keyCode == 27) {
                closeAll();
                updateUrl(self.initialPage);
            }
        });
        window.addEventListener('popstate', function(e) {
          var pageUrl = e.state;
          if (pageUrl === '/' || pageUrl === null) {
              closeAll();
          } else {
              if (!peekPageExists(pageUrl)) return true;
              closePeekPage();
              openPeek(pageUrl);
          }
        });
    }
    function updateUrl(pageUrl) {
        history.pushState(pageUrl, null, pageUrl);
    }
    function get_data_tag(pageUrl) {
        return '[data-page="' + window.sanitiseUrl(pageUrl) + '"]';
    }
    function peekPageExists(pageUrl) {
        return ($('.' + self.peekPage + get_data_tag(pageUrl)).length);
    }
    function openPeek(pageUrl) {
        openPeekPage(pageUrl);
        toggleNav('open');
        toggleFixedPage('fix');
        toggleLink('active', pageUrl);
    }
    function openPeekPage(pageUrl) {
        $('.' + self.peekObject)
            .addClass(self.peekVisible)
            .find('.' + self.peekPage + get_data_tag(pageUrl))
                .addClass(self.peekPageActive);
    }
    function closeAll() {
        closePeekPage();
        toggleNav('close');
        toggleFixedPage('unfix');
        toggleLink('unactive');
    }
    function closePeekPage() {
        $('.' + self.peekObject).removeClass(self.peekVisible);
        $('.' + self.peekPage).removeClass(self.peekPageActive);
    }
    function toggleNav(state) {
        state = ('open' === state);
        $('.' + self.navbar).toggleClass(self.navbarModifier, state);
        $('.' + self.hamburger).toggleClass(self.hamburgerModifier, state);
    }
    function toggleFixedPage(state) {
        $(self.html).toggleClass(self.htmlFixed, ('fix' === state));
    }
    function toggleLink(state, pageUrl) {
        $('.' + self.peekLinkParent).removeClass(self.activeLink);
        if ('active' === state) {
            $('.' + self.peekLinkParent + ' a[href*=' + window.sanitiseUrl(pageUrl) + ']')
                .closest('.' + self.peekLinkParent)
                    .addClass(self.activeLink);
        }
    }
    init();
}
