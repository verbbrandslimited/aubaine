// @TODO Use history replace when changing dropdown or buttons of menu
function Menu() {
    var self = this;

    /**
     * Initialise the object
     */
    function init() {
        instantiateObjects();
        setupArcData();
        addHandlers();
    }

    /**
     * Set up the initial classes to use across the menu object
     */
    function instantiateObjects() {
        self.menuObject = 'js-menu-page';
        self.locations = 'c-menu-page__menu-locations';
        self.menuLocation = 'c-menu-location';
        self.menuNames = 'c-menu-page__menu-names';
        self.menuNameItems = 'c-menu-name';
        self.menuNameActive = 'c-btn--active';
        self.menuBlock = 'c-menu-page__menu-block';
        self.menus = 'c-menu';
        self.menusActive = 'c-menu--active';
    }

    function setMenuSelectionDEV(menuObject) {
        var locationTag = getLocationTag(menuObject),
            menuTag = getMenuTag(menuObject);
        console.log(locationTag);
        console.log(menuTag);
        $('.' + self.menuNames + locationTag)
            .children().children().children().children('.' + self.menuNameItems + menuTag)
            .addClass(self.menuNameActive);
        $('.' + self.menus + locationTag + menuTag).addClass(self.menus + '--active');
        updateMenuQueryParam(menuObject.location, menuObject.name);
    }


    // ============================
    // popup section on Menu page
    //=============================
    $(function () {
        var triggered_times = 0,
            buffer = 100;

        $(window).scroll(function () {
            var $menuBlock = $('.c-menu-page__menu-block--active'),
                // This scroll trigger is for the bottom of the menu block
                // scrollTrigger = $menuBlock.offset().top + $menuBlock.height() - viewPortHeight() - buffer,
                scrollTrigger = 200,
                hasTriggered = triggered_times >= 1;

            if ($(window).scrollTop() >= scrollTrigger && !hasTriggered) {
                $('.popup-section').addClass("active");
                triggered_times++;
            }
        });

        // Closing the Popup Box
        $(".close-button").click(function () {
            $(".popup-section").removeClass("active");
        });
    });

    function viewPortHeight() {
        return Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    }

    /**
     * Setup the initial data array to store which menu headers have been arced
     */
    function setupArcData() {
        self.arcHeaders = {};
        var activeMenu = $('.' + self.menusActive);
        if (activeMenu.length) {
            var locationObject = {
                'location': activeMenu.data('location'),
                'name': activeMenu.data('menu')
            };
            storeArcLocation(locationObject);
            var $menu = $('.' + self.menusActive +
                '[data-location="' + locationObject.location + '"]' +
                '[data-menu="' + locationObject.name + '"]'
            );
            window.arcLettering($menu);
        }
    }

    /**
     * Unset the current location select by hiding the blocks and menu block
     * Then show the new location object by showing new blocks and menu block
     * @param  object locationObject The data-location tag of the new location
     */
    function changeSelectedLocation(locationObject) {
        unsetLocationSelection();
        setLocationSelection(locationObject);
    }

    /**
     * Display the location that has a specific data-location tag
     * @param  object locationObject An object with a key 'location', and a string value
     */
    function getLocationTag(locationObject) {
        return '[data-location=' + locationObject.location + ']';
    }

    /**
     * Hide any currently displayed menus and then show a menu with specific
     * data-location and data-menu tags
     * @param  object menuObject An object with keys 'location' and 'name', with string values
     */
    function changeSelectedMenu(menuObject) {
        unsetMenuSelection(menuObject.location);
        setMenuSelection(menuObject);
        setMenuSelectionDEV(menuObject);
        arcMenuHeaders({
            'location': menuObject.location,
            'name': menuObject.name
        });
    }

    /**
     * Display the menu that has specific data-location and data-menu tags
     * @param  object menuObject An object with keys 'location' and 'name', with string values
     */
    function getMenuTag(menuObject) {
        return '[data-menu=' + menuObject.name + ']';
    }

    /**
     * Sets the currently selected location to that defined by its data-location
     * tag, by adjusting classes on the elements
     *
     * @param string dataLocationTag The value of data-location to locate
     */
    function setLocationSelection(locationObject) {
        var dataLocationTag = getLocationTag(locationObject);
        $('.' + self.menuLocation + dataLocationTag).addClass('c-btn--active');
        $('.' + self.menuNames + dataLocationTag).addClass(self.menuNames + '--active');
        $('.' + self.menuBlock + dataLocationTag).addClass(self.menuBlock + '--active');
        var $dropdownValue = $('.c-menu-location--mobile[data-value="' + locationObject.location + '"]');
        if ($dropdownValue.length) {
            $(this).parent().siblings('.c-form__dropdown').find('.c-form__dropdown-selected')
                .data('value', locationObject.location)
                .text($dropdownValue.text());
        }
        // Need a default menu if none is already selected
        $selectedLocation = $('.' + self.menuNames + dataLocationTag);
        $selectedMenu = $selectedLocation.children('.' + self.menuNameItems + '.' + self.menuNameActive);
        if (!$selectedMenu.length) {
            changeSelectedMenu(getDefaultMenu($selectedLocation));
        } else {
            updateMenuQueryParam(locationObject.location, $selectedMenu.data('menu'));
        }
    }

    /**
     * Unsets the currently selected location by adjusting classes on the elements
     */
    function unsetLocationSelection() {
        $('.' + self.menuLocation + '.c-btn--active').removeClass('c-btn--active');
        $('.' + self.menuNames + '--active').removeClass(self.menuNames + '--active');
        $('.' + self.menuBlock + '--active').removeClass(self.menuBlock + '--active');
    }

    /**
     * Get the data for the default menu item for a specific location
     * @param  jquery object $location The menuNames object that holds the location's menus
     * @return object                  An object with keys 'location' and 'name', with string values
     */
    function getDefaultMenu($location) {
        var $firstMenu = $location
            .children('.' + self.menuNameItems)
            .first();
        var location = $location.data('location'),
            name = $firstMenu.data('menu');
        return {
            'location': location,
            'name': name
        };
    }

    /**
     * Sets the currently selected menu to that defined by its data-location and
     * data-menu tags, by adjusting classes on the elements
     *
     * @param  object menuObject An object with keys 'location' and 'name', with string values
     */
    function setMenuSelection(menuObject) {
        var locationTag = getLocationTag(menuObject),
            menuTag = getMenuTag(menuObject);

        $('.' + self.menuNames + locationTag)
            .children('.' + self.menuNameItems + menuTag)
            .addClass(self.menuNameActive);
        $('.' + self.menus + locationTag + menuTag).addClass(self.menus + '--active');
        updateMenuQueryParam(menuObject.location, menuObject.name);
    }

    /**
     * Unsets the currently selected menu by adjusting classes on the elements
     *
     * @param  string location The location slug to unset menus for
     */
    function unsetMenuSelection(location) {
        if (location !== undefined) {
            $menuNames = $('.' + self.menuNames + '[data-location=' + location + ']');
            $menuNameItems = $menuNames.children($('.' + self.menuNameItems + '.' + self.menuNameActive));
            $menus = $('.' + self.menus + '--active' + '[data-location=' + location + ']');
        } else {
            $menuNameItems = $('.' + self.menuNameItems + '.' + self.menuNameActive);
            $menus = $('.' + self.menus + '--active');
        }
        $menuNameItems.removeClass(self.menuNameActive);
        $menus.removeClass(self.menus + '--active');
    }

    /**
     * Updates the URL to change the location to the new location string
     *
     * @param  string locationString The location slug
     */
    function updateLocationQueryParam(locationString) {
        var url = new URI(window.location.href);
        url.removeQuery('loc');
        url.addQuery('loc', locationString);
        history.replaceState(null, null, url._parts.path + '?' + url._parts.query);
    }

    /**
     * Updates the URL to change the location and menus to the new strings
     *
     * @param  string locationString The location slug
     * @param  string menuString The menu slug
     */
    function updateMenuQueryParam(locationString, menuString) {
        var url = new URI(window.location.href);
        url.removeQuery('loc');
        url.removeQuery('menu');
        url.addQuery('loc', locationString);
        url.addQuery('menu', menuString);
        history.replaceState(null, null, url._parts.path + '?' + url._parts.query);
    }

    /**
     * Stores menu and location data into self.arcHeaders object.
     * @param  object locationObject An object with a key 'location', and a string value
     */
    function storeArcLocation(locationObject) {
        if (locationObject.location === undefined ||
            locationObject.name === undefined) return;

        if (hasArced(locationObject)) return;

        self.arcHeaders[locationObject.location].push(locationObject.name);
    }

    /**
     * Initiates the lettering plugin on menu specific `.u-arc`
     * elements. The setInterval is used to keep firing the lettering
     * plugin until it is complete.
     * @param  object locationObject An object with a key 'location', and a string value
     */
    function arcHeader(locationObject) {
        var $menu = $('.' + self.menus +
            '[data-location="' + locationObject.location + '"]' +
            '[data-menu="' + locationObject.name + '"]'
        );
        window.arcLettering($menu);
    }

    /**
     * Returns whether a specific locationObject has had its headers arced or not
     */
    function hasArced(locationObject) {
        if (self.arcHeaders[locationObject.location] === undefined) {
            self.arcHeaders[locationObject.location] = [];
        }

        return (self.arcHeaders[locationObject.location].indexOf(locationObject.name) >= 0);
    }

    /**
     * Arcs headers in a menu if not already arced
     */
    function arcMenuHeaders(locationObject) {
        if (locationObject.location === undefined ||
            locationObject.name === undefined) return;

        if (hasArced(locationObject)) return;

        storeArcLocation(locationObject);

        arcHeader(locationObject);
    }

    /**
     * Bind event handlers across the objects to handle interaction with the menus
     */
    function addHandlers() {
        $('.' + self.menuNameItems).on('click.menu', function (event) {
            $clickedMenuName = $(event.target);
            $clickedMenuGroup = $clickedMenuName.parents('.' + self.menuNames);

            if ($(window).width() <= 735) {
                changeSelectedMenu({
                    'location': $clickedMenuGroup.data('location'),
                    'name': $clickedMenuName.data('value')
                });
            } else {
                changeSelectedMenu({
                    'location': $clickedMenuGroup.data('location'),
                    'name': $clickedMenuName.data('menu')
                });
            }
        });
        $('.' + self.menuLocation + '.' + self.menuLocation + '--desktop').on('click.menu', function (event) {
            $clickedMenuLocation = $(event.target);
            changeSelectedLocation({
                'location': $clickedMenuLocation.data('location')
            });
        });
        $('.' + self.menuLocation + '.' + self.menuLocation + '--mobile').on('click.menu', function (event) {
            $clickedMenuLocation = $(event.target);
            changeSelectedLocation({
                'location': $clickedMenuLocation.data('value')
            });
        });
    }

    init();
}
