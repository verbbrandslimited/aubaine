function Events() {
    var self = this;
    /**
     * Initialise the object
     */
    function init() {
        instantiateObjects();
        addHandlers();
        accordionInit();
        eventsModalPopup();
        locationFilter();
        popupPopulator();
    }
    /**
     * Set up the initial classes to use across the events object
     */
    function instantiateObjects() {
        self.eventsDropdown = 'js-events-dropdown';
        self.eventsDescriptions = 'js-location-event-descriptions';
        self.eventsDetails = 'js-location-event-details';
        self.eventsInitialMessage = 'js-events-initial-message';
        self.swiper = 'js-carousel--locations-block';
        self.swiperItem = 'swiper-slide';
        self.locationWrapper = 'location-desc-container';
        self.locationDesc = 'desc-container';
        self.locationReadMore = 'read-more';
        self.enquireModal = 'enquiry-popup';
        self.enquireTrigger = 'location-enquire';
        self.enquireClose = 'exit-popup';
        self.enquireOverlay = 'overlay';
    }
    /**
     * By default, all location event content is hidden. This removes that
     * hiding class when user requests to view event content of location
     */
    function showEventsContent($section, location_id) {
        $section
            .children()
                .addClass('u-hide');
        $section
            .find('[data-location="' + location_id + '"]')
                .removeClass('u-hide');
    }
    /**
     * An initial message is shown on load to guide the user to select a
     * location from the dropdown. This hides that message
     */
    function hideInitialMessage() {
        $('.' + self.eventsInitialMessage).addClass('u-hide');
    }
    /**
     * If a carousel has been added to the template then if
     * available, slide carousel to relevent location events image
     */
    function slideToLocationImage(location_id) {
        var swiper = $('.' + self.swiper);
        if (!swiper.length) return;

        var slideIndex = swiper
            .find('.' + self.swiperItem + '[data-location="' + location_id + '"]')
                .index();

        if (slideIndex < 0) return;

        window.locationsBlockSwiper.slideTo(slideIndex);
    }
    /**
     * Bind event handlers across the objects to handle interaction with events
     */
    function addHandlers() {
        $('.' + self.eventsDropdown).on('change', function(event) {
            var locationId = $(this).val();
            hideInitialMessage();
            showEventsContent($('.' + self.eventsDescriptions), locationId);
            showEventsContent($('.' + self.eventsDetails), locationId);
            slideToLocationImage(locationId);
        });
    }
    /**
     * Events page accordion:
     * Add height attr to container then,
     * animate height of attr on click
     */
    function accordionInit() {
	    $('.' + self.locationWrapper).each(function(){
		    var $content = $(this).find('.' + self.locationDesc);
		    $content.attr('data-height', $content.outerHeight(true));
	    });
	    
	    $('.' + self.locationReadMore).click(function(){
		    console.log("clicked");
		    var $height = $(this).siblings().find('.' + self.locationDesc).data('height');
		    
		    if( $(this).siblings('.' + self.locationWrapper).hasClass('reveal') ){
			    $(this).siblings('.' + self.locationWrapper)
			    	.css('height', '84px')
				    	.removeClass('reveal');
				$(this)
			    	.text('Read More')
			    		.removeClass('reveal');
		    } else {
			    $(this).siblings('.' + self.locationWrapper)
			    	.addClass('reveal')
			    		.css('height', $height + 'px');
			    $(this)
			    	.text('Read Less')
			    		.addClass('reveal');
		    }
	    });
    }
    /**
     * Events page accordion:
     * Open / Close event enquire modal
     */
    function eventsModalPopup() {
	    $('.' + self.enquireTrigger).click(function(evt){
		    evt.preventDefault();
		    
		    $('.' + self.enquireModal)
		    	.toggleClass('reveal');
	    });
	    
	    $('.' + self.enquireClose).click(function(){
		    $('.' + self.enquireModal)
		    	.removeClass('reveal');
	    });
    }
    
    function popupPopulator() {
	    $(".location .location-enquire").click(function(evt){
		    var $parent = evt.target.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.childNodes;
		    var $title = $parent[1];
		    $title.toString().replace('<h3>', '').replace('</h3>', '');
		    console.log($title);
	    });
    }
    
    function locationFilter() {
	    $(".location-selection-filter .c-form__dropdown-options").click(function(evt){
		    $id = evt.target.dataset.value;
		    $(".location")
		    	.addClass("filter-enabled")
		    		.removeClass('reveal');
		    $(".location[data-id='" + $id + "']").addClass('reveal');
		    
		    $(".location-selection-filter")
		    	.addClass('filter-enabled');
		    
		    $(".location-selection-filter .reset").click(function(){
			    $('.location')
			    	.removeClass('filter-enabled')
			    		.removeClass('reveal');
			    $(".location-selection-filter")
					.removeClass('filter-enabled');
					
				$(".location-selection-filter .c-form__dropdown-selected").text('Select a location to find events...');
		    });
	    });
    }
    init();
}

