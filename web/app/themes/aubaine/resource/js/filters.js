function Filter($filterObject) {
    var self = this;
    self.$filterObject = $filterObject;

    /**
     * Initialise the object
     */
    function init() {
        instantiateObjects();
        setupInitialState();
        addHandlers();
    }

    /**
     * Set up the filters object
     */
    function instantiateObjects() {
        self.filteringType = getFilteringType();
        self.activeFilters = [];
        self.classes = {
            filter: 'c-filters',
            term: 'c-filters__term',
            item: 'c-filters__item',
            filterActive: 'c-filters--active',
            termActive: 'c-filters__term--active',
            itemActive: 'c-filters__item--active',
            filterHook: 'js-filter',
            termHook: 'js-filter-term',
        };
        self.$filterTerms = self.$filterObject.find('.' + self.classes.term);
        self.$filterItems = self.$filterObject.find('.' + self.classes.item);
    }

    /**
     * Adds any already active filters at time of setup to the internal list
     * of active filters, and refreshes the filter object to apply them
     * @return null
     */
    function setupInitialState() {
        if (countActiveFilters() > 0) {
            self.$filterTerms.each(function() {
                $this = $(this);
                if ($this.hasClass(self.classes.termActive)) {
                    toggleFilterItem($this.data('filter'), true);
                }
            });
            refreshFilterObject();
        }
    }

    /**
     * Return the type of object the filters will be filtering
     * @return string|null The type of object to filter, or null if unknown
     */
    function getFilteringType() {
        if (self.$filterObject.hasClass('c-menu')) return 'menu';
        return null;
    }

    /**
     * Turn a filter on or off
     * @param  jQuery object $filter  The filter to turn on or off
     * @param  boolean      newState  The state to toggle the filter to
     * @return null
     */
    function toggleFilter($filter, newState) {
        $filter.toggleClass(self.classes.termActive, newState);
        toggleFilterItem($filter.data('filter'), newState);
        refreshFilterObject();
    }

    /**
     * Adds or removes a filter from the list of active filter items
     * @param  string  filterTerm        The filter string to toggle
     * @param  boolean isActivatingItem  True to activate, false to deactivate
     * @return null
     */
    function toggleFilterItem(filterTerm, isActivatingItem) {
        var filterPosition = self.activeFilters.indexOf(filterTerm);
        if (isActivatingItem && (-1 === filterPosition)) {
            self.activeFilters.push(filterTerm);
        } else if (!isActivatingItem && (filterPosition > -1)) {
            self.activeFilters.splice(filterPosition, 1);
        }
    }

    /**
     * Checks to see if an active filter is applied to one of the filter terms
     * and sets itself to active if so, and unactive if not. Should always be
     * called whenever a filter is toggled on or off.
     * @return null
     */
    function refreshFilterObject() {
        self.$filterObject.toggleClass(self.classes.filterActive, countActiveFilters() > 0);
        refreshFilteredItems();
    }

    /**
     * Checks the list of currently filtered for attributes, and searches through
     * the filter items, activating those that match and deactivating those that
     * do not
     * @return null
     */
    function refreshFilteredItems() {
        self.$filterItems.each(function() {
            var $this = $(this),
                active = true;
            for (index = 0; index < self.activeFilters.length; index++) {
                if (!$this.is('[data-' + self.activeFilters[index] + ']')) {
                    active = false;
                    break;
                }
            }
            $this.toggleClass(self.classes.itemActive, active);
        });
    }

    /**
     * Count how many filters are currently active in the filter object
     * @return int Count of active filters
     */
    function countActiveFilters() {
        return self.$filterObject.find(
                '.' + self.classes.term + '.' + self.classes.termActive
            ).length;
    }

    /**
     * Bind event handlers across the objects to handle interaction with the filters
     */
    function addHandlers() {
        self.$filterTerms.on('click.filter', function(event) {
            var $this = $(this),
                currentState = $this.hasClass(self.classes.termActive);
            toggleFilter($this, !currentState);
        });
    }
    init();
}
