/**
 * Contents:
 *
 * 1.0 Core Functionality
 *     1.1 Basic Functions
 *     1.2 Debouncers + Throttlers
 * 2.0 Theme Specific Functions
 *     2.1 Lettering scripts
 *     2.2 Hamburger
 *     2.3 Fixed navbar
 *     2.4 Hero swiper
 *     2.5 Form input labels
 *     2.6 Newsletter submit enabler
 *     2.7 Custom dropdowns
 *     2.8 Menus
 *     2.9 Peek pages
 *     2.11 Contact us page
 *     2.12 Sidebar link hover
 *
 */

jQuery(document).ready(function($) {

    /** 1.0 Core Functionality */

    /** 1.1 Basic Functions */

    if (!Modernizr.svg) $('img[src$=\'.svg\']').attr('src', fallback);

    function scrollTo(pixels, duration) {
        if (pixels === undefined) return false;
        duration = duration || 400;
        $('html, body').animate({ scrollTop: pixels }, duration);
    }

    function scrollToElement($element) {
        if (!$element.length) return false;
        scrollTo($element.offset().top);
    }

    $('a[href^=#]').click(function() {
        event.preventDefault();
        if ($(this).attr('href').substr(1) && $(this.hash).offset() !== undefined) {
            $('html, body').animate({ scrollTop: $(this.hash).offset().top }, 200);
        }
    });

    $('a[href=#top]').click(function() {
        scrollTo(0);
    });

    $.fn.removeClassRegex = function(regex) {
        return $(this).removeClass(function(index, classes) {
            return classes.split(/\s+/).filter(function(c) {
                return regex.test(c);
            }).join(' ');
        });
    };

    window.sanitiseUrl = function(url) {
        if (url.charAt(url.length - 1) === '/') {
            url = url.substr(0, url.length - 1);
        }
        return url.substr(url.lastIndexOf('/') + 1);
    };


    // $(window).scroll(function()  {
    //
    //
    //
    //     var scroll = $(window).scrollTop() ;
    //     var triggered_times = 0;
    //
    //     if (scroll >= 200) {
    //
    //         $(".popup-section-homepage").addClass("active");
    //
    //     }
    //
    // });

    var triggered_times = 0;

    $(window).scroll(function() {



        var scroll = $(window).scrollTop() ;


        if (scroll >= 500 && triggered_times === 0) {
            $(".popup-section-homepage").toggleClass("active");
            triggered_times = 1;
        }
    });

    $(".close-button").click(function(){
        $(".popup-section-homepage").removeClass("active")
    });





    /** Use this throttle function to prevent a function from being called more than once
     * during a certain amount of time
     */
    function throttle(fn, threshhold, scope) {
        threshold = threshhold || (threshhold = 250);
        var last,
            deferTimer;
        return function () {
            var context = scope || this;

            var now = +new Date(),
                args = arguments;
            if (last && now < last + threshhold) {
                // hold on to it
                clearTimeout(deferTimer);
                deferTimer = setTimeout(function () {
                    last = now;
                    fn.apply(context, args);
                }, threshhold);
            } else {
                last = now;
                fn.apply(context, args);
            }
        };
    }

    /** 1.2 Debouncers + Throttlers */

    // Window resize debouncer
    /*
    $(window).resize(function() {
        if (this.resizeEvent) clearTimeout(this.resizeEvent);
        this.resizeEvent = setTimeout(function() {
            $(this).trigger('windowResize');
        }, 500);
    });
    $(window).bind('windowResize', function() {

    });
    */

    /** Window scroll throttler events **/

    // Window scroll throttler
    /*
    $(window).on('scroll', throttle(function() {
        doStuff();
    }, 300));
    */

    /** 2.0 Theme Specific Functions */

    /** 2.1 Letter JS */

    window.arcLettering = function(element) {
        if ($(window).width() < 768) return false;
        arcElement = ( element !== undefined ) ? element.find('.u-arc') : $('.u-arc');
        arcElement.lettering().circleType({radius: 484});
    };
    if (!$('.c-menu-page').length) {
        window.arcLettering();
    }

    /** 2.2 Hamburger */

    var menuOpen = false;

    $('.c-hamburger').click(function() {
        if ($(this).hasClass('c-hamburger--peek-close')) return; // Same button overloaded to close dynamic menus too, if open
        menuOpen = !menuOpen;
        $(this).toggleClass('c-hamburger--active', menuOpen);
        $('.c-navbar').toggleClass('c-navbar--menu', menuOpen);
        $('.c-sidebar').toggleClass('c-sidebar--active', menuOpen);
    });

    /** 2.3 Fixed navbar */

    window.fixedNavbar = function() {
        if ($(window).width() < 768) return false;
        $('.c-navbar').toggleClass('c-navbar--fixed', $(document).scrollTop() > 70);
    };

    $(window).on('scroll', throttle(function() {
        fixedNavbar();
    }, 50));

    /** 2.4 Hero swiper */

    if ($('body').hasClass('home')) {
        var heroSwiper = new Swiper('.c-carousel--hero', {
            speed: 500,
            autoplay: 4000,
            effect: 'fade',
            noSwipingClass: 'swiper-slide',
            onTransitionStart: function(swiper) {
                fadeCurrentOfferingText();
            },
            onTransitionEnd: function(swiper) {
                showOfferingText(swiper.activeIndex);
            }
        });
        heroSwiper.stopAutoplay(); // Initially stop autoplay

        var offerings_inital_delay = 2600,
            offerings_second_delay = 1000;

        $(window).load(function() {
            $('.c-hero').addClass('c-hero--visible');
            $('.c-navbar').removeClass('c-navbar--up');
            $('.c-hero__booking').removeClass('c-hero__booking--down');
            setTimeout(function() {
                $('.c-hero__branding--main .c-site-branding').addClass('c-site-branding--hide');
                heroSwiper.startAutoplay();
                setTimeout(function() {
                    $('.c-offerings__item--initial').removeClass('c-offerings__item--initial');
                    $('.c-hero__branding--small .c-site-branding').addClass('c-site-branding--show');
                }, offerings_second_delay);
            }, offerings_inital_delay);
        });
    }

    $('#booking-location').change(timeSelect);
    function timeSelect() {
        var target = $('#booking-location option:selected').val();
        if(target.indexOf("selfridges") >=0) {
            $('.time-picker--default').removeClass('show');
            $('.time-picker--selfridges').addClass('show');
        } else {
            $('.time-picker--default').addClass('show');
            $('.time-picker--selfridges').removeClass('show');
        }
    }

    function fadeCurrentOfferingText() {
        $('.c-offerings__item--active')
            .removeClass('c-offerings__item--active');
    }

    function showOfferingText(goToIndex) {
        goToIndex++; // Swiper gives a zeroed index but the CSS nth selector starts at 1
        $('.c-offerings .c-offerings__item:nth-child(' + goToIndex + ')')
            .addClass('c-offerings__item--active');
    }

    /** 2.5 Form input labels */

    $('.js-form__input')
        .on('focus', function() {
            $(this)
                .closest('.c-form__field')
                    .addClass('c-form__field--filled');
        })
        .on('blur', function() {
            if (!$(this).val()) {
                $(this)
                    .closest('.c-form__field')
                        .removeClass('c-form__field--filled');
            }
        });


    /** 2.6 Newsletter submit enabler */

    $('.js-newsletter-input')
        .on('focus', function() {
            $('.js-newsletter-submit')
                .removeAttr('disabled');
        })
        .on('blur', function() {
            if (!$(this).val()) {
                $('.js-newsletter-submit')
                    .attr('disabled', true);
            }
        });


    /** 2.7 Custom dropdowns */

    $('.js-dropdown')
        .each(function() {
            var dropdownClass = ($(this).hasClass('js-dropdown--fallup')) ? ' c-form__dropdown--fallup' : '';
            var $dropdown = $('<div class="c-form__dropdown' + dropdownClass +'"/>');
            var $optionsList = $('<div class="c-form__dropdown-options"/>');
            var $selected = $('<div class="c-form__dropdown-selected"/>');
            var $default = $(this).find('option:selected');
            $selected
                .attr('data-value', $default.attr('value'))
                .html($default.html());
            $(this)
                .find('option')
                    .not('[disabled=disabled]')
                        .each(function() {
                            var $option = $('<div class="c-form__dropdown-item" />');
                            $option
                                .attr('data-value', $(this).attr('value'))
                                .addClass($(this).attr('class'))
                                .html($(this).html());
                            $optionsList.append($option);
                        });
            $dropdown.append($optionsList);
            $(this)
                .append($dropdown)
                .find('.c-form__dropdown')
                    .append($selected);
            $(this)
                .find('.c-form__select')
                    .hide();
        })
        .find('.c-form__dropdown')
            .click(function() {
                $(this).toggleClass('c-form__dropdown--open');
            })
            .find('.c-form__dropdown-item')
                .click(function() {
                    $(this)
                        .closest('.js-dropdown')
                            .find('.c-form__dropdown-selected')
                                .attr('data-value', $(this).attr('data-value'))
                                .html($(this).html());
                    $(this)
                        .closest('.js-dropdown')
                            .find('.c-form__select')
                                .val($(this).attr('data-value'))
                                .trigger('change');
                });

        $(document).click(function(event) {
            if ($('.c-form__dropdown--open').length && !$(event.target).is('.c-form__dropdown-selected')) {
                $('.c-form__dropdown--open').removeClass('c-form__dropdown--open');
            }
        });

    /** 2.8 Menus */
    if ($('.js-menu-page').length) {
        var menu = new Menu();
    }

    /** 2.9 Peek pages */

    if ($('.js-peek-link').length) {
        var peekPages = new Peek();
    }

    if ($('.js-carousel--gallery').length) {
        var singleLocationSwiper = new Swiper('.js-carousel--gallery', {
            autoplay: 3000,
            grabCursor: true,
            pagination: '.swiper-pagination',
            paginationClickable: true,
            speed: 1000
        });
    }

    if ($('.js-carousel--locations-block').length) {
        window.locationsBlockSwiper = new Swiper('.js-carousel--locations-block', {
            speed: 1000
        });
    }

    if ($('.js-datepicker').length) {
        $('.js-datepicker').each(function() {
            new Pikaday({
                container: $(this).parent().get(0),
                field: this,
                firstDay: 1,
                minDate: new Date()
            });
        });
    }
    
    if ($('.js-datepicker2').length) {
        $('.js-datepicker2').each(function() {
            var disabledDays = this.getAttribute('data-disable');
            var id, days;
                        
            new Pikaday({
                container: $(this).parent().get(0),
                disableDayFn: function(date){
                  
                  // Disable select days
                  var disabled = false;
                  
                  for(i = 0; i < disabledDays.length; i++)
                    if(parseInt(disabledDays[i]) === date.getDay())
                      disabled = true;

                  return disabled;
                },
                field: this,
                firstDay: 1,
                minDate: new Date()
            });
        });
    }

    /** 2.10 Filters */
    if ($('.js-filter').length) {
        $('.js-filter').each(function() {
            new Filter($(this));
        });
    }

    if ($('.js-events-page').length || $('.js-locations-block').length) {
        var events = new Events();
    }

    $(window).load(function() {
        $('.c-detail-header--loading').removeClass('c-detail-header--loading');
    });

    /** 2.11 Contact us page */

    $('.js-contact-restaurants-toggle').click(function() {
        $('.c-contact__section--active').removeClass('c-contact__section--active');
        $('.js-contact-restaurants-list').addClass('c-contact__section--active');
    });

    /** 2.12 Sidebar link hover */

    var sidebarBackgrounds = [
        'blue-dark',
        'yellow',
        'green-dark',
        'pink-dark'
    ];

    $('.c-sidebar__links-list--main ul li a').hover(function(){
        var hoveredSidebarItem = $(this)
            .parent()
                .index();
        $('.c-sidebar').removeClassRegex(/^u-bg-color/);
        $('.c-sidebar').addClass('u-bg-color--' + sidebarBackgrounds[hoveredSidebarItem]);
    }).mouseout(function(){
        $('.c-sidebar').removeClassRegex(/^u-bg-color/);
    });

	// Contact Page - location redirect
    $(".location-linker .c-form__dropdown-item").click(function(){
	    $url = $(this).data('url');
	    window.location.href = $url;
    });

    // Events Page - Filter locations
    $(".location-selection-filter select").on("change", function(){
	    var $id = $(this).val();

	    $(".location-venues .location")
	    	.addClass('filter-enabled')
	    		.removeClass('reveal');

	    $(".location-venues .location[data-id='" + $id + "']")
	    	.addClass('reveal');
    });

    /**
     * Fishbowl Subscribe
     */
    var $form = $('#fishbowl-subscribe');
    var $inputs = {
        firstName: $('#subscribe-FirstName', $form),
        lastName: $('#subscribe-LastName', $form),
        emailAddress: $('#subscribe-EmailAddress', $form),
        birthdate: $('#subscribe-Birthdate', $form),
        storeCode: $('#subscribe-StoreCode', $form),
        isEmployee: $('#subscribe-IsEmployee', $form),
        privacy: $('#subscribe-Privacy', $form),
        confirmSubscribe: $('#subscribe-ConfirmSubscribe', $form),
        listID: $('#listID', $form),
        optIn: $('#subscribe-GDPR_ReOptIn', $form),
    };

    var listID = {
        member: $inputs.listID.val(),
        employee: '30064771389'
    };

    var constraints = {
        firstName: {
            presence: {
                allowEmpty: false,
            },
        },
        lastName: {
            presence: {
                allowEmpty: false,
            },
        },
        emailAddress: {
            presence: {
                message: 'can\'t be blank',
            },
            email: {
                message: 'Invalid email',
            },
        },
        birthdate: {
            presence: true,
        },
        storeCode: {
            exclusion: {
                within: [''],
                message: 'Please select your nearest store',
            },
        },
        privacy: {
            presence: {
                message: 'You must agree to our Privary Policy',
            },
        },
        confirmSubscribe: {
            presence: {
                message: 'Please confirm that you wish to subscribe',
            },
        },
    };

    new Pikaday({
        container: document.getElementById('subscribe-birthdate-container'),
        field: $inputs.birthdate[0],
        format: 'DD/MM',
        yearRange: 0,
        minDate: new Date(new Date().getFullYear(), 0, 1),
        maxDate: new Date(new Date().getFullYear(), 11, 31),
        toString: function(date, format) {
          return moment(date).format(format);
        },
        onOpen: function() {
          $inputs.birthdate.closest('.c-form__field').addClass('c-form__field--border-green');
        },
        onClose: function() {
          $inputs.birthdate.closest('.c-form__field').removeClass('c-form__field--border-green');
        },
    });

    $inputs.isEmployee.change(function(){
        console.log($(this).is(':checked'));

        var isChecked = $(this).is(':checked');
        if (isChecked) {
            $inputs.listID.val(listID.member + ',' + listID.employee);
        } else {
            $inputs.listID.val(listID.member);
        }
    });

    validate.formatters.custom = function(errors) {
        var validationObject = {};
        $.each(errors, function(key, value){
            var errorMessage = value.options.message || value.error;
            if (Array.isArray(validationObject[value.attribute])) {
                validationObject[value.attribute].push(errorMessage);
            } else {
                validationObject[value.attribute] = [errorMessage];
            }
        });
        return validationObject;
    };

    $form.submit(function(e){
        /**
         * Reset Errors
         */
        $('.errors', this).remove();
        $('input.error', this).removeClass('error');

        /**
         * Validate form inputs
         */
        var validationObject = {};

        $.each($inputs, function(key, value) {
            if ($inputs[key].attr('type') === 'checkbox') {
                validationObject[key] = $inputs[key].is(':checked') || null;
            } else {
                validationObject[key] = $inputs[key].val();
            }
        });

        var validation = validate(validationObject, constraints, {format: 'custom'});

        if (validation) {
            $.each(validation, function(key, value) {
                $inputs[key].addClass('error');
                var $container = $inputs[key].closest('.input-wrapper');
                var $errors = $('<ul class="errors"></ul>');

                $.each(validation[key], function(errorkey, errormessage) {
                    $errors.append('<li>' + errormessage + '</li>')
                });

                $container.append($errors);
            });

            return false;
        }

        /**
         * Set Opt-in to true and submit
         */
        $inputs.optIn.val(true);
        return true;
    });

    /**
     * Custom Placeholders
     */
    $('.custom-placeholder').each(function(){
        var $input = $('input', this);
        var $placeholder = $('.custom-placeholder__placeholder', this);

        $input.change(function(){
            var value = $(this).val();
            if (value) {
                $placeholder.hide();
            } else {
                $placeholder.show();
            }
        });
    });
});

jQuery(document).load(function(){
	/** (VERB) 2.12 Map peek trigger */

    $(".book-table-link").click(function(evt){
	    evt.preventDefault();
	    console.log("clicked");
	    $("#menu-top-level-menu #menu-item-40 a").trigger('click');
    });
});
