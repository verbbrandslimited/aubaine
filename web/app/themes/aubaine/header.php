<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div class="container">
 *
 * @package Hex Digital
 * @subpackage Aubaine
 * @since 2017
 */
?><!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ) ?>">
        <meta name="viewport" content="width=device-width">
        <meta name="format-detection" content="telephone=no">
        <title><?php wp_title( '|', true, 'right' ) ?></title>
        <?php if( is_page(11) ): ?>
          <meta name="description" content="Aubaine is a passionate marriage of French culinary creativity and the discerning standards of the London dining scene.">
        <?php endif; ?>
        <!-- Google Tag Manager -->
            <?php if ( is_production() ) : ?>
                <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-MCH27CQ');</script>
            <?php endif ?>
        <!-- End Google Tag Manager -->

        <!-- Global site tag (gtag.js) - AdWords: 792861252 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-791955007"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'AW-791955007');
        </script>

        <?php

        $url = 'https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        ?>

        <?php if(strpos($url,'offers/happy-hour/') !== false) :?>

            <script>
                gtag('event', 'page_view', {
                    'send_to': 'AW-791955007',
                    'user_id': 'replace with value'
                });
            </script>

        <?php endif; ?>

        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js" integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c" crossorigin="anonymous"></script>
        <?php wp_head() ?>
    </head>

    <body <?php body_class() ?>>
    <!-- Google Tag Manager (noscript) -->
        <?php if ( is_production() ) : ?>
            <noscript>
                <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MCH27CQ"
                    height="0" width="0" style="display:none;visibility:hidden"></iframe>
            </noscript>
        <?php endif ?>
    <!-- End Google Tag Manager (noscript) -->
    <?php include 'build/img/sprites.svg' ?>
    <div class="c-peek js-peek">
        <?php include_all_peek_pages() ?>
    </div>
    <?php include get_partials_directory_uri() . '/navbar.php' ?>
    <?php include get_partials_directory_uri() . '/sidebar.php' ?>
    <?php include_hero() ?>
