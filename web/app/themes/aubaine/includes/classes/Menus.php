<?php

namespace Aubaine;

class Menus {
    private $menu_data;

    function __construct() {
        $this->menu_data = [];
    }

    /**
     * Add a new LocationObject to the Menus data
     * @param LocationObject $location The location object to add
     * @param int|null       $index    An optional index to add as the key
     */
    function add_location( LocationObject $location, int $index = null ) {
        if ( isset( $index ) ) {
            if ( ! array_key_exists( $index, $this->menu_data ) ) {
                $this->menu_data[ $index ] = $location;
            } else {
                throw new Exception('Attempted to add LocationObject with a key, ' .
                                     $index . ', that already exists.');
            }
        } else {
            $this->menu_data[] = $location;
        }
    }

    /**
     * Get all the locations as an array of LocationObjects
     * @return array Array of all LocationObjects
     */
    function get_all_locations() {
        return $this->menu_data;
    }

    /**
     * Get a single LocationObject from an index
     * @param  int    $index   The Menus index to fetch the LocationObject of
     * @return LocationObject  The corresponding Location Object
     */
    function get_location( int $index ) {
        return $this->menu_data[ $index ];
    }

    /**
     * Update a single LocationObject that has a specific index
     * @param  int            $index    The index of the LocationObject
     * @param  LocationObject $location The new LocationObject to replace it with
     */
    function update_location( int $index, LocationObject $location ) {
        remove_location( $index );
        add_location( $location, $index );
    }

    /**
     * Remove a LocationObject that has a specific index
     * @param  int $index The index of the LocationObject
     */
    function remove_location( int $index ) {
        unset( $this->menu_data[ $index ] );
    }

    /**
     * Sort the menu data by each LocationObject's priority
     */
    function sort_menu_by_location() {
        usort( $this->menu_data, ['Aubaine\LocationObject', 'sort_by_location_priority'] );
    }
}
