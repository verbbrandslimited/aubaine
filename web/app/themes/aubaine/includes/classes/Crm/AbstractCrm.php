<?php

namespace Aubaine\Crm;

abstract class AbstractCrm implements CrmInterface {

    /**
     * The default options to give to the CRM
     * @var array
     */
    protected $defaultOptions;

    /**
     * Any extra options to submit in a request
     * @var array
     */
    protected $options;

    /**
     * The last query that was sent to the CRM
     * @var array
     */
    protected $lastQuery;

    /**
     * The response from the most recent request the CRM has made
     * @var array
     */
    protected $response;

    /**
     * Replace the current options with a new list of options
     * If the options are not an array they will be cast to an array
     * @param string|array
     */
    public function resetOptions( $options ) {
        $this->options = $this->defaultOptions;
    }

    /**
     * Execute the query, using the set options
     */
    public function execute() {
        $this->lastQuery = $this->options;
        $this->response = $this->request( $this->options );
    }

    /**
     * Get the latest response from the CRM
     * @return array | null
     */
    public function getResponse() {
        return $this->response;
    }

    /**
     * Get the status of the latest response, whether it passed or failed
     * @return boolean
     */
    public function getResponseStatus() {
        return $this->response['code'] === 302;
    }

    /**
     * Get the latest query sent to the CRM
     * @return array | null
     */
    public function getLastQuery() {
        return $this->lastQuery;
    }

    /**
     * Get the latest response code from the CRM
     * @return string | null
     */
    public function getResponseCode() {
        if ( array_key_exists( 'code', $this->response ) ) {
            return $this->response['code'];
        }
    }

    /**
     * Get the latest response body from the CRM
     * @return string | null
     */
    public function getResponseBody() {
        if ( array_key_exists( 'body', $this->response ) ) {
            return $this->response['body'];
        }
    }

    /**
     * Sends a POST HTTP request to a CRM URL and returns
     * the response.
     * @param  string           $requestUrl
     * @param  array            $params
     * @return array
     */
    public function request( $params ) {
        $ch = curl_init( $this->endpoint() );

        $postString = http_build_query( $params, '', '&' );

        curl_setopt( $ch, CURLOPT_POST, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $postString );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_HEADER, true );

        $response = $this->build_response_array( $ch );

        curl_close( $ch );

        return $response;
    }

    /**
     * Executes, retrieves and builds a response array.
     * @param  object $ch
     * @return array
     */
    private function build_response_array( $ch ) {
        $responseBody = curl_exec( $ch );
        $responseCode = curl_getinfo( $ch, CURLINFO_RESPONSE_CODE );

        return [
            'code' => $responseCode,
            'body' => $responseBody,
        ];
    }
}
