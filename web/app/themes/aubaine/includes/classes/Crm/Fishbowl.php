<?php

namespace Aubaine\Crm;

class Fishbowl extends AbstractCrm implements CrmInterface {

    /**
     * Fishbowl constructor
     */
    function __construct() {
        $this->defaultOptions = [
            'SiteGUID' => $this->GUID(),
        ];

        $this->options = $this->defaultOptions;
    }

    /**
     * @return string
     */
    public function endpoint() {
        return 'http://aubaine.fbmta.com/members/subscribe.aspx';
    }

    /**
     * Return the GUID for Fishbowl
     * @return string
     */
    public function GUID() {
        return '4A5656EF-8A72-4C92-9D0F-4892A4FD5C78';
    }

    /**
     * Append one or more options to the current list of options
     * If the options are not an array they will be cast to an array
     * @param string|array $options
     */
    public function addOptions( $options ) {
        $this->options = array_merge( $this->options, (array) $options );
    }

    /**
     * Set the List ID that the Fishbowl functions will interact with
     * @param string $list_id
     */
    public function setListID( $list_id ) {
        $this->addOptions( ['ListId' => $list_id] );
    }

    /**
     * Set the Email address that the Fishbowl functions will use
     * @param string $list_id
     */
    public function setEmailAddress( $email ) {
        $this->addOptions( ['EmailAddress' => $email] );
    }

    /**
     * Process the newsletter signup form and subscribe the
     * user to the Newsletter mailing list in the CRM
     */
    public function subscribe() {
        $this->addOptions( ['Action' => 'subscribe'] );
        $this->execute();
    }
}
