<?php

namespace Aubaine\Crm;

interface CrmInterface {

    /**
     * Return the endpoint for the particular CRM
     * @return string
     */
    public function endpoint();
}
