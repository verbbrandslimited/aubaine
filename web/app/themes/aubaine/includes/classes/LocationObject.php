<?php

namespace Aubaine;

class LocationObject {
    private $name;
    private $location_priority;
    private $menus;

    function __construct( $name, $location_priority, $menus ) {
        $this->name = $name;
        $this->location_priority = $location_priority;
        $this->menus = $menus;
    }

    /**
     * Get the name of the LocationObject
     * @return string The LocationObject's name
     */
    function get_name() {
        return $this->name;
    }

    /**
     * Get the priority of the LocationObject
     * @return integer The LocationObject's priority
     */
    function get_priority() {
        return $this->location_priority;
    }

    /**
     * Get the menus available at the LocationObject
     * @return integer The LocationObject's menus
     */
    function get_menus() {
        return $this->menus;
    }

    /**
     * Returns
     * @return integer  positive int if location two is higher priority
     *                  0 if locations are equal priority
     *                  negative int if location two is lower priority
     */
    static function sort_by_location_priority( $location_object_one, $location_object_two ) {
        return $location_object_two->get_priority() - $location_object_one->get_priority();
    }
}
