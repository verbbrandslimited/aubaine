<?php

/**
 * Return the HTML for an SVG symbol
 *
 * @param  string        $symbol_name           The name of the symbol
 * @param  string        $role                  The role of the symbol
 * @param  string|array  $class_names Any additional class names needed
 * @return string
 */
function get_svg( $symbol_name, $role = 'img', $class_names = '' ) {
    if ( is_array( $class_names ) ) {
        if ( 0 !== count( $class_names ) ) {
            $class_names = ' ' . implode( ' ', $class_names );
        } else {
            $class_names = '';
        }
    } elseif ( strlen( $class_names ) > 0 ) {
        $class_names = ' ' . $class_names;
    }
    $symbol_name = esc_attr( strtolower( $symbol_name ) );
    $role = esc_attr( $role );
    $class_names = esc_attr( $class_names );

    $html = '<span class="icon icon-' . $symbol_name . $class_names . '">' . PHP_EOL;
    $html .= '    <svg role="' . $role . '">' . PHP_EOL;
    $html .= '        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-' . $symbol_name . '"/>' . PHP_EOL;
    $html .= '    </svg>' . PHP_EOL;
    $html .= '</span>';
    return $html;
}

/**
 * Retrieve an SVG image name from an identifying string, often used in Wordpress CMS
 * Can be used inside of a get_svg() call as the first parameter, when required
 * @param  string $identifier The identifier for the svg
 * @return string|null        The actual identifying part of the filename for the SVG
 */
function get_svg_name_from_identifier( $identifier ) {
    $icons = [
        'location-marker' => 'location-marker',
        'email' => 'letter',
        'telephone' => 'telephone',
        'letter' => 'letter',
        'underground' => 'underground',
        'group' => 'group',
        'user' => 'user',
    ];
    if ( array_key_exists( $identifier, $icons ) ) return $icons[ $identifier ];
}
