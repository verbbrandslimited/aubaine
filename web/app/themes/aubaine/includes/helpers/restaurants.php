<?php

/**
 * Includes a restaurant block, which fills with location page data
 * @param  string $identifier The block name, also the name of the include file
 * @param  int    $page_id    Optional - The page ID that holds the data to populate the block with
 *                                       Will default get_the_ID(), or the page id within a loop.
 * @return null
 */
function include_restaurant_block( $identifier, $page_id = null ) {
    if ( ! isset( $page_id ) ) $page_id = get_the_ID();
    $file_path = get_partials_directory_uri() . '/restaurant-blocks/' . $identifier . '.php';
    if ( file_exists( $file_path ) ) {
        include $file_path;
    }
}

/**
 * Includes restaurants list partial with the class modfier suffix
 * based on location param.
 * @param  string $location File location of partial usage
 * @return null
 */
function include_restaurants_list( $location = null ) {
    $list_location_modifier_suffix = ( $location ) ? $location : 'default';
    include get_partials_directory_uri() . '/restaurants-list.php';
}

/**
 * Retrieve an array of all the location categories and their meta data
 * @return array<WP_Term> List of all non-empty categories - a location taxonomy
 */
function get_location_categories() {
    return get_terms( [
        'taxonomy' => 'location_categories',
        'hide_empty' => true,
        'exclude' => array( 22 ),
    ] );
}

/**
 * Returns an array of locations, optionally filtered by a specific location category
 * @param  string|null      $location_category_slug  The optional slug of the location category
 * @return array<WP_Object>                          The list of posts under the location category
 */
function get_restaurants( $location_category_slug = null ) {
    $args = [
        'posts_per_page' => -1,
        'post_type' => 'locations',
        'order' => 'ASC',
        'orderby' => 'title',
    ];
    if ( $location_category_slug ) {
        $args['tax_query'] = [
            [
                'taxonomy' => 'location_categories',
                'field' => 'slug',
                'terms' => $location_category_slug,
            ]
        ];
    }
    return get_posts( $args );
}

/**
 * Builds the panel array that is used by templates/partials/panel.php.
 * @return array                  Utilised by the panel partial.
 */
function gift_card_panel() {
    return [
        'background' => 'decoration--small',
        'background_colour' => 'pink-dark',
        'background_image' => null,
        'background_image_type' => null,
        'content_position' => 'top',
        'content_type' => 'text',
        'panel_title' => 'Gift card',
        'panel_title_first_line_arched' => false,
        'panel_title_second_line' => false,
        'panel_text' => get_field( 'gift_card_text' ),
        'newsletter_text' => null,
        'panel_buttons' => [
            [
                'url' => get_field( 'gift_card_url' ),
                'text' => 'Buy now',
            ]
        ]
    ];
}
