<?php

use \Aubaine\Crm\Fishbowl;

function generate_random_string( $length = 10 ) {
    return substr( str_shuffle( str_repeat( $x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil( $length / strlen( $x ) ) ) ), 1, $length );
}

function is_spam_bot() {
    // If a honeypot is present and it's filled in, then this was posted by a spam bot
    return ( isset( $_POST['hp'] ) && strlen( $_POST['hp'] ) > 0 );
}

function simple_validation( $inputs ) {
    $required_fields = ['newsletter-email'];
    $errors = [];
    foreach( $required_fields as $key => $input ) {
        if ( null === $input ) {
            $errors[ $key ] = 'You must enter a value in this field';
        }
    }
    return $errors;
}

function is_duplicate_submission() {
    $message_identity = get_identity_from_post( $_POST );
    $session_message_identity = isset( $_SESSION['message_identity'] ) ? $_SESSION['message_identity'] : '';
    if ( $message_identity === $session_message_identity ) return true;
}

function store_message_identity( $message_identity ) {
    $_SESSION['message_identity'] = $message_identity;
}

function get_identity_from_post( $post ) {
    return md5( implode( '', $post ) );
}

function old( $field_name, $post_object ) {
    return ( isset( $post_object['data'][ $field_name ] ) )
        ? $post_object['data'][ $field_name ]
        : '';
}

function sanitise_post_data( $post_data ) {
    $post_object = [];
    foreach( $post_data as $key => $data ) {
        $post_object[ $key ] = htmlspecialchars( $data );
    }
    return $post_object;
}

function handle_post() {
    $post_object = [];
    if ( ! isset( $_POST ) ) return $post_object;
    if ( is_spam_bot() ) return $post_object;
    if ( isset( $_POST['newsletter-signup'] ) ) {
        // We check message identity to prevent duplicate identical form submissions
        if ( is_duplicate_submission() ) return $post_object;
        store_message_identity( get_identity_from_post( $_POST ) );
        $post_object['inputs'] = sanitise_post_data( $_POST );
        if ( $errors = simple_validation( $post_object['inputs'] ) ) {
            $post_object['errors'] = $errors;
            return $post_object;
        }

        $fishbowl = new Fishbowl();
        $fishbowl->setListID( FISHBOWL_NEWSLETTER_LIST_ID );
        $fishbowl->setEmailAddress( $_POST['newsletter-email'] ); // @TODO Do not trust user input!! Don't use this raw!
        $fishbowl->subscribe();
        if ( ! $fishbowl->getResponseStatus() ) {
            $post_object['errors']['general'] = 'We were unable to process the submission. Please try again.';
            return $post_object;
        }
        $post_object['success'] = true;
    }
    return $post_object;
}
$post_object = handle_post();
