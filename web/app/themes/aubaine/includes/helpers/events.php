<?php

/**
 * Builds an array of event page content for each location.
 *
 * @return array Events information of each location.
 */
function location_events_content() {
    $locations = get_restaurants();
    $events = [];
    foreach ( $locations as $key => $location ) {
        if ( get_field( 'events_enabled', $location->ID ) ) {
            $events[ $location->ID ] = [
                'location_title' => $location->post_title,
                'address' => get_field('address', $location->ID),
                'description' => get_field( 'events_information', $location->ID ),
                'capacities' => location_capacity( $location->ID ),
                'design_my_night_venue_id' => get_field( 'design_my_night_venue_id', $location->ID ),
                'location_image' => get_field( 'locations_block_image', $location->ID ),
            ];
        }
    }
    return $events;
}

/**
 * Builds array of capacity information about areas within a location.
 *
 * @param  int    $location_id ID of the single location post.
 * @return array               Location capacity information.
 */
function location_capacity( $location_id ) {
    $venue_areas = [];
    if ( $areas = get_field( 'capacity_venue_areas', $location_id ) ) {
        foreach ( $areas as $area ) {
            $venue_areas[] = [
                'area_title' => $area['venue_area'],
                'capacity' => $area['capacity_information']
            ];
        }
    }
    return $venue_areas;
}

/**
 * Include an icon list markup partial with all relevent
 * variables added. This includes adding capacity info
 * onto the locations contact info list items.
 *
 * @param  int   $location_id ID of the single location post.
 * @param  array $location    Events information of each location.
 * @return null
 */
function display_location_details( $location_id, $location, $column_count = 1 ) {
    // The following $list_ variables are required within the icon-list.php partial
    $list_enabled = 1;
    $list_items = get_field( 'contact_items', $location_id );
    $list_disabled_text = get_field( 'contact_disabled_text', $location_id );
    $list_columns_class = ( $column_count > 1 ) ? ' u-columns--' . $column_count : '';

    $unwanted_contact_details = [
        'phone',
        'email',
    ];
    $list_items = remove_unwanted_contact_details( $list_items, $unwanted_contact_details );

    $list_items[] = [
        'icon' => 'group',
        'text' => build_capacity_body( $location ),
    ];
    include get_partials_directory_uri() . '/icon-list.php';
}

/**
 * Generate list for each location area with respective
 * capacity information
 *
 * @param  array $location Events information of each location.
 * @return string Markup of location capacity information.
 */
function build_capacity_body( $location ) {
    $html = '<hr><ul class="o-list o-list--bare">';
    foreach ( $location['capacities'] as $area ) {
        $html .= sprintf(
            '<li class="o-list__item">' .
                '<p>%1$s:<br>%2$s</p>' .
            '</li>',
            $area['area_title'],
            $area['capacity']
        );
    }
    $html .= '</ul>';
    return $html;
}

/**
 * Array of enquiry types along with their respective Design my
 * night type ID
 * @return array Array of enquiry types
 */
function enquiry_type_options() {
    return [
        'Restaurant Group Booking' => '587a4cf3c71620b305468d45',
        'Birthday' => '587a4d56c71620447f468eb8',
        'Engagement Party' => '587a4de7c716202d09468c77',
        'Wedding' => '587a4e1fd5c588e44ac236fa',
        'Breakfast' => '587a4e65c716206208468cfe',
        'Exclusive Hire' => '587a4e9ed5c5881c4bc236bc',
        'Brunch' => '587a6c4bd5c588545bc23716',
        'Drinks &amp; Canapes' => '587a6c8cc716204a19468ce6',
        'Baby Shower' => '587a6f15d5c588035cc23721',
        'Sit Down Lunch' => '587a6f15d5c588035cc23722',
        'Sit Down Dinner' => '587a6f5dd5c588d15cc236c2',
        'Other' => '587c8e6cd5c588562650d161',
    ];
}

/**
 * Checks if any location has a Design my night venue ID
 * set. Used for conditional wrappers of the enquiry form
 * @param  array    $event_locations Array of data about each location.
 * @return boolean                   If ID found, return true
 */
function has_venue_ids( $event_locations ) {
    foreach ( $event_locations as $location ) {
        if ( isset( $location['design_my_night_venue_id'] ) ) {
            return true;
        }
    }
}

/**
 * Build array of enquiry booking timeslots from 8am - 8pm
 * @return array Timeslots for enquiries
 */
function generate_enquiry_timeslot_options() {
    $minute_intervals = ['00', '15', '30', '45'];
    $times = [];
    for ( $hour = 8; $hour < 20; $hour++ ) {
        foreach ( $minute_intervals as $interval ) {
            $times[] = two_sf( $hour ) . ':' . $interval;
        }
    }
    $times[] = '20:00'; // The loop above doesn't apply the final 20:00 time
    return $times;
}
function generate_selfridges_timeslot_options() {
    $minute_intervals = ['00', '15', '30', '45'];
    $times = [];
    for ( $hour = 9; $hour < 11; $hour++ ) {
        foreach ( $minute_intervals as $interval ) {
            $times[] = two_sf( $hour ) . ':' . $interval;
        }
    }
    $times[] = '11:00'; // The loop above doesn't apply the final times.
    $times[] = '11:15'; // The loop above doesn't apply the final times.
    $times[] = '11:30'; // The loop above doesn't apply the final times.
    return $times;
}

/**
 * Forces an integer to always be two significant zeroes by padding the left
 * side with 0's
 * @param  int    $hour Hour to be kept as 2 significant numbers
 * @return string       Formatted string with leading zeroes if required
 */
function two_sf( $hour ) {
    return str_pad( $hour, 2, '0', STR_PAD_LEFT );
}
