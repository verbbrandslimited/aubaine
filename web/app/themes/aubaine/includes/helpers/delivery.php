<?php

/**
 * Build list of delivery restaurants markup for
 * shortcode usage
 * @return string Markup of delivery restaurants list
 */
function delivery_restaurants_list_shortcode() {
    $html = '<ul class="o-list o-list--bare u-text-center u-margin-top-large">';
    foreach ( build_delivery_restaurants_list() as $location ) {
        $html .= '<li class="o-list__item u-margin-bottom-small">' .
                     '<a class="u-link u-color--primary" href="' . $location['url'] . '">' .
                         $location['title'] .
                     '</a>' .
                 '</li>';
    }
    $html .= '</ul>';

    return $html;
}
add_shortcode( 'delivery_restaurants_list', 'delivery_restaurants_list_shortcode' );

/**
 * Builds array of deliveroo links for locations
 * which have deliveroo delivery method enabled.
 * @return array Delieroo links with respective location title
 */
function build_delivery_restaurants_list() {
    $locations = get_restaurants();
    $delivery_list = [];
    foreach ( $locations as $location ) {
        if ( $deliveroo_link = get_deliveroo_link( $location ) ) {
            if ( get_field( 'delivery_enabled', $location->ID ) ) {
                $delivery_list[] = [
                    'title' => $location->post_title,
                    'url' => $deliveroo_link,
                ];
            }
        }
    }
    return $delivery_list;
}

/**
 * Runs through the passed location delivery links
 * repeater to see if a deliveroo method is set. If
 * so, returns the URL.
 * @param  array   $location ACF repeater of delivery links.
 * @return string            URL of deliveroo link.
 */
function get_deliveroo_link( $location ) {
    if ( $delivery_links = get_field( 'delivery_links', $location->ID ) ) {
        foreach ( $delivery_links as $link ) {
            if ( 'deliveroo' === $link['type'] ) {
                return $link['url'];
            }
        }
    }
}
