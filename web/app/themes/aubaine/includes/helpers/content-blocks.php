<?php

/**
 * Get the correct layout block item classes from the blocks background type
 * @param  string $background The type of background for the block
 * @return string             The corresponding required classes
 */
function get_content_item_classes( $background ) {
    $item = 'o-layout-block__item';
    if ( 'none' === $background['type'] ) {
        $item .= ' o-layout-block__item--content-only';
    } else if ( 'decoration' === $background['type'] ) {
        $item .= ' o-layout-block__item--decorative';
    } else if ( 'decoration--small' === $background['type'] ) {
        $item .= ' o-layout-block__item--decorative o-layout-block__item--decorative-small';
    } else if ( 'image' != $background['type'] ) {
        $item .= ' o-layout-block__item--bg-color';
    }
    return $item;
}

/**
 * Retrieve required styling and classes for a layout background, based on its type
 * and given image or colour data
 * @param  array $background Contains the type, image, colour and motif, if required
 * @return array<string>     The styles and classes to apply to the layout background
 */
function get_layout_background_tags( $background ) {
    $classes = '';
    $style = '';
    $new_style = null;
    if ( 'colour' === $background['type'] ||
         'motif' === $background['type'] ||
         'decoration--small' === $background['type'] ||
         'decoration' === $background['type'] ) {
        $classes .= ' o-layout-block__background--' . $background['colour'];
    }
    if ( 'motif' === $background['type'] ) {
        $classes .= ' o-layout-block__background--motif';
    }
    if ( 'decoration--small' === $background['type'] ) {
        $classes .= ' o-layout-block__background--nm';
    }
    if ( 'image' === $background['type'] ) {
        $new_style = 'background-image: url( ' . $background['image'] . ' )';
    }
    if ( $new_style ) {
        $style = ' style="';
        $style .= $new_style;
        $style .= '"';
    }
    return [
        'classes' => $classes,
        'style' => $style,
    ];
}

/**
 * Return the class modifier to position the layout content in its block
 * @param  string $content_position Location, such as top or bottom
 * @return string                   The corresponding modifier class
 */
function get_content_position( $content_position ) {
    $position = '';
    if ( 'bottom' === $content_position ) {
        $position = ' o-layout-block__content--bottom';
    }
    return $position;
}

/**
 * Unsets contact details array items that are passed in
 * as the contact details full array is used in multiple
 * areas across the site
 * @param  array $list_items               ACF repeater array of contact details
 * @param  array $unwanted_contact_details List of unwanted contact details
 * @return array                           Contact details array with removed details
 */
function remove_unwanted_contact_details( $list_items, $unwanted_contact_details ) {
    foreach ( $list_items as $key => $item ) {
        if ( in_array( $item['type'], $unwanted_contact_details ) ) {
            unset( $list_items[ $key ] );
        }
    }
    return $list_items;
}

/**
 * If background type is blog or image, return light button class.
 * @param  string $background_type  Panel background type.
 * @return string                   Light button class.
 */
function get_panel_button_colour_class( $background_type ) {
    if ( in_array( $background_type, ['image', 'colour', 'decoration', 'decoration--small', 'motif'] ) ) return ' c-btn--light';
}

/**
 * Returns a HTML string which adds an id to a block for hash
 * anchor navigational purposes
 * @param  string $block_id     ACF id field.
 * @return string               HTML of an ID decloration with block ID added.
 */
function scrollToId( $block_id = null ) {
    return ( $block_id ) ? ' id="' . $block_id . '"' : '';
}
