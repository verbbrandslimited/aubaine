<?php

/**
 * Gets the flexible content template URI.
 *
 * This is used to return a valid URI of the template when looping through the
 * flexible content blocks and returns the correctly named template. It should
 * be used in any template (usually the `page.php` file which is used to display
 * the flexible content blocks.
 *
 * Here is a code example of what the usage of this function would look like in
 * a template file:
 *
 *     while ( have_posts() ) : the_post();
 *         if ( have_rows( 'flexible_content' ) ) :
 *             while ( have_rows( 'flexible_content' ) ) : the_row();
 *                 $layout = get_flexible_content_template_uri( get_row_layout() );
 *                 if ( file_exists( $layout ) ) include $layout;
 *             endwhile;
 *         endif;
 *     endwhile;
 *
 * @param  string $template The name of the template
 * @return string           The absolute path of the template
 */
function get_flexible_content_template_uri( $template ) {
    $template = str_replace( '_', '-', $template );
    return get_flexible_content_directory_uri() . '/' . $template . '.php';
}

/**
 * Validate an ACF checkbox that has only a single value, and return that value if it exists
 * @param  array  $checkbox_object The ACF checkbox object as pulled from get_field()
 * @return string|null             The value of the desired checkbox object item
 */
function acf_single_value_checkbox( $checkbox_object ) {
    if ( $checkbox_object && is_array( $checkbox_object ) && array_key_exists( 0, $checkbox_object ) ) {
        return $checkbox_object[0];
    }
}

/**
 * Checks if an ACF repeater item has a particular field set
 * @param  string $field    The field name
 * @param  array  $repeater The repeater object as pulled from get_field()
 * @return boolean          Whether the field exists in the repeater or not
 */
function acf_repeater_item_has_field( $field, $repeater ) {
    return array_key_exists( $field, $repeater );
}

/**
 * Checks if an ACF repeater item has a multiple fields set
 * @param  array $field    The field names as strings
 * @param  array  $repeater The repeater object as pulled from get_field()
 * @return boolean          Whether the fields all exist in the repeater or not
 */
function acf_repeater_item_has_fields( $fields, $repeater ) {
    return ! array_diff_key( array_flip( $fields ), $repeater );
}

/**
 * Returns a HTML style tag with a background image in if one exists
 * @param  string $background_image The image, as gotten from `get_field()` on an image field
 * @return string                   The style tag, with prefixed space
 */
function acf_get_background_style( $background_image ) {
    if ( $background_image ) {
        return ' style="background-image: url( \'' . $background_image . '\' )"';
    }
    return '';
}