<?php

/**
 * Include all peek pages into the top of each page
 * @return null
 */
function include_all_peek_pages() {
    foreach ( glob( get_partials_directory_uri() . '/peek-pages/*.php' ) as $filename ) {
        include $filename;
    }
}
