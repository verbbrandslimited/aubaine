<?php

/**
 * Returns the settings for each panel of
 * the blog page.
 * @return array Panel settings
 */
function get_blog_panels_config() {
    return [
        '1' => [
            'background_type' => 'image',
            'background_colour' => 'grey',
            'background_image_type' => 'square',
            'content_position' => 'bottom',
        ],
        '2' => [
            'background_type' => 'colour',
            'background_colour' => 'blue-dark',
            'background_image_type' => 'square',
            'content_position' => 'center',
        ],
        '3' => [
            'background_type' => 'colour',
            'background_colour' => 'green',
            'background_image_type' => 'square',
            'content_position' => 'center',
        ],
        '4' => [
            'background_type' => 'colour',
            'background_colour' => 'pink-dark',
            'background_image_type' => 'thin',
            'content_position' => 'center',
        ],
        '5' => [
            'background_type' => 'image',
            'background_colour' => 'grey',
            'background_image_type' => 'thin',
            'content_position' => 'center',
        ],
        '6' => [
            'background_type' => 'image',
            'background_colour' => 'grey',
            'background_image_type' => 'square',
            'content_position' => 'bottom',
        ],
        '7' => [
            'background_type' => 'colour',
            'background_colour' => 'grey',
            'background_image_type' => 'square',
            'content_position' => 'center',
        ],
    ];
}

/**
 * Builds the panel array that is used by templates/partials/panel.php.
 * @param  int      $panel_number The index of panel within the blog while loop.
 * @return array                  Utilised by the panel partial.
 */
function generate_blog_panel_array( $panel_number ) {
    $background_image = get_blog_panel_tile_background_image( $panel_number );
    return [
        'background' => get_blog_panel_type( $panel_number, $background_image ),
        'background_colour' => get_panel_setting( $panel_number, 'background_colour' ),
        'background_image' => $background_image,
        'background_image_type' => get_panel_setting( $panel_number, 'background_image_type' ),
        'content_position' => get_panel_setting( $panel_number, 'content_position' ),
        'content_type' => 'blog',
        'panel_title' => get_the_title(),
        'panel_title_first_line_arched' => false,
        'panel_title_second_line' => false,
        'panel_text' => null,
        'newsletter_text' => null,
        'panel_buttons' => [
            [
                'url' => get_permalink(),
                'text' => 'Read more',
            ]
        ]
    ];
}

/**
 * If a blog page has less than 7 tiles then
 * we need to retain the layout. To do this we
 * generarte an empty tile.
 * @param  int    $panel_number The index of panel within the blog while loop.
 * @return array                To create an empty panel.
 */
function generate_empty_blog_panel_array( $panel_number ) {
    return [
        'background' => 'none',
        'background_colour' => get_panel_setting( $panel_number, 'background_colour' ),
        'background_image' => null,
        'content_position' => null,
        'content_type' => 'blog',
        'panel_title' => null,
        'panel_text' => null,
        'panel_title_first_line_arched' => false,
        'panel_title_second_line' => false,
        'newsletter_text' => null,
        'panel_buttons' => null,
    ];
}

/**
 * Returns desired panel setting.
 * @param  int    $panel_number The index of panel within the blog while loop.
 * @param  string $setting      Requested setting.
 * @return string               Setting value.
 */
function get_panel_setting( $panel_number, $setting ) {
    $panels_config = get_blog_panels_config();
    if ( $panels_config[ $panel_number ][ $setting ] ) return $panels_config[ $panel_number ][ $setting ];
}

/**
 * Gets background image URL based on the panel and
 * it's respective tile image type.
 * @param  int    $panel_number The index of panel within the blog while loop.
 * @return string               Source of background image
 */
function get_blog_panel_tile_background_image( $panel_number ) {
    $image_type = get_panel_setting( $panel_number, 'background_image_type' );
    if ( $background_image = get_field( 'tile_image_' . $image_type , get_the_ID() ) ) {
        return $background_image;
    }
}

/**
 * Due to the nature of the blog page design, each panel
 * is either a colour or image panel. This is set based
 * on the index of the panel within the loop. It also checks
 * if an image is avaible. If not, reverts to colour.
 * @param  int          $panel_number       The index of panel within the blog while loop.
 * @param  string|bool  $background_image   String of image URL else false.
 * @return string                           Panel type.
 */
function get_blog_panel_type( $panel_number, $background_image ) {
    $panels_config = get_blog_panels_config();
    if ( $panels_config[ $panel_number ]['background_type'] === 'image' && $background_image ) return 'image';
    return 'colour';
}

/**
 * Build blog categories options. Both desktop anchors and
 * mobile dropdowns are created and printed.
 *
 * @todo Active state of currently selected category (similar to menus).
 * @return array Categories for both mobile and desktop.
 */
function get_blog_categories() {
    $blog_categories = ['desktop' => '', 'mobile' => ''];
    foreach ( get_categories() as $cat ) {
        $blog_categories['mobile'] .= sprintf(
            '<option class="c-menu-location c-menu-location--mobile" ' .
            'value="category/%1$s">%2$s</option>',
            $cat->slug,
            $cat->name
        );
        $blog_categories['desktop'] .= sprintf(
            '<a class="c-menu-location c-menu-location--desktop ' .
            'c-btn c-btn--dim c-btn--line" ' .
            'href="blog/category/%1$s">%2$s</a>',
            $cat->slug,
            $cat->name
        );
    }
    return $blog_categories;
}
