<?php

use \Aubaine\Menus;
use \Aubaine\LocationObject;

/**
 * Retrieve the menu data associated with each of the Menu post types
 * @param  boolean $sorted Whether to sort the locations before returning the object
 * @return Menus           Object containing all LocationObjects for all Menus
 */
function get_menu_data( $sorted ) {
    $menu_data = new Menus();

    $args = [
        'post_type' => 'menus',
        'post_status' => 'publish',
    ];
    $query = new WP_Query( $args );

    while ( $query->have_posts() ) {
        $query->the_post();
        $location_title = get_the_title();
        if ( ! $menu_position = get_field( 'menu_position' ) ) {
            $menu_position = 0;
        }
        $menus = get_field( 'menus' );

        $menu_data->add_location( new LocationObject(
            $location_title,
            $menu_position,
            $menus
        ) );
    }
    wp_reset_query();
    $menu_data->sort_menu_by_location();
    return $menu_data;
}

function get_slug( $key ) {
    return sanitize_title( $key );
}

function get_location_options( $menu_data, $initial_menu = null ) {
    $location_options = ['desktop' => '', 'mobile' => ''];
    foreach ( $menu_data->get_all_locations() as $location_key => $location ) {
        $location_slug = get_slug( $location->get_name() );
        $active = ['desktop' => '', 'mobile' => ''];
        if ( is_queried_location( $initial_menu, $location_slug ) || is_default_active_location( $initial_menu, $location_key ) ) {
            $active['desktop'] = ' c-btn--active';
            $active['mobile'] = ' selected="selected"';
        }
        $location_options['mobile'] .= sprintf(
            '<option class="c-menu-location c-menu-location--mobile" ' .
            'data-location="%2$s" value="%2$s"%1$s>%3$s</option>',
            $active['mobile'],
            $location_slug,
            $location->get_name()
        );
        $location_options['desktop'] .= sprintf(
            '<div class="c-menu-location c-menu-location--desktop ' .
            'c-btn c-btn--dim c-btn--line%1$s" data-location="%2$s">%3$s </div>',
            $active['desktop'],
            $location_slug,
            $location->get_name()
        );
    }
    return $location_options;
}

/**
 * Get a menu location and optionally also a menu name from query params
 * @return array The menu location and optional menu name
 */
function get_menu_from_query_params() {
    if ( $loc = get_query_var( 'loc', null ) ) {
        return $menu_query = [
            'location' => $loc,
            'menu' => get_query_var( 'menu', null ),
        ];
    }
}

function is_initial_location( $initial_menu, $location_slug, $location_key ) {
    return is_queried_location( $initial_menu, $location_slug ) ||
           is_default_active_location( $initial_menu, $location_key );
}

function is_initial_menu( $initial_menu, $location_slug, $menu_slug, $location_key, $menu_title ) {
    return is_queried_menu( $initial_menu, $location_slug, $menu_slug ) ||
           is_default_active_menu( $initial_menu, $location_key, $location_slug, $menu_title );
}

function is_queried_location( $menu_query, $location_slug ) {
    if ( $menu_query && array_key_exists( 'location', $menu_query ) ) {
        return $menu_query['location'] === $location_slug;
    }
}

function is_queried_menu( $menu_query, $location_slug, $menu_slug ) {
    if ( ! is_queried_location( $menu_query, $location_slug ) ) return false;
    if ( $menu_query && array_key_exists( 'menu', $menu_query ) ) {
        return $menu_query['menu'] === $menu_slug;
    }
}

function is_default_active_location( $menu_query, $location_key ) {
    return ( $menu_query === null && $location_key === 0 );
}

function is_default_active_menu( $menu_query, $location_key, $location_slug, $menu_key ) {
    if ( $menu_query['location'] !== null ) {
        if ( ! is_queried_location( $menu_query, $location_slug ) ) return false;
    } else {
        if ( ! is_default_active_location( $menu_query, $location_key ) ) return false;
    }
    return ( $menu_query['menu'] === null && $menu_key === 0 );
}

/**
 * Get the query vars required by Wordpress to use the menu query params logic
 * @return array The query vars required to be registered by the menus logic
 */
function get_menu_query_vars() {
    return ['loc', 'menu'];
}

/**
 * Return the available menu filters and their text
 * @return array The menu filter keys and values
 */
function get_menu_filters() {
    return [
        'vegetarian' => 'Vegetarian',
        'dairy-free' => 'Dairy Free',
        /** To be added soon
            'vegan' => 'Vegan',
            'gluten-free' => 'Gluten Free',
            'crustacean-free' => 'Crustacean Free',
            'nut-free' => 'Nut Free',
        **/
    ];
}

function get_data_from_menu_item_filters( $filters ) {
    $data_string = '';
    if ( is_array( $filters ) && count( $filters ) > 0 ) {
        foreach( $filters as $filter ) {
            $data_string .= ' data-' . $filter . '="true"';
        }
    }
    return $data_string;
}
