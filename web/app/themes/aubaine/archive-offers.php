<?php
get_header();
?>
    <?php while ( have_posts() ) : the_post() ?>
        <div class="c-detail-page">
            <div class="c-detail-page__header c-detail-header c-detail-header--loading">
                <?php include get_partials_directory_uri() . '/detail-pages/detail-header.php' ?>
            </div>
            <div class="o-wrapper">
                <?php if ( have_rows( 'flexible_content_blocks' ) ) : ?>
                    <?php while ( have_rows( 'flexible_content_blocks' ) ) : the_row() ?>
                        <?php $layout = get_flexible_content_template_uri( get_row_layout() ) ?>
                        <?php if ( file_exists( $layout ) ) include $layout ?>
                    <?php endwhile ?>
                <?php endif ?>
            </div>
        </div>
    <?php endwhile ?>

<?php get_footer() ?>
