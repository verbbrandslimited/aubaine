<?php
/**
 * The template for displaying our singular location pages for
 * location post type
 *
 * @package Hex Digital
 * @subpackage Aubaine
 * @since 2017
 */

get_header();

// Vars
$selected_times = get_field('available_times');
$days          = get_field('unavailable_days');
$locations    = get_field('select_locations');
$countDays = (int)0;
$new_times = "";



$locations !== '' || $locations !== null ? $locations = $locations : $locations = get_restaurants();
$selected_times == '' || $selected_times == null ? $new_times = generate_enquiry_timeslot_options() : $new_times = $selected_times;

?>
    <?php while ( have_posts() ) : the_post() ?>
      <div class="c-detail-page">
        <div class="c-detail-page__header c-detail-header c-detail-header--loading">
          <?php include get_partials_directory_uri() . '/detail-pages/detail-header.php' ?>
        </div>
        <div class="o-wrapper">
          <div class="c-booking c-booking--large">
            <div class="o-wrapper">
              <h2 class="c-content-block__header"><?= _e('Book A Table', 'aubaine'); ?></h2>
              <?php echo "<pre>"; var_dump($selected_times); echo "</pre>"; ?>
              <form action="" class="c-form--booking" method="post" target="_blank">
                <div class="c-booking__form c-form">
                  <div class="c-booking__column">
                    <div class="c-form__field c-form__field--set-width c-form__field--date c-form__field--color-white c-form__field--border-green u-margin-bottom-none date-picker">
                      <input class="c-form__input u-text-center js-datepicker2" data-disable="<?php if( $days ):foreach($days as $day): if($countDays > 0): echo ", "; endif; echo $day; $countDays++; endforeach; endif; ?>" name="booking-date" type="text" value="<?= date( 'j M Y' ) ?>">
                    </div>
                    <div class="o-button-group__item c-form__field c-form__field--dropdown js-dropdown time-picker">
                      <select class="c-form__select" name="booking-time" required>
                        <option selected disabled>Time</option>
                        <?php foreach ( $new_times as $time ) : ?>
                          <option value="<?= $time ?>"><?= date( 'h:i A', strtotime( $time ) ) ?></option>
                        <?php endforeach ?>
                      </select>
                    </div>
                    <div class="o-button-group o-button-group--tight">
                      <div class="o-button-group__item c-form__field c-form__field--set-width c-form__field--color-white c-form__field--border-green c-form__field--dropdown js-dropdown">
                        <?php include get_partials_directory_uri() . '/booking/party-options.php'; ?>
                      </div>
                      <div class="o-button-group__item c-form__field c-form__field--set-width c-form__field--color-white c-form__field--border-green c-form__field--dropdown js-dropdown">
                        <select class="c-form__select" name="booking-location">
                          <?php if ( $locations ) : ?>
                              <option selected disabled>Choose Location</option>
                              <?php foreach ( $locations as $location ) : ?>
                                  <?php if ( $url_prefix = get_field( 'url_prefix', $location->ID ) ) : ?>
                                      <option value="<?= $url_prefix ?>">at <?= $location->post_title ?></option>
                                  <?php endif ?>
                              <?php endforeach ?>
                          <?php endif ?>
                        </select>
                      </div>
                    </div>
                    
                    <button class="c-btn c-btn--light"
                      name="booking-submit"
                      type="submit">Book a table</button>
                      
                    <div class="c-booking__group-text">
                        For large groups and party bookings please enquire through our <a class="o-link o-link--naked" href="<?= get_permalink( get_page_id( 'events' ) ) ?>">Events page</a>.
                    </div>
                    <div class="c-booking__group-text">
                        Bookings are not available for Institut Français and Covent Garden Deli. Walk-ins only.
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    <?php endwhile ?>

<?php get_footer() ?>
