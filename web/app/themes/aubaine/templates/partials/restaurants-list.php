<?php $terms = get_location_categories() ?>

<div class="c-detail-list<?= ' c-detail-list--' . $list_location_modifier_suffix ?>">
    <div class="o-layout">
        <?php if ( $terms ) : ?>
            <?php foreach ( $terms as $term ) : ?>
                <div class="o-layout__item u-1/1 u-1/3@device">
                    <ul class="c-detail-list__list o-list o-list--bare u-text-center">
                        <li class="c-detail-list__item o-list__item"><?= $term->name ?></li>
                        <?php $locations = get_restaurants( $term->slug ) ?>
                        <?php if ( $locations ) : ?>
                            <?php foreach ( $locations as $location ) : ?>
                                <li class="c-detail-list__item o-list__item"><a class="c-detail-list__link" href="<?= get_permalink( $location->ID ) ?>"><?= $location->post_title ?></a></li>
                            <?php endforeach ?>
                        <?php endif ?>
                    </ul>
                </div>
            <?php endforeach ?>
        <?php endif ?>
    </div>
</div>
