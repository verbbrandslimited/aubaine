<style>
    .static-newsletter{
        text-align: center;
    }
    .static-newsletter h2{
        font-family: proxima-nova;
        font-size: 24px;
        color: #FFFFFF;
        letter-spacing: 2.61px;
        text-transform: uppercase;
    }
    .static-newsletter p{
        color: #FFFFFF;
        font-family: proxima-nova;
        font-size: 16px;
    }
    .static-newsletter .newsletter-button{
        display: inline-block;
        width: 230px;
        height: 51px;
        background-color: #FFFFFF;
        color: #A3BEA7;
        font-size: 12px;
        font-family: proxima-nova;
    }
</style>
<div class="static-newsletter">
    <h2>SIGN-UP TO OUR NEWSLETTER</h2>
    <p>Sign up to our email newsletter to be the first to hear about our latest offers, seasonal menus and upcoming events</p>
    <div class="newsletter-button">Sign Up Now</div>
</div>