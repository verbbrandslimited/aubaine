<?php
if ( ! isset( $page_id ) ) $page_id = get_the_ID();
$events_enabled = (int) acf_single_value_checkbox(
                                get_field( 'events_enabled', $page_id )
                            );
$events_disabled_text = get_field( 'events_disabled_text', $page_id );
$events_information = get_field( 'events_information', $page_id );
?>

<?php if ( 1 === $events_enabled && $events_information ) : ?>
    <?= $events_information ?>
    <a class="c-btn c-btn--primary c-btn--line" href="<?= get_permalink( get_page_id( 'events' ) ) ?>">More info</a>
<?php else : ?>
    <p><?= ( $events_disabled_text ? : 'Currently there are no Events available' ) ?></p>
<?php endif ?>
