<?php
if ( ! isset( $page_id ) ) $page_id = get_the_ID();
$opening_hours_enabled = (int) acf_single_value_checkbox(
                                   get_field( 'opening_hours_enabled', $page_id )
                               );
$opening_hours_disabled_text = get_field( 'opening_hours_disabled_text', $page_id );
$opening_times = get_field( 'opening_times', $page_id );
?>

<?php if ( 1 === $opening_hours_enabled && $opening_times ) : ?>
    <ul class="o-list o-list--bare u-color--grey-dark">
        <?php foreach( $opening_times as $item ) : ?>
            <?php if ( acf_repeater_item_has_field( 'day', $item ) ) : ?>
                <li class="o-list__item">
                    <p><?= $item['day'] ?>: <?= ( $item['time']
                                              ? '<span class="u-bold">' . $item['time'] . '</span>'
                                              : '' ) ?></p>
                </li>
            <?php endif ?>
        <?php endforeach ?>
    </ul>
<?php else : ?>
    <p><?= ( $opening_hours_disabled_text ? : 'There are currently no Opening Hours listed' ) ?></p>
<?php endif ?>
