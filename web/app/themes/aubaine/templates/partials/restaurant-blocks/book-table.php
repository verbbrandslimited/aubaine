<?php
if ( ! isset( $page_id ) ) $page_id = get_the_ID();
$book_table_enabled = (int) acf_single_value_checkbox(
                                get_field( 'book_table_enabled', $page_id )
                            );
$book_table_disabled_text = get_field( 'book_table_disabled_text', $page_id );
?>

<?php if ( 1 === $book_table_enabled ) : ?>
    <?php include get_partials_directory_uri() . '/booking/booking-stack.php' ?>
<?php else : ?>
    <p><?= ( $book_table_disabled_text ? : 'Bookings are currently disabled' ) ?></p>
<?php endif ?>
