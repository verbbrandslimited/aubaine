<?php
if ( ! isset( $page_id ) ) $page_id = get_the_ID();
$list_enabled = (int) acf_single_value_checkbox(
                             get_field( 'contact_enabled', $page_id )
                         );
$list_disabled_text = get_field( 'contact_disabled_text', $page_id );
$list_items = get_field( 'contact_items', $page_id );
$extra_links = get_field( 'extra_links', $page_id );

$unwanted_contact_details = [
    'events-phone',
    'events-email',
];
$list_items = remove_unwanted_contact_details( $list_items, $unwanted_contact_details );
?>

<?php include get_partials_directory_uri() . '/icon-list.php' ?>
