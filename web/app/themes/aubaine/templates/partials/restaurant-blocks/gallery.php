<?php
if ( ! isset( $page_id ) ) $page_id = get_the_ID();
$gallery_enabled = (int) acf_single_value_checkbox(
                             get_field( 'gallery_enabled', $page_id )
                         );
$gallery_images = get_field( 'gallery_images', $page_id );
?>

<?php if ( 1 === $gallery_enabled && $gallery_images ) : ?>
    <div class="c-carousel c-carousel--gallery swiper-container js-carousel--gallery">
        <div class="c-carousel__wrapper swiper-wrapper">
            <?php foreach ( $gallery_images as $image ) : ?>
                <?php if ( acf_repeater_item_has_field( 'url', $image ) ) : ?>
                    <div class="c-carousel__item swiper-slide">
                        <img alt="<?= $image['alt'] ? : '' ?>" class="c-gallery__image" src="<?= $image['url'] ?>">
                        <?php if ( acf_repeater_item_has_field( 'caption', $image ) ) : ?>
                            <span class="c-carousel__caption"><?= $image['caption'] ?></span>
                        <?php endif ?>
                    </div>
                <?php endif ?>
            <?php endforeach ?>
        </div>
        <div class="swiper-pagination"></div>
    </div>
<?php endif ?>
