<?php
if ( ! isset( $page_id ) ) $page_id = get_the_ID();
$delivery_enabled = (int) acf_single_value_checkbox(
                              get_field( 'delivery_enabled', $page_id )
                          );
$delivery_disabled_text = get_field( 'delivery_disabled_text', $page_id );
$delivery_text = get_field( 'delivery_text', $page_id );
$delivery_links = get_field( 'delivery_links', $page_id );
?>

<?php if ( 1 === $delivery_enabled && ( $delivery_text || $delivery_links ) ) : ?>
    <?php if ( $delivery_text ) : ?>
        <p><?= $delivery_text ?></p>
    <?php endif ?>
    <?php if ( $delivery_links ) : ?>
        <?php foreach( $delivery_links as $item ) : ?>
            <?php if ( acf_repeater_item_has_fields( ['url', 'text'], $item ) ) : ?>
                <a class="c-btn c-btn--primary u-margin-top" href="<?= $item['url'] ?>"><?= $item['text'] ?></a>
            <?php endif ?>
        <?php endforeach ?>
    <?php endif ?>
<?php else : ?>
    <p><?= ( $delivery_disabled_text ? : 'There are currently no Menus listed' ) ?></p>
<?php endif ?>
