<?php
if ( ! isset( $page_id ) ) $page_id = get_the_ID();
$menus_enabled = (int) acf_single_value_checkbox(
                           get_field( 'menus_enabled', $page_id )
                       );
$menus_disabled_text = get_field( 'menus_disabled_text', $page_id );
$menus_text = get_field( 'menus_text', $page_id );
$menus = get_field( 'menus', $page_id );
?>

<?php if ( 1 === $menus_enabled && ( $menus_text || $menus ) ) : ?>
    <?php if ( $menus_text ) : ?>
        <p><?= $menus_text ?></p>
    <?php endif ?>
    <?php if ( $menus ) : ?>
        <div class="o-button-group o-button-group--tight u-margin-top-large">
            <?php foreach( $menus as $menu ) : ?>
                <?php if ( acf_repeater_item_has_fields( ['url', 'text'], $menu ) ) : ?>
                    <a class="c-btn c-btn--primary c-btn--fixed o-button-group__item" href="<?= $menu['url'] ?>"><?= $menu['text'] ?></a>
                <?php endif ?>
            <?php endforeach ?>
        </div>
    <?php endif ?>
<?php else : ?>
    <p><?= ( $menus_disabled_text ? : 'There are currently no Menus listed' ) ?></p>
<?php endif ?>
