<nav class="c-navbar<?= nav_modifier() ?><?php is_page('events') == true ? '--events' : '' ?>">
    <div class="c-navbar__social">
        <?php include get_partials_directory_uri() . '/social-links.php' ?>
    </div>
    <div class="c-navbar__branding">
        <?php include get_partials_directory_uri() . '/site-branding.php' ?>
    </div>
    <div class="c-navbar__links">
        <?php get_wp_menu( 'top-menu', 'c-navbar__links-container' ) ?>
    </div>
    <div class="c-navbar__hamburger">
        <?php include get_partials_directory_uri() . '/hamburger.php' ?>
    </div>
</nav>
