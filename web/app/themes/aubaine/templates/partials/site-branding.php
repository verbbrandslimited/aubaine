<div class="c-site-branding">
    <a href="<?= get_site_url() ?>">
        <?= get_svg( 'logo', 'img', 'c-site-branding__logo' ) ?>
    </a>
</div>
