<?php if ( isset( $list_enabled ) && ( isset( $list_items ) || isset( $extra_links ) ) ) : ?>
    <ul class="o-list o-list--bare<?= $list_columns_class ?>">
        <?php if ( 1 === $list_enabled && ( $list_items || $extra_links ) ) : ?>
            <?php if ( $list_items ) : ?>
                <?php foreach( $list_items as $item ) : ?>
                    <?php if ( acf_repeater_item_has_field( 'text', $item ) ) : ?>
                        <li class="o-list__item">
                            <p>
                                <div class="o-media">
                                    <div class="o-media__img">
                                        <?= get_svg(
                                            get_svg_name_from_identifier( $item['icon'] ),
                                            'img',
                                            'c-content-block__list-icon'
                                        ) ?>
                                    </div>
                                    <div class="o-media__body">
                                        <?php if ( 'email' === $item['icon'] ) : ?>
                                            <a class="o-link o-link--naked" href="mailto:<?= antispambot( $item['text'] ) ?>"><?= antispambot( $item['text'] ) ?></a>
                                        <?php else : ?>
                                            <?= $item['text'] ?>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </p>
                        </li>
                    <?php endif ?>
                <?php endforeach ?>
            <?php endif ?>
        <?php else : ?>
            <p><?= ( $list_disabled_text ? : 'There is currently no Contact information listed' ) ?></p>
        <?php endif ?>
        <?php if ( isset( $extra_links ) ) : ?>
            <?php foreach( $extra_links as $item ) : ?>
                <?php if ( acf_repeater_item_has_fields( ['url', 'text'], $item ) ) : ?>
                    <a class="c-btn c-btn--primary u-margin-top" href="<?= $item['url'] ?>"><?= $item['text'] ?></a>
                <?php endif ?>
            <?php endforeach ?>
        <?php endif ?>
    </ul>
<?php endif ?>
