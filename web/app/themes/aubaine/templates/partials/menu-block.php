<?php if (!isset($location_key, $menu_title, $key, $category)) return ?>
<?php $for = $location_key . '-' . $menu_title . '-' .$key ?>
<div class="c-content-block o-accordion o-accordion--limit o-accordion--limit-device">
    <div class="o-accordion__container">
        <input class="o-accordion__checkbox" id="<?= $for ?>" type="checkbox"></input>
        <label class="c-content-block__header c-content-block__header--device-padding o-accordion__header<?= ( $category['heading_arch'] ) ? ' c-content-block__header--arc' : '' ?>"
               for="<?= $for ?>">
            <span<?= ( $category['heading_arch'] ) ? ' class="u-arc"' : '' ?>><?= $category['heading'] ?></span>
            <?php if ( array_key_exists( 'sub_heading', $category ) && strlen( $category['sub_heading'] ) > 0 ) : ?>
                <span class="c-content-block__sub-header"><?= $category['sub_heading'] ?></span>
            <?php endif ?>
            <div class="o-accordion__switch o-accordion__switch--closed">+</div>
            <div class="o-accordion__switch o-accordion__switch--open">-</div>
        </label>
        <div class="c-content-block__body o-accordion__content">
            <?php foreach ( $category['menu_items'] as $menu_item ) : ?>
                <?php $data_string = get_data_from_menu_item_filters( $menu_item['filters'] ) ?>
                <div class="c-menu-item c-filters__item"<?= $data_string ?>>
                    <div class="c-menu-item__content">
                        <p><span class="u-bold"><?= $menu_item['title'] ?></span><br>
                        <span><?= $menu_item['description'] ?></span></p>
                    </div>
                    <div class="c-menu-item__price">
                        <?= $menu_item['price'] ?>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
