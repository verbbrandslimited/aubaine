<?php $blog_categories = get_blog_categories() ?>
<div class="u-bg-color--white-dark u-padding-bottom-small">
    <div class="o-wrapper">
        <div class="c-form u-h-center u-hide-from-device" style="width:276px">
            <div class="c-form__field js-dropdown">
                <select class="c-menu-page__menu-locations c-form__select">
                    <?= $blog_categories['mobile'] ?>
                </select>
            </div>
        </div>
        <div class="u-hide-until-device u-text-center">
            <div class="o-button-group c-menu-page__menu-locations">
                <?= $blog_categories['desktop'] ?>
            </div>
        </div>
    </div>
</div>
