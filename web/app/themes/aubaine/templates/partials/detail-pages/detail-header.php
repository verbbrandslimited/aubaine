<?php
$header_background_style = acf_get_background_style( get_field( 'header_background' ) );
$header_background_colour = get_field( 'header_content_background_colour' );
$header_background_colour_class = ( $header_background_colour ) ? ' u-bg-color--' . $header_background_colour : '';
$header_content = get_field( 'header_content' );
$header_links = get_field( 'header_links' );
?>

<div class="c-detail-header__wrapper">
    <div class="c-detail-header__content u-text-center<?= $header_background_colour_class ?>">
        <h3 class="u-title u-title--large"><?php the_title() ?></h3>
        <?php if ( $header_content ) : ?>
            <p><?= $header_content ?></p>
        <?php endif ?>
        <?php if ( $header_links ) : ?>
            <div class="o-button-group u-margin-top-large o-button-group--spaced">
                <?php foreach ( $header_links as $item ) : ?>
                    <?php if ( acf_repeater_item_has_fields( ['url', 'text'], $item ) ) : ?>
                        <a class="c-btn c-btn--light c-btn--line" href="<?= $item['url'] ?>">
                            <?= $item['text'] ?>
                        </a>
                    <?php endif ?>
                <?php endforeach ?>
            </div>
        <?php endif ?>
    </div>
    <div class="c-detail-header__background-image"<?= $header_background_style ?>></div>
    <div class="c-hero__gradient c-hero__gradient--top"></div>
</div>
