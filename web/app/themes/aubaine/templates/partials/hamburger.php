<button class="c-hamburger<?= component_modifier( 'hamburger' ) ?>" type="button">
    <span class="c-hamburger__box">
        <span class="c-hamburger__inner"></span>
    </span>
</button>
