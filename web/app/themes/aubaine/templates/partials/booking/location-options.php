<?php $locations = get_restaurants() ?>
<?php if ( $locations ) : ?>
    <option selected disabled>Choose Location</option>
    <?php foreach ( $locations as $location ) : ?>
        <?php if ( $url_prefix = get_field( 'url_prefix', $location->ID ) ) : ?>
            <option value="<?= $url_prefix ?>">at <?= $location->post_title ?></option>
        <?php endif ?>
    <?php endforeach ?>
<?php endif ?>
