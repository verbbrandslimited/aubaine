<div class="c-booking c-booking--large">
    <div class="o-wrapper">
	    
        <form action="" class="c-form--booking" method="post" target="_blank">
            <div class="c-booking__form c-form">
	            <div class="c-booking__column">
                    <div class="c-form__field c-form__field--set-width c-form__field--date c-form__field--color-white c-form__field--border-green u-margin-bottom-none">
                        <input class="c-form__input u-text-center js-datepicker" name="booking-date" type="text" value="<?= date( 'j M Y' ) ?>">
                    </div>
                </div>
                <div class="c-booking__column">
                    <div class="o-button-group o-button-group--tight">
                        <div class="o-button-group__item c-form__field c-form__field--set-width c-form__field--color-white c-form__field--border-green c-form__field--dropdown js-dropdown">
                            <?php include get_partials_directory_uri() . '/booking/party-options.php'; ?>
                        </div>
                        <div class="o-button-group__item c-form__field c-form__field--set-width c-form__field--color-white c-form__field--border-green c-form__field--dropdown js-dropdown">
                            <select class="c-form__select" name="booking-location">
                                <?php include get_partials_directory_uri() . '/booking/location-options.php'; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="c-booking__column">
                    <button class="c-btn c-btn--light"
                            name="booking-submit"
                            type="submit">Book a table</button>
                </div>
                <div class="c-booking__column">
                    <div class="c-booking__group-text">
                        For large groups and party bookings please enquire through our <a class="o-link o-link--naked" href="<?= get_permalink( get_page_id( 'events' ) ) ?>">Events page</a>.
                    </div>
                    <div class="c-booking__group-text">
                        Bookings are not available for Institut Français and Covent Garden Deli. Walk-ins only.
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
