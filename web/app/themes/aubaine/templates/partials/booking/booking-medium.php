<div class="c-booking c-booking--medium">
    <div class="o-wrapper">
        <form action="" class="c-form--booking" method="post" target="_blank">
            <div class="c-form">
                <div class="o-button-group o-button-group--tight o-flex o-flex--justify-center">
	                <div class="o-button-group__item c-form__field c-form__field--set-width c-form__field--color-white c-form__field--border-green c-form__field--dropdown js-dropdown datepicker">
                        <?php include get_partials_directory_uri() . '/booking/datepicker.php'; ?>
                    </div>
                    <div class="o-button-group__item c-form__field c-form__field--set-width c-form__field--color-white c-form__field--border-white c-form__field--dropdown js-dropdown js-dropdown--fallup">
                        <?php include get_partials_directory_uri() . '/booking/party-options.php'; ?>
                    </div>
                    <div class="o-button-group__item c-form__field c-form__field--set-width c-form__field--color-white c-form__field--border-white c-form__field--dropdown js-dropdown js-dropdown--fallup">
                        <select class="c-form__select" name="booking-location">
                            <?php include get_partials_directory_uri() . '/booking/location-options.php'; ?>
                        </select>
                    </div>
                    <button class="o-button-group__item c-btn c-btn--light c-btn--fill-hover" name="booking-submit" type="submit">Book a table</button>
                </div>
            </div>
        </form>
    </div>
</div>
