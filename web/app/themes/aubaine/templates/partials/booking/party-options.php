<select class="c-form__select" name="booking-covers">
    <?php for ( $i = 1; $i < 9; $i++ ) : ?>
        <option value="<?= $i ?>"<?= ( $i === 2 ) ? ' selected' : '' ?>>
            <?= $i ?> <?= ( $i === 1 ) ? 'person' : 'people' ?>
        </option>
    <?php endfor ?>
</select>
