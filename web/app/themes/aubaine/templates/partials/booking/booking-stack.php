<div class="c-booking c-booking--stack">
    <form action="" method="post" target="_blank">
        <div class="c-form">
            <div class="o-button-group o-button-group--tight">
                <div class="o-button-group__item c-form__field c-form__field--dropdown js-dropdown">
                    <?php include get_partials_directory_uri() . '/booking/party-options.php'; ?>
                </div>
                <div class="o-button-group__item c-form__field c-form__field--dropdown js-dropdown">
                    <select class="c-form__select" name="booking-location">
                        <?php include get_partials_directory_uri() . '/booking/location-options.php'; ?>
                    </select>
                </div>
                <div class="o-button-group__item c-form__field c-form__field--date">
                    <input class="c-form__input js-datepicker" name="booking-date" type="text" value="<?= date( 'j M Y' ) ?>">
                </div>
                <div class="o-button-group__item u-margin-top-large">
                    <button class="c-btn c-btn--primary c-btn--block" name="booking-submit" type="submit">Book a table</button>
                </div>
            </div>
        </div>
    </form>
</div>
