<?php if ( ! $booking_tab_copy = get_field( 'booking_tab_copy' ) ) : ?>
    <?php $booking_tab_copy = 'See something you like?' ?>
<?php endif ?>
<div class="c-booking-tab">
    <div class="o-wrapper">
        <div class="c-booking-tab__inner">
            <p class="c-booking-tab__copy u-bold u-color--white">
                <?= $booking_tab_copy ?>
            </p>
            <span class="js-peek-link">
                <a class="c-btn c-btn--light"
                   href="<?= get_permalink( get_page_id( 'book-a-table' ) ) ?>">
                    Book now
                </a>
            </span>
        </div>
    </div>
</div>
