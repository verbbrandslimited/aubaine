<section class="c-hero">
    <div class="c-hero__inner">
        <h1 class="c-hero__title"><?= hero_title() ?></h1>
    </div>
</section>
