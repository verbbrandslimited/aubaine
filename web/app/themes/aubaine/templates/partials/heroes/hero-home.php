<section class="c-hero c-hero--home">
    <div class="c-hero__inner">
        <?php if ( have_rows('hero_carousel') ) : ?>
            <div class="c-carousel c-carousel--hero swiper-container">
                <div class="c-carousel__wrapper swiper-wrapper">
                     <?php while ( have_rows( 'hero_carousel' ) ) : the_row() ?>
                        <div class="c-carousel__item swiper-slide">
                            <?php $carousel_image = get_sub_field( 'background_image' ) ?>
                            <img alt="<?= $carousel_image['alt'] ?>" class="c-hero__background-image" src="<?= $carousel_image['url'] ?>">
                        </div>
                        <div class="c-hero__gradient c-hero__gradient--top"></div>
                        <div class="c-hero__gradient c-hero__gradient--bottom"></div>
                    <?php endwhile ?>
                </div>
            </div>
            <div class="c-hero__text-content">
                <div class="c-hero__branding c-hero__branding--main">
                    <?php include get_partials_directory_uri() . '/site-branding.php' ?>
                </div>
                <div class="c-hero__branding c-hero__branding--small">
                    <?php include get_partials_directory_uri() . '/site-branding.php' ?>
                </div>
                <div class="c-offerings">
                    <?php $i = 1 ?>
                    <?php while ( have_rows( 'hero_carousel' ) ) : the_row() ?>
                        <h2 class="c-offerings__item<?= ( $i === 1 ) ? ' c-offerings__item--active c-offerings__item--initial' : '' ?>">
                            <span class="c-offerings__line u-arc"><?= get_sub_field( 'first_line_text' ) ?></span>
                            <span class="c-offerings__line"><?= get_sub_field( 'second_line_text' ) ?></span>
                            <span class="c-offerings__line"><?= get_sub_field( 'third_line_text' ) ?></span>
                        </h2>
                        <?php $i++ ?>
                    <?php endwhile ?>
                </div>
                <div class="c-hero__booking c-hero__booking--down">
                    <?php include get_partials_directory_uri() . '/booking/booking-medium.php' ?>
                </div>
                <div class="c-hero__links">
                    <ul class="o-list o-list--bare">
                        <li class="o-list__item js-peek-link"><a class="o-list__link" href="<?= get_permalink( get_page_id( 'our-restaurants' ) ) ?>">Our restaurants</a></li>
                        <li class="o-list__item"><a class="o-list__link" href="<?= get_permalink( get_page_id( 'our-menus' ) ) ?>">Our menus</a></li>
                        <li class="o-list__item js-peek-link"><a class="c-btn c-btn--light" href="<?= get_permalink( get_page_id( 'book-a-table' ) ) ?>">Book a table</a></li>
                    </ul>
                </div>
            </div>
        <?php endif ?>
    </div>
</section>
