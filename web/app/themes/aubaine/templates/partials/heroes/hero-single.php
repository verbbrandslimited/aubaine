<?php $single_post_hero_background_color = ( get_field( 'blog_post_hero_colour' ) )
    ? ' u-bg-color--' . get_field( 'blog_post_hero_colour')
    : '' ?>
<section class="c-hero c-hero--single<?= $single_post_hero_background_color ?>">
    <div class="c-hero__inner">
        <h1 class="c-hero__title u-color--white"><?= get_the_title() ?></h1>
        <h4 class="c-hero__date u-text-center"><?= get_the_date( 'l F j, Y' ) ?></h4>
    </div>
    <div class="c-hero__back-button">
        <a class="u-link u-color--white" href="<?= home_url('/blog'); ?>">Back</a>
    </div>
    <div class="c-hero__tags">
    </div>
</section>
