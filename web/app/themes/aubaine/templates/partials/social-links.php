<div class="c-social">
    <a class="c-social__link" href="<?= get_social_link( 'facebook' ) ?>" rel="noopener noreferrer" target="_blank"><?= get_svg( 'facebook', 'img', 'c-icon c-icon--small' ) ?></a>
    <a class="c-social__link" href="<?= get_social_link( 'instagram' ) ?>" rel="noopener noreferrer" target="_blank"><?= get_svg( 'insta', 'img', 'c-icon c-icon--small' ) ?></a>
    <a class="c-social__link" href="<?= get_social_link( 'twitter' ) ?>" rel="noopener noreferrer" target="_blank"><?= get_svg( 'twitter', 'img', 'c-icon c-icon--small' ) ?></a>
    <a class="c-social__link" href="<?= get_social_link( 'pinterest' ) ?>" rel="noopener noreferrer" target="_blank"><?= get_svg( 'pinterest', 'img', 'c-icon c-icon--small' ) ?></a>
</div>
