<?php if ( $panel ) : ?>
    <?php $background['type'] = $panel['background'] ?>
    <?php $background['colour'] = ( isset( $panel['background_colour'] ) ? $panel['background_colour'] : '' ) ?>
    <?php $background['image'] = ( isset ( $panel['background_image'] ) ? $panel['background_image'] : '' ) ?>
    <?php $background['image_type'] = ( isset ( $panel['background_image_type'] ) ? $panel['background_image_type'] : '' ) ?>
    <?php $content_position = $panel['content_position'] ?>
    <?php $content_type = $panel['content_type'] ?>
    <?php $panel_title_first_line = $panel['panel_title'] ?>
    <?php $panel_title_first_line_arched = $panel['panel_title_first_line_arched'] ?>
    <?php $panel_title_second_line = $panel['panel_title_second_line'] ?>
    <?php $panel_text = ( isset ( $panel['panel_text'] ) ? $panel['panel_text'] : '' ) ?>
    <?php $newsletter_text = ( isset ( $panel['newsletter_text'] ) ? $panel['newsletter_text'] : '' ) ?>
    <?php $panel_buttons = $panel['panel_buttons'] ?>
    <?php $item = get_content_item_classes( $background ) ?>
    <?php $background_tags = get_layout_background_tags( $background ) ?>
    <?php $position = get_content_position( $content_position ) ?>
    <div class="<?= $item ?>">
        <div class="o-layout-block__background<?= $background_tags['classes'] ?>"<?= $background_tags['style'] ?>>
            <?php if ( 'image' === $background['type'] && 'blog' === $content_type ) : ?>
                <div class="o-layout-block__overlay o-layout-block__overlay--<?= $background['image_type'] ?>"></div>
            <?php endif ?>
            <?php if ( 'motif' === $background['type'] ) : ?>
                <?= get_svg( 'logo-roundel', 'img', 'c-icon c-icon--small c-icon--top-motif' ) ?>
                <?= get_svg( 'logo-roundel', 'img', 'c-icon c-icon--small c-icon--bottom-motif' ) ?>
            <?php endif ?>
            <?php if ( in_array( $background['type'], ['decoration', 'decoration--small'] ) ) : ?>
                <div class="o-layout-block__grid">
                    <div class="o-layout-block__grid-arc u-bg-color--<?= $background['colour'] ?>"></div>
                    <div class="o-layout-block__skirt o-layout-block__skirt--left">
                        <?php if ( 'decoration' === $background['type'] ) : ?>
                            <div class="o-layout-block__skirt-angle"></div>
                        <?php endif ?>
                    </div>
                    <div class="o-layout-block__skirt o-layout-block__skirt--right">
                        <?php if ( 'decoration' === $background['type'] ) : ?>
                            <div class="o-layout-block__skirt-angle"></div>
                        <?php endif ?>
                    </div>
                </div>
            <?php endif ?>
        </div>
        <div class="o-layout-block__content<?= $position ?>">
            <div class="c-text-block">
                <?php if ( $panel_title_first_line ) : ?>
                    <div class="c-text-block__header">
                        <div<?= ( $panel_title_first_line_arched ) ? ' class="u-arc"' : '' ?>><?= $panel_title_first_line ?></div>
                        <?php if ( $panel_title_second_line ) : ?>
                            <div class="c-text-block__title-cross<?= ( $panel_title_first_line_arched ) ? ' c-text-block__title-cross--under-arch' : '' ?>"></div>
                            <div><?= $panel_title_second_line ?></div>
                        <?php endif ?>
                    </div>
                <?php endif ?>
                <?php if ( 'text' === $content_type ) : ?>
                    <div class="c-text-block__content u-text-center">
                        <?php if ( $panel_text ) : ?>
                            <?= $panel_text ?>
                        <?php endif ?>
                    </div>
                <?php elseif ( 'newsletter' === $content_type ) : ?>
                    <div class="c-text-block__content u-text-center">
                        <form action="<?= site_url() ?>/newsletter" class="c-form" method="post">
                            <input name="newsletter-signup" type="hidden" value="1">
                            <input name="random-string" type="hidden" value="<?= generate_random_string() ?>">
                            <div class="c-form__field c-form__field--color-white c-form__field--border-white c-form__field--alt">
                                <?php if ( $newsletter_text ) : ?>
                                    <label class="c-form__label c-form__label" for="newsletter-email">
                                        <?= $newsletter_text ?>
                                    </label>
                                <?php endif ?>
                                <input class="c-form__input c-form__input--text js-form__input js-newsletter-input" id="newsletter-email" name="newsletter-email" type="email">
                            </div>
                            <div class="u-text-center">
                                <button disabled class="c-btn c-btn--line c-btn--primary c-btn--light js-newsletter-submit" type="submit">Sign up</button>
                            </div>
                        </form>
                    </div>
                <?php endif ?>
                <?php if ( $panel_buttons ) : ?>
                    <div class="c-text-block__buttons o-button-group">
                        <?php foreach ( $panel_buttons as $button ) : ?>
                            <?php if ( acf_repeater_item_has_fields( ['url', 'text'], $button ) ) : ?>
                                <a class="c-btn c-btn--primary c-btn--line o-button-group__item<?= get_panel_button_colour_class( $background['type'] ) ?>" href="<?= $button['url'] ?>"><?= $button['text'] ?></a>
                            <?php endif ?>
                        <?php endforeach ?>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
<?php endif ?>
