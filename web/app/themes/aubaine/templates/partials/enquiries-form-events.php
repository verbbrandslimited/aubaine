<?php if ( isset( $event_locations ) && has_venue_ids( $event_locations ) ) : ?>
    <form action="" method="post" target="_blank">
        <div class="c-form">
            <div class="o-button-group o-button-group--tight">
                <div class="o-button-group__item c-form__field c-form__field--dropdown js-dropdown">
                    <select class="c-form__select" name="enquiry-venue" required>
                        <option selected disabled>Select Location</option>
                        <?php foreach ( $event_locations as $location_id => $location ) : ?>
                            <?php if ( $location['design_my_night_venue_id'] ) : ?>
                                <option value="<?= $location['design_my_night_venue_id'] ?>"><?= $location['location_title'] ?></option>
                            <?php endif ?>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="o-button-group__item c-form__field c-form__field--dropdown js-dropdown">
                    <select class="c-form__select" name="enquiry-type" required>
                        <option selected disabled>Select Type</option>
                        <?php foreach ( enquiry_type_options() as $type => $value ) : ?>
                            <option value="<?= $value ?>"><?= $type ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="o-button-group__item c-form__field">
                    <label class="c-form__label" for="enquiry-size">Number of people</label>
                    <input class="c-form__input c-form__input--text js-form__input" id="enquiry-size" min="0" name="enquiry-size" required type="number">
                </div>
                <div class="o-button-group__item c-form__field c-form__field--date">
                    <input class="c-form__input js-datepicker" name="enquiry-date" required type="text" value="<?= date( 'j M Y' ) ?>">
                </div>
                <div class="o-button-group__item c-form__field c-form__field--dropdown c-form__field--time js-dropdown">
                    <select class="c-form__select" name="enquiry-time" required>
                        <option selected disabled>Select Timeslot</option>
                        <?php foreach ( generate_enquiry_timeslot_options() as $time ) : ?>
                            <option value="<?= $time ?>"><?= date( 'h:i A', strtotime( $time ) ) ?></option>
                        <?php endforeach ?>
                    </select>
                </div>

                <div class="o-button-group__item u-margin-top-large">
                    <button class="c-btn c-btn--primary c-btn--block" name="enquiry-submit" onClick="ga('send', 'event', { eventCategory: 'private dining enquiry', eventAction: 'click'})" type="submit">Enquire now</button>
                </div>
            </div>
        </div>
    </form>
<?php endif ?>
