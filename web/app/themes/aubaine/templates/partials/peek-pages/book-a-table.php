<div class="c-peek__page u-bg-color--green-dark" data-page="book-a-table">
    <div class="o-wrapper">
	    <div class="panel-title">
		    <h2>Book A Table</h2>
	    </div>
        <?php include get_partials_directory_uri() . '/booking/booking-large.php' ?>
    </div>
</div>