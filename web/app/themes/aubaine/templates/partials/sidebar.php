<div class="c-sidebar">
    <div class="c-sidebar__links-container">
        <?php get_wp_menu( 'top-menu', 'c-sidebar__links-list c-sidebar__links-list--main' ) ?>
        <?php get_wp_menu( 'sidebar-menu-slot-one', 'c-sidebar__links-list c-sidebar__links-list--extra' ) ?>
        <?php get_wp_menu( 'sidebar-menu-slot-two', 'c-sidebar__links-list c-sidebar__links-list--extra' ) ?>
    </div>
    <div class="c-newsletter c-newsletter--sidebar">
        <?php get_svg( 'logo-roundel', 'img', 'c-icon c-icon--small c-icon--top-motif c-sidebar__motif' ) ?>
        <style>
        .c-sidebar .static-newsletter{
            text-align: center;
        }
        .c-sidebar .static-newsletter h2{
            font-size: 24px;
            color: #FFFFFF;
            letter-spacing: 2.61px;
            text-transform: uppercase;
        }
        .c-sidebar .static-newsletter p{
            color: #FFFFFF;
            font-size: 16px;
        }
        .c-sidebar .static-newsletter .newsletter-button{
            display: inline-block;
            width: 230px;
            height: 51px;
            background-color: #FFFFFF;
            color: gray;
            font-size: 15px;
            text-transform: uppercase;
            line-height: 55px;
            font-weight: bold;
        }
        </style>
        <div class="static-newsletter">
            <h2>SIGN-UP TO OUR NEWSLETTER</h2>
            <p>Sign up to our email newsletter to be the first to hear about our latest offers, seasonal menus and upcoming events</p>
            <a href="<?php echo get_site_url(); ?>/subscribe/"><div class="newsletter-button">Sign Up Now</div></a>
        </div>
    </div>
</div>
