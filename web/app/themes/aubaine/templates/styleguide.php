<?php
/**
 * The template for displaying c-styleguide elements and components.
 *
 * @package Hex Digital
 * @subpackage Aubaine
 * @since 2017
 *
 * Template Name: Styleguide
 */

get_header() ?>

<section class="c-styleguide c-styleguide--colors">
    <div class="o-wrapper">
        <div class="c-styleguide__meta">
            <h2 class="c-styleguide__title c-styleguide__title--medium">Colours</h2>
        </div>
        <?php $colors = [
            "Tones" => [
                'black' => '#000000',
                'black-light' => '#333333',
                "grey-dark" => "#515151",
                "grey" => "#7c7c81",
                "grey-light" => "#a3a3a7",
                "grey-lighter" => "#c8c8cc",
                "white-darker" => "#e4e4e4",
                "white-dark" => "#f6f6f9",
                "white" => "#ffffff"
            ],
            "Colours" => [
                "green-dark" => "#adc6b0",
                "green" => "#c5d7c7",
                "pink-dark" => "#db9c97",
                "pink" => "#e5b9b6",
                "blue-dark" => "#b9c1d6",
                "blue" => "#ced3e2",
                "blue-light" => "#c6cde0",
                "yellow" => "#f5be73",
                "gold" => "#b79b35"
            ],
            "Theme Specific Colours" => [
                "primary" => "#7c7c81"
            ]
        ] ?>
        <?php foreach ( $colors as $type => $palette ) : ?>
            <div class="c-styleguide__part">
                <h3 class="c-styleguide__title c-styleguide__title--small"><?= $type ?></h3>
                <div class="c-styleguide__content">
                    <?php foreach ( $palette as $title => $hex ) : ?>
                        <div class="c-styleguide__color">
                            <span class="c-styleguide__swatch u-bg-color--<?= $title ?>"></span>
                            <span class="c-styleguide__text"><?= $title ?></span>
                            <span class="c-styleguide__text"><?= $hex ?></span>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</section>

<section class="c-styleguide c-styleguide--colors">
    <div class="o-wrapper">
        <div class="c-styleguide__meta">
            <h2 class="c-styleguide__title c-styleguide__title--medium">Typography</h2>
        </div>
        <div class="c-styleguide__part">
            <h3 class="c-styleguide__title c-styleguide__title--small">Typeface</h3>
            <div class="c-styleguide__part">
                <div class="c-styleguide__content">
                    <div class="c-styleguide__typeface c-styleguide__typeface--vinny-light">
                        <span class="c-styleguide__text">Vinny light</span>
                        <p>ABCDEFGHIJKLM<br>
                        NOPQRSTUVWXYZ<br>
                        abcdefghijklm<br>
                        nopqrstuvwxyz<br>
                        1234567890</p>
                    </div>
                    <div class="c-styleguide__typeface c-styleguide__typeface--vinny-bold">
                        <span class="c-styleguide__text">Vinny light</span>
                        <p class="">ABCDEFGHIJKLM<br>
                        NOPQRSTUVWXYZ<br>
                        abcdefghijklm<br>
                        nopqrstuvwxyz<br>
                        1234567890</p>
                    </div>
                    <div class="c-styleguide__typeface c-styleguide__typeface--altissima">
                        <span class="c-styleguide__text">Altissima</span>
                        <p class="">ABCDEFGHIJKLM<br>
                        NOPQRSTUVWXYZ<br>
                        abcdefghijklm<br>
                        nopqrstuvwxyz<br>
                        1234567890</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="c-styleguide__part">
            <h3 class="c-styleguide__title c-styleguide__title--small">Scale</h3>
            <div class="c-styleguide__content">
                <table class="c-styleguide__table">
                    <tr class="heading-row"><th>Size</th><th>REM</th><th>PX</th><th>Actual size</th></tr>
                    <tr><td>Small</td><td>0.75rem</td><td>12px</td><td><span class="u-content--small">Salmon spring roll</span></td></tr>
                    <tr><td>Base</td><td>0.9375rem</td><td>15px</td><td><span class="u-content--base">Salmon spring roll</span></td></tr>
                    <tr><td>Base Mobile</td><td>1rem</td><td>16px</td><td><span class="u-content--base-mobile">Salmon spring roll</span></td></tr>
                    <tr><td>Medium</td><td>2.25rem</td><td>36px</td><td><span class="u-content--medium">Salmon spring roll</span></td></tr>
                    <tr><td>Medium Mobile</td><td>2.0625rem</td><td>33px</td><td><span class="u-content--medium-mobile">Salmon spring roll</span></td></tr>
                    <tr><td>Large</td><td>3.25rem</td><td>52px</td><td><span class="u-content--large">Salmon spring roll</span></td></tr>
                    <tr><td>Large Mobile</td><td>2.5rem</td><td>40px</td><td><span class="u-content--large-mobile">Salmon spring roll</span></td></tr>
                </table>
            </div>
        </div>
        <div class="c-styleguide__part">
            <h3 class="c-styleguide__title c-styleguide__title--small">Body copy</h3>
            <div class="c-styleguide__content">
                <p class="u-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
        </div>
        <div class="c-styleguide__part">
            <h3 class="c-styleguide__title c-styleguide__title--small">Line height</h3>
            <div class="c-styleguide__content">
                <span class="c-styleguide__text">Line height for all heading and body text is <strong>1.3125</strong> times the font size.</span>
            </div>
        </div>
        <div class="c-styleguide__part">
            <h3 class="c-styleguide__title c-styleguide__title--small">Line length</h3>
            <div class="c-styleguide__content">
                <span class="c-styleguide__text">All body copy should be within an element with the <pre>.u-content</pre> class.<br>
                    This will keep line lengths between <strong>45 - 85</strong> characters.</span>
            </div>
        </div>
        <div class="c-styleguide__part">
            <h3 class="c-styleguide__title c-styleguide__title--small">Headings</h3>
            <h1 class="u-title--large">Heading one</h1>
            <h2 class="u-title--medium">Heading two</h2>
            <h2 class="u-title--medium u-arc">Heading arc</h2>
        </div>
    </div>
</section>

<section class="c-styleguide c-styleguide--motion">
    <div class="o-wrapper">
        <div class="c-styleguide__meta">
            <h2 class="c-styleguide__title c-styleguide__title--medium">Motion</h2>
        </div>
        <div class="c-styleguide__part">
            <div class="c-styleguide__content">
            </div>
        </div>
    </div>
</section>

<section class="c-styleguide c-styleguide--icons">
    <div class="o-wrapper">
        <div class="c-styleguide__meta">
            <h2 class="c-styleguide__title c-styleguide__title--medium">Icons &amp; Logos</h2>
        </div>
        <div class="c-styleguide__part">
            <h3 class="c-styleguide__title c-styleguide__title--small">Logos</h3>
            <div class="c-styleguide__content">
                <?= get_svg( 'logo' ) ?>
            </div>
        </div>
        <div class="c-styleguide__part">
            <h3 class="c-styleguide__title c-styleguide__title--small">Icons</h3>
            <div class="c-styleguide__content">
                <?php include get_partials_directory_uri() . '/social-links.php' ?>
            </div>
        </div>
    </div>
</section>

<section class="c-styleguide c-styleguide--hamburger">
    <div class="o-wrapper">
        <div class="c-styleguide__meta">
            <h2 class="c-styleguide__title c-styleguide__title--medium">Buttons</h2>
        </div>
        <div class="c-styleguide__part">
            <h3 class="c-styleguide__title c-styleguide__title--small">Hamburger</h3>
            <div class="c-styleguide__content">
                <?php include get_partials_directory_uri() . '/hamburger.php' ?>
            </div>
        </div>
        <div class="c-styleguide__part">
            <h3 class="c-styleguide__title c-styleguide__title--small">General</h3>
            <button type="button" class="c-btn c-btn--primary">Primary button</button><br><br>
            <button type="button" class="c-btn c-btn--line c-btn--primary">Line button</button><br><br>
            <button type="button" class="c-btn c-btn--line c-btn--primary" disabled>Line disabled button</button><br><br>
            <button type="button" class="c-btn c-btn--dim">Dim button</button><br><br>
            <div class="c-styleguide__content" style="background: #f5be73; padding: 15px;">
                <button type="button" class="c-btn c-btn--light">Light button</button>
            </div><br>
            <button type="button" class="c-btn c-btn--primary c-btn--condensed">Condensed</button><br><br>
        </div>
    </div>
</section>

<section class="c-styleguide">
    <div class="o-wrapper">
        <div class="c-styleguide__meta">
            <h2 class="c-styleguide__title c-styleguide__title--medium">Blocks</h2>
        </div>
        <div class="c-styleguide__part">
            <h3 class="c-styleguide__title c-styleguide__title--small">Content Block</h3>
            <div class="c-styleguide__content" style="max-width: 540px">
                <div class="c-content-block">
                    <div class="c-content-block__header">
                        Content Block
                    </div>
                    <div class="c-content-block__body">
                        <p>And here's some body content</p>
                        <p>Or some long content: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam aspernatur illo similique exercitationem quam fugit officia repellat laborum dignissimos nam totam, libero, voluptate, accusantium placeat debitis tempore a! Pariatur, quidem.</p>
                        <ul>
                            <li>And with</li>
                            <li>a list</li>
                            <li>in here</li>
                        </ul>
                        <o>
                            <li>Or ordered</li>
                        </o>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="c-styleguide c-styleguide--inputs">
    <div class="o-wrapper">
        <div class="c-styleguide__meta">
            <h2 class="c-styleguide__title c-styleguide__title--medium">Inputs</h2>
        </div>
        <div class="c-styleguide__part">
            <h3 class="c-styleguide__title c-styleguide__title--small">General</h3>
            <div class="c-styleguide__content">
                <div class="c-form">
                    <div class="c-form__field">
                        <label class="c-form__label" for="test">Basic</label>
                        <input class="c-form__input c-form__input--text js-form__input" type="text" id="test">
                    </div>
                </div>
            </div>
            <div class="c-styleguide__content" style="background: #f5be73; padding: 15px;">
                <div class="c-form">
                    <div class="c-form__field c-form__field--white u-margin-bottom-none">
                        <label class="c-form__label c-form__label" for="test2">White</label>
                        <input class="c-form__input c-form__input--text js-form__input" type="text" id="test2">
                    </div>
                </div>
            </div><br>
            <div class="c-styleguide__content">
                <div class="c-form">
                    <div class="c-form__field c-form__field--alt">
                        <label class="c-form__label c-form__label" for="test3">Alternative</label>
                        <input class="c-form__input c-form__input--text js-form__input" type="text" id="test3">
                    </div>
                </div>
            </div>
            <div class="c-styleguide__content">
                <div class="c-form">
                    <div class="c-form__field c-form__field--dropdown js-dropdown">
                        <select class="c-form__select" name="test4">
                            <option value="1">Item Something</option>
                            <option value="2">Item 2</option>
                            <option value="3">Item 3</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="c-text-image-block">
    <div class="o-wrapper">
        <div class="c-text-image-block__wrapper o-flex">
            <div class="c-text-image-block__split c-text-image-block__split--text">
                <h2 class="c-text-image-block__subtitle u-title--large u-color--gold">Title</h2>
                <p class="c-text-image-block__copy u-content u-text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                <div class="c-text-image-block__links o-button-group u-text-center">
                    <a href="#" class="c-btn c-btn--primary c-btn--line o-button-group__item">OUR PHILOSOPHY</a>
                    <a href="#" class="c-btn c-btn--primary c-btn--line o-button-group__item">OUR MENUS</a>
                </div>
            </div>
            <div class="c-text-image-block__split c-text-image-block__split--image">
                <img alt="" class="c-text-image-block__image o-object-fit" src="<?= get_build_uri( '/img/image1.jpg' ) ?>">
            </div>
        </div>
    </div>
</section>

<section class="c-tri-square-block">
    <div class="o-wrapper">
        <div class="c-tri-square-block__wrapper o-flex">
            <div class="c-tri-square-block__split">
                <div class="c-tri-square-block__inner">
                    <div class="c-tri-square-block__part">
                        <h2 class="c-tri-square-block__subtitle u-title--medium u-color--gold u-text-center">Aubaine Deli</h2>
                        <p class="c-tri-square-block__copy u-content u-text-center">Wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo.</p>
                        <div class="c-tri-square-block__links o-button-group u-text-center">
                            <a href="#" class="c-btn c-btn--primary c-btn--line o-button-group__item">More info</a>
                        </div>
                    </div>
                    <div class="c-tri-square-block__part">
                        <div class="c-tri-square-block__background">
                            <img alt="" class="o-object-fit" src="<?= get_build_uri( '/img/placeholder.jpg') ?>">
                        </div>
                        <div class="c-tri-square-block__foreground">
                            <h2 class="c-tri-square-block__subtitle u-title--medium u-color--white u-text-center">Newsletter</h2>
                            <form action="" method="post">
                                <div class="c-form">
                                    <div class="c-form__field c-form__field--white c-form__field--alt">
                                        <label class="c-form__label c-form__label" for="newsletter-email">Enter email for our latest news &amp; offers</label>
                                        <input class="c-form__input c-form__input--text js-form__input js-newsletter-input" id="newsletter-email" name="newsletter-email" type="text">
                                    </div>
                                    <div class="u-text-center">
                                        <button disabled class="c-btn c-btn--line c-btn--primary c-btn--light js-newsletter-submit" type="submit">Sign up</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="c-tri-square-block__split">
                <div class="c-tri-square-block__inner">
                    <div class="c-tri-square-block__part u-bg-color--pink">
                        <h2 class="c-tri-square-block__subtitle u-title--medium u-color--white u-text-center">Our blog</h2>
                        <p class="c-tri-square-block__copy u-content u-text-center u-color--white">Wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit.</p>
                        <div class="c-tri-square-block__links o-button-group u-text-center">
                            <a href="#" class="c-btn c-btn--light c-btn--line o-button-group__item">Aubaine latest</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="c-styleguide c-styleguide--inputs">
    <div class="o-wrapper">
        <div class="c-styleguide__meta">
            <h2 class="c-styleguide__title c-styleguide__title--medium">Objects</h2>
        </div>
        <div class="c-styleguide__part">
            <h3 class="c-styleguide__title c-styleguide__title--small">Accordion</h3>
            <div class="c-styleguide__content">

                <div class="c-content-block o-accordion">
                    <div class="o-accordion__container">
                        <input class="o-accordion__checkbox" id="accordion-one" type="checkbox" checked></input>
                        <label class="c-content-block__header o-accordion__header" for="accordion-one">
                            Accordion Heading
                            <div class="o-accordion__switch o-accordion__switch--closed">+</div>
                            <div class="o-accordion__switch o-accordion__switch--open">-</div>
                        </label>
                        <div class="c-content-block__body o-accordion__content">
                            <p>Some accordion content shere</p>
                            <p>Notice how the accordion is applied to a content block</p>
                            <p>It simply gives the content block accordion functionality</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<?php include_restaurants_list() ?>

<?php get_footer() ?>
