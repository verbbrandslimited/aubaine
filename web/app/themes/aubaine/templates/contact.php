<?php
/**
 * The template for the Contact us page.
 *
 * @package Hex Digital
 * @subpackage Aubaine
 * @since 2017
 *
 * Template Name: Contact us
 */
$args = [
	'post_type' => 'locations',
	'posts_per_page' => -1,
	'orderby' => 'post_title',
	'order' => 'ASC',
	'tax_query' => [
		[
			'taxonomy' => 'location_categories',
			'field' => 'id',
			'terms' => [22],
			'operator' => 'NOT IN'
		]
	]
];
$locations = new WP_Query($args);
$last_posts_select = [];
$last_posts_div = [];
$count = 0;
$dubai = '';

get_header(); ?>

<style type="text/css">

.acf-map {
	width: 100%;
	height: 500px;
}

/* fixes potential theme css conflict */
.acf-map img {
   max-width: inherit !important;
}

</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCBEW6Ww788OVzLqg3uyePEvJeDq5qSJqo"></script>
<script type="text/javascript">
(function($) {

/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function new_map( $el ) {
	
	// var
	var $markers = $el.find('.marker');
	
	
	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP,
		styles      : [
							  {
							    "elementType": "geometry",
							    "stylers": [
							      {
							        "color": "#f5f5f5"
							      }
							    ]
							  },
							  {
							    "elementType": "labels.icon",
							    "stylers": [
							      {
							        "visibility": "off"
							      }
							    ]
							  },
							  {
							    "elementType": "labels.text.fill",
							    "stylers": [
							      {
							        "color": "#616161"
							      }
							    ]
							  },
							  {
							    "elementType": "labels.text.stroke",
							    "stylers": [
							      {
							        "color": "#f5f5f5"
							      }
							    ]
							  },
							  {
							    "featureType": "administrative.land_parcel",
							    "elementType": "labels.text.fill",
							    "stylers": [
							      {
							        "color": "#bdbdbd"
							      }
							    ]
							  },
							  {
							    "featureType": "poi",
							    "elementType": "geometry",
							    "stylers": [
							      {
							        "color": "#eeeeee"
							      }
							    ]
							  },
							  {
							    "featureType": "poi",
							    "elementType": "labels.text.fill",
							    "stylers": [
							      {
							        "color": "#757575"
							      }
							    ]
							  },
							  {
							    "featureType": "poi.park",
							    "elementType": "geometry",
							    "stylers": [
							      {
							        "color": "#e5e5e5"
							      }
							    ]
							  },
							  {
							    "featureType": "poi.park",
							    "elementType": "labels.text.fill",
							    "stylers": [
							      {
							        "color": "#9e9e9e"
							      }
							    ]
							  },
							  {
							    "featureType": "road",
							    "elementType": "geometry",
							    "stylers": [
							      {
							        "color": "#ffffff"
							      }
							    ]
							  },
							  {
							    "featureType": "road.arterial",
							    "elementType": "labels.text.fill",
							    "stylers": [
							      {
							        "color": "#757575"
							      }
							    ]
							  },
							  {
							    "featureType": "road.highway",
							    "elementType": "geometry",
							    "stylers": [
							      {
							        "color": "#dadada"
							      }
							    ]
							  },
							  {
							    "featureType": "road.highway",
							    "elementType": "labels.text.fill",
							    "stylers": [
							      {
							        "color": "#616161"
							      }
							    ]
							  },
							  {
							    "featureType": "road.local",
							    "elementType": "labels.text.fill",
							    "stylers": [
							      {
							        "color": "#9e9e9e"
							      }
							    ]
							  },
							  {
							    "featureType": "transit.line",
							    "elementType": "geometry",
							    "stylers": [
							      {
							        "color": "#e5e5e5"
							      }
							    ]
							  },
							  {
							    "featureType": "transit.station",
							    "elementType": "geometry",
							    "stylers": [
							      {
							        "color": "#eeeeee"
							      }
							    ]
							  },
							  {
							    "featureType": "water",
							    "elementType": "geometry",
							    "stylers": [
							      {
							        "color": "#c9c9c9"
							      }
							    ]
							  },
							  {
							    "featureType": "water",
							    "elementType": "labels.text.fill",
							    "stylers": [
							      {
							        "color": "#9e9e9e"
							      }
							    ]
							  }
						  ]
	};
	
	
	// create map	        	
	var map = new google.maps.Map( $el[0], args);
	
	
	// add a markers reference
	map.markers = [];
	
	
	// add markers
	$markers.each(function(){
		
    	add_marker( $(this), map );
		
	});
	
	
	// center map
	center_map( map );
	
	
	// return
	return map;
	
}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

var currentInfowindow = null;

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map,
		icon		: document.location.protocol + '//' + document.location.host + '/wp-content/themes/aubaine/resource/img/svg/aubaine-map-pin.svg'
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});
		
		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {
		    if( currentInfowindow ){
			    currentInfowindow.close();
		    }

			if( $marker.html().length > 10 ){
				infowindow.open( map, marker );
				currentInfowindow = infowindow;
			}
		});
		
		google.maps.event.addListener(map, "click", function(event) {
		    infowindow.close();
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
// global var
var map = null;

$(document).ready(function(){

	$('.acf-map').each(function(){

		// create map
		map = new_map( $(this) );

	});

});

})(jQuery);
</script>

    <?php if ( have_posts() ): the_post() ?>
        <div class="o-wrapper">
            <div class="c-contact js-contact verb-update">
                <div class="c-contact__sections js-contact-sections">
	                <h2>Contact a restaurant</h2>
					<div class="c-form location-linker">
						<div class="o-button-group__item c-form__field c-form__field--dropdown js-dropdown">
							<select class="c-form__select js-events-dropdown" style="display: none;">
								<option selected="" disabled="">Select a restaurant</option>
								<?php while( $locations->have_posts() ): $locations->the_post();
									if( ! has_term( 'disable-location', 'location_categories' ) ): ?>
										<option value="<?= get_the_ID(); ?>"><?= the_title(); ?></option>
									<?php else:
										$last_posts_select[] = [
											'id'    => get_the_ID(),
											'title' => get_the_title()
										];
									endif;
                                endwhile; wp_reset_postdata(); ?>
<!--
                                <?php foreach( $last_posts_select as $p ): ?>
                                	<option value="<?= $p['id']; ?>"><?= $p['title']; ?></option>
                                <?php endforeach ?>
-->
                            </select>
							<div class="c-form__dropdown">
								<div class="c-form__dropdown-options">
									<?php while( $locations->have_posts() ): $locations->the_post();
										if( ! has_term( 'international', 'location_categories' ) && ! has_term( 'bakery-cafe', 'location_categories' ) ): ?>
											<div class="c-form__dropdown-item" data-value="<?= get_the_ID(); ?>" data-url="<?php the_permalink(); ?>"><?php the_title(); ?></div>
										<?php else:
											$last_posts_div[] = [
												'id'    => get_the_ID(),
												'link'  => get_the_permalink(),
												'title' => get_the_title()
											];
										endif;
	                                endwhile; wp_reset_postdata(); ?>
	                                <?php foreach( $last_posts_div as $p ):
	                                	if( $p['id'] === 134 ):
	                                		$dubai = $p;
	                                	elseif( $p['id'] === 132 ): ?>
	                                		<div class="c-form__dropdown-item" data-value="<?= $dubai['id']; ?>" data-url="<?= $dubai['link']; ?>"><?= $dubai['title']; ?></div>
		                                	<div class="c-form__dropdown-item" data-value="<?= $p['id']; ?>" data-url="<?= $p['link']; ?>"><?= $p['title']; ?></div>
		                                <?php else: ?>
		                                	<div class="c-form__dropdown-item" data-value="<?= $p['id']; ?>" data-url="<?= $p['link']; ?>"><?= $p['title']; ?></div>
		                                <?php endif; ?>
	                                <?php endforeach; ?>
								</div>
								<div class="c-form__dropdown-selected">Select a restaurant</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
        <div class="location-map">
            <div class="acf-map">
                <?php while( $locations->have_posts() ): $locations->the_post();
                	// Vars
                	$area = get_the_terms( get_the_ID(), 'location_categories' );
                	$location    = get_field('map_location');
                	$contact     = get_field( 'contact_items', get_the_ID() );
                	$events_menu = get_field('events_menu');
                	if( isset($location) && ! has_term( 'international', 'location_categories', get_the_ID() ) ): ?>
						<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
							<div class="info-window-wrapper" data-data="<?= $international; ?>">
								<div class="window-header">
									<h2><?= get_the_title(); ?></h2>
									<?php foreach( $contact as $c ):
                                        if( $c['type'] == 'address' ): ?>
                                            <span><?= $c['text']; ?></span>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
								</div> <!-- /.window-header -->
								<div class="window-contact">
									<?php foreach( $contact as $c ):
	                                    if( $c['type'] == 'nearest-tube' ): ?>
	                                    	<div class="nearest-tube">
												<div class="o-media__img">
			                                        <span class="icon icon-underground c-content-block__list-icon">
													    <svg role="img">
													        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-underground"></use>
													    </svg>
													</span>
												</div>
	                                        	<span><?= $c['text']; ?></span>
	                                    	</div>
	                                    <?php endif; ?>
	                                <?php endforeach; ?>
	                                <?php foreach( $contact as $c ):
	                                    if( $c['type'] == 'events-phone' ): ?>
	                                    	<div class="events-phone">
	                                        	<div class="o-media__img">
			                                        <span class="icon icon-telephone c-content-block__list-icon">
													    <svg role="img">
													        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-telephone"></use>
													    </svg>
													</span>
												</div>
	                                        	<span><?= $c['text']; ?></span>
	                                    	</div>
	                                    <?php endif; ?>
	                                <?php endforeach; ?>
	                                <?php foreach( $contact as $c ):
	                                    if( $c['type'] == 'email' ): ?>
	                                    	<div class="events-email">
	                                        	<div class="o-media__img">
			                                        <span class="icon icon-letter c-content-block__list-icon">
													    <svg role="img">
													        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-letter"></use>
													    </svg>
													</span>
												</div>
	                                            <a href="mailto:<?= $c['text']; ?>" title="<?php _e('Email aubaine', 'aubaine'); ?>"><span><?= $c['text']; ?></span></a>
	                                    	</div>
	                                    <?php endif; ?>
	                                <?php endforeach; ?>
								</div> <!-- /.window-contact -->
								<div class="window-links js-contact">
									<ul class="js-contact-section">
										<?php if ($ot = get_field('url_prefix')): ?>
										<li class="o-list__item c-contact__list-item js-peek-link"><a class="c-btn c-btn--primary book-table-link" href="/book-a-table/" data-ot="<?php echo $ot; ?>"><?php _e('Book a table', 'aubaine'); ?></a></li>
										<?php endif; ?>
		                                <li><a href="<?php the_permalink(); ?>" class="more-link" title="<?php the_title(); ?>"><?php _e('Find Out More', 'aubaine'); ?></a></li>
									</ul>
								</div> <!-- /.window-links -->
							</div>
						</div>
					<?php endif;
				endwhile; ?>
			</div>
        </div> <!-- /.location-map -->
        <div class="o-wrapper">
	        <div class="wrapper-contact">
		        <div class="feedback">
			        <div class="contain">
				        <h2>Feedback</h2>
				        <p>Tell us about your restaurant visit.</p>
				        <p>We value your feedback and believe it's important for us to know where to improve so we can ensure all our guests have the best possible experience.</p>
				        <a class="c-contact__link" target="_blank" href="http://www.aubainefeedback.co.uk/" rel="nofollow" target="_blank">Go to Feedback Form</a>
			        </div>
		        </div>
		        <div class="careers">
			        <div class="contain">
				        <h2>Careers</h2>
				        <p>Work with us here at Aubaine!</p>
				        <p>With eight restaurants, two cafés, and bakery sites across London, Aubaine is a true blend of cosmopolitan style and high-quality contemporary French cuisine. Combining a modern French approach with the rich heritage of London.</p>
				        <a class="c-contact__link" target="_blank" href="https://www.caterer.com/jobs-at/aubaine/jobs">view Opportunities</a>
			        </div>
		        </div>
	        </div>
        </div>

    <?php endif ?>

<?php get_footer() ?>
