<?php if ( $content = get_sub_field( 'wysiwyg_content' ) ) : ?>
    <section class="o-layout-block o-layout-block--fluid">
        <div class="o-layout-block__item o-layout-block__item--content-only"<?= scrollToId( get_sub_field( 'block_id' ) ) ?>>
            <div class="c-text-block c-text-block--no-flex u-padding-top-large">
                <?= $content ?>
            </div>
        </div>
    </section>
<?php endif ?>
