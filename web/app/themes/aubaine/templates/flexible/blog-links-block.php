<?php if ( $links = get_sub_field( 'links' ) ) : ?>
    <div class="c-blog__section c-blog__section--links">
        <div class="c-blog__buttons o-button-group">
            <?php foreach ( $links as $link ) : ?>
                <?php if ( acf_repeater_item_has_fields( ['url', 'text'], $link ) ) : ?>
                    <a class="c-btn c-btn--primary o-button-group__item"
                       href="<?= $link['url'] ?>">
                        <?= $link['text'] ?>
                    </a>
                <?php endif ?>
            <?php endforeach ?>
        </div>
    </div>
<?php endif ?>
