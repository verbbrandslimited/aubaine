<?php $event_locations = location_events_content() ?>

<?php if ( count( $event_locations ) > 0 ) : ?>
    <section class="o-layout-block o-layout-block--fluid">
        <div class="o-layout-block__item u-1/1 u-2/3@desk o-layout-block__item--content-only"<?= scrollToId( get_sub_field( 'block_id' ) ) ?>>
            <div class="c-content-block c-content-block--no-separator js-locations-block u-padding-top-large">
                <?php if ( $location_block_title = get_sub_field( 'locations_block_title' ) ) : ?>
                    <div class="c-content-block__header c-content-block__header--no-separator u-text-center--mobile-only">
                        <?= $location_block_title ?>
                    </div>
                <?php endif ?>
                <div class="c-content-block__body">
                    <div class="o-layout o-layout--large">
                        <div class="o-layout__item u-1/1 u-1/2@desk">
                            <div class="c-form u-margin-bottom-large">
                                <?php if ( $location_block_description = get_sub_field( 'locations_block_description' ) ) : ?>
                                    <?= $location_block_description ?>
                                <?php endif ?>
                                <div class="o-button-group__item c-form__field c-form__field--dropdown js-dropdown">
                                    <select class="c-form__select js-events-dropdown">
                                        <option selected disabled>Select Location</option>
                                        <?php foreach ( $event_locations as $location_id => $location ) : ?>
                                            <option value="<?= $location_id ?>"><?= $location['location_title'] ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <?php if ( $seasonal_menu = get_field( 'seasonal_menu' ) ) : ?>
                                    <a class="c-btn c-btn--primary c-btn--block"
                                       href="<?= $seasonal_menu ?>"
                                       target="_blank"
                                       rel="noopener noreferrer">View Menu</a>
                                <?php endif ?>
                            </div>
                        </div><!--
                        --><div class="o-layout__item u-1/1 u-1/2@desk">
                            <div class="js-location-event-details">
                                <?php foreach ( $event_locations as $location_id => $location ) : ?>
                                    <div class="u-hide" data-location="<?= $location_id ?>">
                                        <?php display_location_details( $location_id, $location ) ?>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="o-layout-block__item u-1/1 u-1/3@desk o-layout-block__item--content-only o-layout-block__item--no-pb">
            <div class="c-content-block c-content-block--no-separator js-locations-block u-padding-top-large u-padding-bottom-large">
                <div class="c-content-block__header c-content-block__header--no-separator u-text-center--mobile-only">
                    Enquiries
                </div>
                <div class="c-content-block__body">
                    <?php include get_partials_directory_uri() . '/enquiries-form.php' ?>
                </div>
            </div>
        </div>
        <div class="o-layout-block__item u-1/1 o-layout-block__item--content-only o-layout-block__item--no-pb">
            <div class="c-carousel c-carousel--locations-block swiper-container js-carousel--locations-block">
                <div class="c-carousel__wrapper swiper-wrapper">
                    <?php foreach ( $event_locations as $location_id => $location ) : ?>
                        <?php if ( $location['location_image'] ) : ?>
                            <div class="c-carousel__item swiper-slide" data-location="<?= $location_id ?>">
                                <img alt="<?= $location['location_image']['alt'] ? : '' ?>"
                                     class="c-gallery__image" src="<?= $location['location_image']['url'] ?>">
                            </div>
                        <?php endif ?>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>
