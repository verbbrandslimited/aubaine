<?php if ( $text = get_sub_field( 'text' ) ) : ?>
    <div class="c-blog__section c-blog__section--text">
        <?= $text ?>
    </div>
<?php endif ?>
