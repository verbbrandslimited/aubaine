<?php
	$panel_count = '';
    $reversed = (int) acf_single_value_checkbox( get_sub_field( 'reversed' ) );
    $panels = get_sub_field( 'panels' );
    if( $panels != false || $panels != null ){
	    $panel_count = count( $panels );
    }
?>

<?php if ( $panel_count ) : ?>
    <section class="o-layout-block<?= ( $reversed ? ' o-layout-block--reverse' : '' ) ?>"<?= scrollToId( get_sub_field( 'block_id' ) ) ?>>
        <?php foreach( $panels as $panel ) : ?>
            <?php include get_partials_directory_uri() . '/panel.php' ?>
        <?php endforeach ?>
    </section>
<?php endif ?>