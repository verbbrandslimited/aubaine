<?php if ( $image = get_sub_field( 'full_image' ) ) : ?>
    <div class="c-blog__section c-blog__section--image">
        <img alt="<?= $image['alt'] ?>" class="c-blog__image" src="<?= $image['url'] ?>">
    </div>
<?php endif ?>
