<?php if ( $offers = get_sub_field( 'offer_items' ) ) : ?>
    <section class="o-layout-block o-layout-block--fluid"<?= scrollToId( get_sub_field( 'block_id' ) ) ?>>
        <div class="o-layout-block__item o-layout-block__item--content-only">
            <div class="c-content-block c-content-block--no-separator u-padding-top-large">
                <?php if ( $offers_block_title = get_sub_field( 'offers_block_title' ) ) : ?>
                    <div class="c-content-block__header c-content-block__header--no-separator u-text-center">
                        <?= $offers_block_title ?>
                    </div>
                <?php endif ?>
                <div class="c-content-block__body">
                    <div class="o-flex o-flex--wrap o-flex--justify-center">
                        <?php foreach( $offers as $offer ) : ?>
                            <div class="c-text-block u-margin-bottom-huge">
                                <?php if ( $offer['offer_title'] ) : ?>
                                    <div class="c-text-block__header c-text-block__header--alt">
                                        <?= $offer['offer_title'] ?>
                                    </div>
                                <?php endif ?>
                                <?php if ( $offer['offer_description'] ) : ?>
                                    <div class="c-text-block__content u-text-center">
                                        <?= $offer['offer_description'] ?>
                                    </div>
                                <?php endif ?>
                                <?php if ( $offer['offer_buttons'] ) : ?>
                                    <div class="c-text-block__buttons o-button-group">
                                        <?php foreach( $offer['offer_buttons'] as $offer_button ) : ?>
                                            <a class="c-btn c-btn--primary o-button-group__item" href="<?= $offer_button['url'] ?>">
                                                <?= $offer_button['text'] ?>
                                            </a>
                                        <?php endforeach ?>
                                    </div>
                                <?php endif ?>
                                <?php if ( $offer['offer_secondary_buttons'] ) : ?>
                                    <?php foreach( $offer['offer_secondary_buttons'] as $offer_secondary_button ) : ?>
                                        <div class="u-color--primary u-margin-top-small">
                                            <a class="o-link--naked" href="<?= $offer_secondary_button['secondary_url'] ?>">
                                                <?= $offer_secondary_button['secondary_text'] ?>
                                            </a>
                                        </div>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>
