<?php
/**
 * The template for displaying all pages.
 *
 * @package Hex Digital
 * @subpackage Aubaine
 * @since 2017
 *
 * Template Name: Homepage
 */

get_header() ?>

<div class="o-wrapper">
    <? while ( have_posts() ) : the_post() ?>
        <? if ( have_rows( 'flexible_content_blocks' ) ) : ?>
            <? while ( have_rows( 'flexible_content_blocks' ) ) : the_row() ?>
                <? $layout = get_flexible_content_template_uri( get_row_layout() ) ?>
                <? if ( file_exists( $layout ) ) include $layout ?>
            <? endwhile ?>
        <? endif ?>
    <? endwhile ?>
</div>

<div class="o-wrapper">
    <div class="c-content-block c-content-block--no-separator u-margin-top-huge">
        <div class="c-content-block__header u-text-center">Our Restaurants</div>
    </div>
    <? include_restaurants_list( 'home-our-restaurants' ) ?>
</div>



<? get_footer() ?>
<div class="popup-section-homepage">
    <div class="inner-container">
        <div class="close-button" style="display: inline-block; float: right;">x</div><!-- close-button  -->
        <div class="title">
            <?php the_field('title'); ?>
        </div><!-- title -->
        <div class="content">
            <?php the_field('content'); ?>
        </div><!-- title -->

        <div class="button">
            <a href="<?php the_field('button_link');?>"><?php the_field('button'); ?></a>
        </div><!-- button -->
    </div><!-- inner-container -->
</div><!-- popup-section -->