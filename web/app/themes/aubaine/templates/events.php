<?php
/**
 * The template for displaying our singular location pages for
 * location post type
 *
 * @package Hex Digital
 * @subpackage Aubaine
 * @since 2017
 *
 * Template Name: Events
 */

get_header();

$args = [
	'post_type' => 'locations',
	'posts_per_page' => -1,
	'orderby' => 'post_title',
	'order' => 'ASC',
	'tax_query' => [
		[
			'taxonomy' => 'location_categories',
			'field' => 'id',
			'terms' => [22],
			'operator' => 'NOT IN'
		]
	]
];
$location_query = new WP_Query($args);
$last_posts_select = [];
$last_posts_div = [];
$count = 0;
$events_menu = get_field('events_menu');

$event_locations = location_events_content() ?>

    
        <div class="c-detail-page js-events-page">
            <div class="c-detail-page__header c-detail-header c-detail-header--loading">
                <?php include get_partials_directory_uri() . '/detail-pages/detail-header.php' ?>
            </div>
            <div class="o-section">
                <div class="o-wrapper">
                    <div class="o-flex o-flex--wrap">
                        <?php if ( count( $event_locations ) > 0 ) : ?>
                            <div class="c-content-block c-detail-page__block-content c-detail-page__block-content--double">
                                <div class="c-content-block__header">
                                    Our venues
                                </div>
                                <div class="c-content-block__body">
                                    <div class="o-layout o-layout--large">
                                        <div class="o-layout__item u-1/1 u-1/2@desk">
	                                        
	                                        <?php if ( have_posts() ) : the_post(); ?>
								                <div class="c-form location-selection-filter">
													<div class="o-button-group__item c-form__field c-form__field--dropdown js-dropdown">
														<select class="c-form__select js-events-dropdown" style="display: none;">
															<option selected="" disabled="">Select a location to find events...</option>
															<?php while( $location_query->have_posts() ): $location_query->the_post();
							                                	if( ! has_term( 'disable-location', 'location_categories' ) ): ?>
																<option value="<?= get_the_ID(); ?>"><?= the_title(); ?></option>
															<?php else:
																$last_posts_select[] = [
																	'id'    => get_the_ID(),
																	'title' => get_the_title()
																];
															endif;
						                                endwhile; wp_reset_postdata(); ?>
							                            </select>
							                            <div class="reset">
								                            <i class="fas fa-sync"></i>
								                            <span class="reset-filter"><?= _e('Reset', 'aubaine'); ?></span>
							                            </div>
													</div>
												</div>
											<?php endif; ?>

                                            <div class="location-venues">
	                                            <?php if( $location_query-have_posts() ): ?>
	                                            	<?php while( $location_query->have_posts() ): $location_query->the_post();
		                                            	$contact = get_field( 'contact_items', get_the_ID() ); ?>
		                                            	<div class="location" data-id="<?= get_the_ID(); ?>">
		                                            		<div class="location-header" data-location="<?= $location_id ?>">
		                                                        <h3><?= get_the_title(); ?></h3>
		                                                        <?php foreach( $contact as $c ):
			                                                        if( $c['type'] == 'address' ): ?>
				                                                        <span><?= $c['text']; ?></span>
			                                                        <?php endif; ?>
		                                                        <?php endforeach; ?>
		                                                    </div>
		                                                    <?php if( $desc = get_field('events_information', get_the_ID()) ): ?>
			                                                    <div class="location-description">
				                                                    <div class="location-desc-container">
					                                                    <div class="desc-container">
					                                                    	<?= $desc; ?>
					                                                    </div>
				                                                    </div>
				                                                    <span class="read-more"><?= _e('Read More', 'aubaine'); ?></span>
			                                                    </div>
			                                                <?php endif; ?>
		                                                    <div class="location-meta">
			                                                    <div class="location-meta--contact">
				                                                    <?php foreach( $contact as $c ):
				                                                        if( $c['type'] == 'nearest-tube' ): ?>
				                                                        	<div class="nearest-tube">
																				<div class="o-media__img">
											                                        <span class="icon icon-underground c-content-block__list-icon">
																					    <svg role="img">
																					        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-underground"></use>
																					    </svg>
																					</span>
																				</div>
					                                                        	<span><?= $c['text']; ?></span>
				                                                        	</div>
				                                                        <?php endif; ?>
			                                                        <?php endforeach; ?>
			                                                        <?php foreach( $contact as $c ):
				                                                        if( $c['type'] == 'events-phone' ): ?>
				                                                        	<div class="events-phone">
					                                                        	<div class="o-media__img">
											                                        <span class="icon icon-telephone c-content-block__list-icon">
																					    <svg role="img">
																					        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-telephone"></use>
																					    </svg>
																					</span>
																				</div>
					                                                        	<span><?= $c['text']; ?></span>
				                                                        	</div>
				                                                        <?php endif; ?>
			                                                        <?php endforeach; ?>
			                                                        <?php foreach( $contact as $c ):
				                                                        if( $c['type'] == 'events-email' ): ?>
				                                                        	<div class="events-email">
					                                                        	<div class="o-media__img">
											                                        <span class="icon icon-letter c-content-block__list-icon">
																					    <svg role="img">
																					        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-letter"></use>
																					    </svg>
																					</span>
																				</div>
						                                                        <a href="mailto:<?= $c['text']; ?>" title="<?= _e('Email aubaine', 'aubaine'); ?>"><span><?= $c['text']; ?></span></a>
				                                                        	</div>
				                                                        <?php endif; ?>
			                                                        <?php endforeach; ?>
			                                                    </div>
																
																<?php if( have_rows( 'capacity_venue_areas' ) ): ?>
																	<div class="o-media__img">
								                                        <span class="icon icon-group c-content-block__list-icon">
																		    <svg role="img">
																		        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-group"></use>
																		    </svg>
																		</span>
																	</div>
				                                                    <div class="location-meta--seating">
					                                                    <?php while( have_rows('capacity_venue_areas') ): the_row(); ?>
					                                                    	<?php
						                                                    	// Vars
						                                                    	$venue = get_sub_field('venue_area');
						                                                    	$cap   = get_sub_field('capacity_information'); ?>
						                                                    	
						                                                    	<div class="area">
							                                                    	<span><?= $venue; ?></span>
							                                                    	<p><?= $cap; ?></p>
						                                                    	</div>
					                                                    <?php endwhile; ?>
	
				                                                    </div>
				                                                <?php endif; ?>
		                                                    </div> <!-- /.location-meta -->
		                                                    <div class="location-button">
			                                                    <a href="#" class="location-enquire"><?= _e('Enquire Now', 'albaine'); ?></a>
			                                                    <?php if( $events_menu ): ?>
				                                                    <a href="<?= $events_menu['url']; ?>" class="location-menu" target="_blank" rel="noopener noreferrer"><?= _e('Sample Menu', 'albaine'); ?></a>
				                                                <?php endif; ?>
		                                                    </div>
														</div> <!-- /.location -->
	                                            	<?php endwhile; wp_reset_postdata(); ?>
	                                            <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        <?php endif ?>
                        <div class="c-content-block c-detail-page__block-content">
                            <div class="enquiry-popup">
	                            <div class="overlay"></div>
	                            <div class="popup-form">
		                            <i class="exit-popup">x</i>
		                            <div class="c-header">
		                                <h2><?= _e('Enquire', 'aubaine'); ?></h2>
		                            </div>
	                                <?php include get_partials_directory_uri() . '/enquiries-form-events.php' ?>
	                            </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- /.o-wrapper -->
            </div>
        </div>



<?php get_footer() ?>
