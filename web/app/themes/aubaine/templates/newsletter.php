<?php
/**
 * The template for newsletter signup
 *
 * @package Hex Digital
 * @subpackage Aubaine
 * @since 2017
 *
 * Template Name: Newsletter
 */

get_header() ?>

    <?php while ( have_posts() ) : the_post() ?>

        <section class="u-margin-top-huge u-margin-bottom-huge">
            <div class="o-wrapper">
                <div class="o-layout o-layout--center">
                    <div class="o-layout__item u-1/1 u-1/2@device u-1/3@desk">
                        <?php if ( isset( $post_object['errors'] ) ) : ?>
                            <h2 class="u-title u-title--large u-color--gold u-text-center">Oops! Something went wrong.</h2>
                            <p class="u-content u-text-center"><?= $post_object['errors']['general'] ?></p>
                        <?php endif ?>
                        <?php if ( isset( $post_object['success'] ) && $post_object['success'] ) : ?>
                            <h2 class="u-title u-title--large u-color--gold u-text-center">Success!</h2>
                            <p class="u-content u-text-center">Thanks for signing up to our newsletter.</p>
                            <a class="c-btn c-btn--primary c-btn--block" href="/">Return home</a>
                        <?php else : ?>
                            <form action="" method="post">
                                <div class="c-form">
                                    <input name="newsletter-signup" type="hidden" value="1">
                                    <input name="random-string" type="hidden" value="<?= generate_random_string() ?>">
                                    <div class="c-form__field c-form__field--alt">
                                        <label class="c-form__label c-form__label" for="newsletter-email">Enter email</label>
                                        <input required class="c-form__input c-form__input--text js-form__input js-newsletter-input" id="newsletter-email"
                                                name="newsletter-email" type="email" value="<?= old('newsletter-email', $post_object) ?>"/>
                                    </div>
                                    <div class="u-text-center">
                                        <button disabled class="c-btn c-btn--line c-btn--primary js-newsletter-submit" type="submit">Sign up</button>
                                    </div>
                                </div>
                            </form>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </section>

    <?php endwhile ?>

<?php get_footer() ?>
