<?php
/**
 * The template for displaying all pages.
 *
 * @package Hex Digital
 * @subpackage Aubaine
 * @since 2017
 *
 * Template Name: Template
 */

get_header() ?>

    <?php while ( have_posts() ): the_post() ?>

        <?php the_title() ?>

        <?php the_content() ?>

    <?php endwhile ?>

<?php get_footer() ?>
