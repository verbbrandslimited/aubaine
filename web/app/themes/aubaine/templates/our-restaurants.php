<?php
/**
 * The template for the dynamic restaurnts list page
 *
 * @package Hex Digital
 * @subpackage Aubaine
 * @since 2017
 *
 * Template Name: Our restaurants
 */

get_header() ?>

    <?php while ( have_posts() ) : the_post() ?>

        <div class="u-bg-color--primary">
            <div class="o-wrapper">
                <?php include_restaurants_list( 'our-restaurants' ) ?>
            </div>
        </div>

    <?php endwhile ?>

<?php get_footer() ?>
