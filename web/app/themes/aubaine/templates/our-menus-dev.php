<?php
/**
 * The template for displaying the menus.
 *
 * @package Hex Digital
 * @subpackage Aubaine
 * @since 2017
 *
 * Template Name: Menus (DEV)
 */

$menu_data = get_menu_data( true );

$initial_menu = get_menu_from_query_params();

get_header() ?>

    <?php while ( have_posts() ): the_post() ?>
        <div class="c-menu-page u-bg-color--white-dark js-menu-page menus-update">
            <div class="o-section o-section--nopad-top">
                <div class="o-wrapper">
                    <?php $location_options = get_location_options( $menu_data, $initial_menu ) ?>
                    <div class="c-form u-h-center u-hide-from-device">
                        <div class="c-form__field c-form__field--dark-border c-form__field--dropdown js-dropdown">
	                        <span class="select-label">Select Menu:</span>
                            <select class="c-menu-page__menu-locations c-form__select">
                                <?= $location_options['mobile'] ?>
                            </select>
                        </div>
                    </div>
                    <div class="u-hide-until-device u-text-center">
                        <div class="o-button-group c-menu-page__menu-locations">
                            <?= $location_options['desktop'] ?>
                        </div>
                    </div>
                    <?php foreach ( $menu_data->get_all_locations() as $location_key => $location ) : ?>
                        <?php $location_slug = get_slug( $location->get_name() ) ?>
                        <?php $is_initial_location = (
                            is_initial_location( $initial_menu, $location_slug, $location_key )
                        ) ?>
                        <?php $active_button_group = (
                            $is_initial_location
                            ? ' c-menu-page__menu-names--active'
                            : ''
                        ) ?>
                        <div class="c-menu-page__menu-names o-button-group o-button-group--tight u-text-center<?= $active_button_group ?>" data-location="<?= $location_slug ?>">
	                        <?php foreach ( $location->get_menus() as $menu_title => $menu ) : ?>
                                <?php $menu_slug = get_slug( $menu['name'] ) ?>
                                <?php $active_menu = (
                                    is_initial_menu(
                                        $initial_menu,
                                        $location_slug,
                                        $menu_slug,
                                        $location_key,
                                        $menu_title
                                    )
                                    ? ' c-btn--active'
                                    : ''
                                ) ?>
                                <a class="c-menu-name c-btn c-btn--dim c-btn--fluid
                                          o-button-group__item<?= $active_menu ?>"
                                   data-menu="<?= $menu_slug ?>">
                                    <?= $menu['name'] ?>
                                </a>
                            <?php endforeach ?>
                            
                            <!-- MOBILE -->
	                        <div class="c-form u-h-center u-hide-from-device">
		                        <div class="c-form__field c-form__field--dark-border c-form__field--dropdown js-dropdown">
			                        <span class="select-label">Select Menu Type:</span>
			                        <select class="c-menu-page__menu-locations-types c-form__select">
			                            <? foreach ( $location->get_menus() as $menu_title => $menu ) : ?>
			                            	<? $menu_slug = get_slug( $menu['name'] ) ?>
			                                <? $active_menu = (
			                                    is_initial_menu(
			                                        $initial_menu,
			                                        $location_slug,
			                                        $menu_slug,
			                                        $location_key,
			                                        $menu_title
			                                    )
			                                    ? ' c-btn--active'
			                                    : ''
			                                ) ?>
			                            	<option class="c-menu-name c-btn c-btn--dim c-btn--fluid o-button-group__item" value="<?= $menu_slug; ?>" data-menu="<?= $menu_slug; ?>"><?= $menu['name'] ?></option>
			                            <? endforeach ?>
			                        </select>
		                        </div>
	                        </div>
                        </div>
                        <!-- MOBILE -->
                        
                        <?php $active_menu_block = (
                            $is_initial_location
                            ? ' c-menu-page__menu-block--active'
                            : ''
                        ) ?>
                        <div class="c-menu-page__menu-block<?= $active_menu_block ?>"
                             data-location="<?= $location_slug ?>">
                            <?php if ( $location->get_menus() !== null && is_array( $location->get_menus() ) ) : ?>
                                <?php foreach ( $location->get_menus() as $menu_title => $menu ) : ?>
                                    <?php $menu_slug = get_slug( $menu['name'] ) ?>
                                    <?php $active_menu_panel = (
                                        is_initial_menu(
                                            $initial_menu,
                                            $location_slug,
                                            $menu_slug,
                                            $location_key,
                                            $menu_title
                                        )
                                        ? ' c-menu--active'
                                        : ''
                                    ) ?>
                                    <div class="c-menu c-filters js-filter<?= $active_menu_panel ?>"
                                         data-location="<?= $location_slug ?>"
                                         data-menu="<?= $menu_slug ?>">
                                        <?= get_svg( 'logo', 'img', 'c-menu__logo' ) ?>
                                        <div class="c-filters__container u-margin-top-large u-margin-bottom">
                                            <span class="c-filters__description o-media">
                                                <?= get_svg( 'eye', 'img', 'c-filters__icon' ) ?> Click to filter by:
                                            </span>
                                            <?php foreach( get_menu_filters() as $filter_key => $filter_value ) : ?>
                                                <span class="c-filters__term js-filter-term" data-filter="<?= $filter_key ?>"><?= $filter_value ?></span>
                                            <?php endforeach ?>
                                        </div>
                                        <div class="o-layout o-layout--large">
                                            <div class="o-layout__item u-1/1 u-1/2@device">
                                                <?php $i = 0 ?>
                                                <?php $second_column = [] ?>
                                                <?php foreach ( $menu['menu_headings'] as $key => $category ) : ?>
                                                    <?php if ( $i % 2 ) : ?>
                                                        <?php $second_column[ $key ] = $category ?>
                                                        <div class="u-hide-from-device">
                                                            <?php include get_partials_directory_uri() . '/menu-block.php' ?>
                                                        </div>
                                                    <?php else : ?>
                                                        <?php include get_partials_directory_uri() . '/menu-block.php' ?>
                                                    <?php endif ?>
                                                    <?php $i++ ?>
                                                <?php endforeach ?>
                                            </div>
                                            <div class="o-layout__item u-1/2 u-hide-until-device">
                                                <?php foreach ( $second_column as $key => $category ) : ?>
                                                    <?php include get_partials_directory_uri() . '/menu-block.php' ?>
                                                <?php endforeach ?>
                                            </div>
                                        </div>
                                        <p class="u-content u-content--small u-text-center u-color--grey u-margin-top-large"><span class="u-bold">v</span> - vegetarian, <span class="u-bold">df</span> - dairy free. Please let us know if you have any allergies or intolerances. Discretionary service charge of 12.5% is included in your bill</p>
                                        <?= get_svg( 'logo-roundel', 'img', 'c-menu__roundel' ) ?>
                                    </div>
                                <?php endforeach ?>
                            <?php else : ?>
                                <p class="u-text-center">No menu's found for <?= $location->get_name() ?>.<br>
                                Please check back later.</p>
                            <?php endif ?>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
            
            <div class="top-button">
                <i class="fas fa-chevron-up"></i>
                <span><?= _e('Back To Top', 'aubaine'); ?></span>
            </div>
        </div>
        

    <?php endwhile ?>

<?php get_footer() ?>
