<?php
/**
 * The template for displaying booking form 
 *
 * @package Hex Digital
 * @subpackage Aubaine
 * @since 2017
 *
 * Template Name: Book a table
 */

get_header() ?>

    <?php while ( have_posts() ): the_post() ?>
        <section class="u-padding-top-huge u-padding-bottom-large u-bg-color--green-dark test">
            <?php include get_partials_directory_uri() . '/booking/booking-book-panel.php' ?>
        </section>
    <?php endwhile ?>

<?php get_footer() ?>
