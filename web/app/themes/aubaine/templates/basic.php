<?php
/**
 * The template for displaying basic content
 *
 * @package Hex Digital
 * @subpackage Aubaine
 * @since 2017
 *
 * Template Name: Basic
 */

get_header() ?>

    <?php while ( have_posts() ): the_post() ?>
        <?php $background_colour = ( get_field( 'background_colour' ) ) ? ' u-bg-color--' . get_field( 'background_colour' ) : '' ?>
        <section class="u-padding-top-large u-padding-bottom-large<?= $background_colour ?>">
            <div class="o-wrapper">
                <div class="o-layout o-layout--center">
                    <div class="o-layout__item u-1/1 u-1/2@device">
                        <?php the_content() ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile ?>

<?php get_footer() ?>
