<?php
/**
 * The template for displaying subscribe forms.
 *
 * @package Verb Brands
 * @subpackage Aubaine
 * @since 2018
 *
 * Template Name: Subscribe
 */


get_header();?>

    <?php while ( have_posts() ): the_post() ?>
      <div class="subscribe-page u-bg-color--green-dark">
        <div class="subscribe-page--wrapper">
          <header>
            <h1><?php the_title(); ?></h1>
          </header>
          <?php the_content() ?>
        </div>
      </div>
    <?php endwhile ?>

<?php get_footer() ?>