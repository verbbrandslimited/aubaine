<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package Hex Digital
 * @subpackage Aubaine
 * @since 2017
 */


$current = get_the_ID();
$urls =  get_field('pages', 'option', false, false);

$title = get_field('title', 'option');
$content = get_field('content', 'option');
$button = get_field('button', 'option');
$link = get_field('link', 'option');
$blog = get_field('blogs_page', 'option');




?>
<?php if(in_array($current, $urls) ) : ?>

    <div class="footer-newsletter container">
        <div class="static-newsletter">
            <h2><?php echo $title; ?></h2>
            <p><?php echo $content; ?></p>
            <a href="<?php echo $link; ?>"><div class="newsletter-button"><?php echo $button; ?></div></a>
        </div>
    </div><!-- container -->
<?php endif; ?>

<?php if ($blog) :?>
    <?php
    global $wp_query;
    if (isset( $wp_query ) && (bool) $wp_query->is_posts_page) :?>
        <div class="footer-newsletter container">
            <div class="static-newsletter">
                <h2><?php echo $title; ?></h2>
                <p><?php echo $content; ?></p>
                <a href="<?php echo $link; ?>"><div class="newsletter-button"><?php echo $button; ?></div></a>
            </div>
        </div><!-- container -->
    <?php endif ; ?>

<?php endif; ?>
<div class="o-instagram">
    <i class="fab fa-instagram"></i>
    <?= do_shortcode('[instagram-feed]'); ?>
</div>

<footer class="c-footer">
    <a class="c-footer__back-to-top" href="#top">Back to top</a>
    <div class="o-wrapper">
        <div class="c-footer__inner">
            <div class="c-footer__branding">
                <?= get_svg( 'logo-roundel', 'img', 'c-footer__logo' ) ?>
            </div>
            <div class="c-footer__social">
                <?php include get_partials_directory_uri() . '/social-links.php' ?>
                <a class="c-footer__contact" href="<?= get_permalink( get_page_id( 'contact-us' ) ) ?>">Contact us</a>
                <a class="c-footer__contact" href="<?php echo get_site_url(); ?>/subscribe/"><div class="newsletter-button">Sign Up Now</div></a>
            </div>
            <?php get_wp_menu( 'footer-menu', 'c-footer__navigation',  'o-list o-list--bare' ) ?>
            <p class="c-footer__legal">&copy; <?= date( 'Y' ) ?> Aubaine. All rights reserved</p>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
</body>
</html>
