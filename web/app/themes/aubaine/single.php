<?php
/**
 * The template for displaying our singular blog post
 *
 * @package Hex Digital
 * @subpackage Aubaine
 * @since 2017
 */

get_header() ?>

<div class="o-wrapper">
    <div class="c-blog__wrapper">
        <div class="c-blog__share o-flex o-flex--align-center">
            <span class="c-blog__share-text">Share</span>
            <a class="c-blog__share-icon" href="<?= get_share_link( 'facebook', get_permalink() ) ?>" rel="noopener noreferrer" target="_blank">
                <?= get_svg( 'facebook', 'img', 'c-icon c-icon--small' ) ?>
            </a>
            <a class="c-blog__share-icon" href="<?= get_share_link( 'twitter', get_permalink(), get_the_title(), 'AubaineUK' ) ?>" rel="noopener noreferrer" target="_blank">
                <?= get_svg( 'twitter', 'img', 'c-icon c-icon--small' ) ?>
            </a>
        </div>
        <?php while ( have_posts() ) : the_post() ?>
            <?php if ( have_rows( 'flexible_blog_blocks' ) ) : ?>
                <?php while ( have_rows( 'flexible_blog_blocks' ) ) : the_row() ?>
                    <?php $layout = get_flexible_content_template_uri( get_row_layout() ) ?>
                    <?php if ( file_exists( $layout ) ) include $layout ?>
                <?php endwhile ?>
            <?php else : ?>
                <div class="u-margin-top-large u-margin-bottom-large">
                    <h2 class="u-title u-title--large u-color--gold">There is no post content.</h2>
                </div>
            <?php endif ?>
        <?php endwhile ?>
    </div>
</div>

<?php get_footer() ?>
