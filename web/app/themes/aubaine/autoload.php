<?php
/**
 * Autoloader Registration
 *
 * The following line would cause the function to attempt to load the
 * \Aubaine\Foo\Bar class from /includes/classes/Foo/Bar.php:
 *
 *      new \Aubaine\Foo\Bar;
 *
 * @param string $class The fully-qualified class name.
 * @return void
 */
spl_autoload_register( function ( $class ) {

    $prefix = 'Aubaine\\';

    $base_dir = __DIR__ . '/includes/classes/';

    $len = strlen( $prefix );
    if ( strncmp( $prefix, $class, $len ) !== 0 ) {
        // Only handle classes in the $prefix namespace
        return;
    }

    $relative_class = substr( $class, $len );

    $file = $base_dir . str_replace( '\\', '/', $relative_class ) . '.php';

    if ( file_exists( $file ) ) {
        require $file;
    }
});
