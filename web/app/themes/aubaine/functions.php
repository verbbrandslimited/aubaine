<?php
/**
 * The functions for holding main functionality for the website.
 *
 * @package Hex Digital
 * @subpackage Aubaine
 * @since 2017
 */

/**
 * Table of Contents
 *
 * 0.0 Autoload, Namespacing + Dependencies
 *
 * 1.0 Aubaine
 *     1.1 Environment Functions
 *     1.2 URI + Versioning Functions
 *     1.4 Theme Setup
 *     1.5 ACF Export
 *     1.6 Register Menus
 *     1.7 Page type
 *     1.8 Component class modifiers
 */

/** 0.0 Autoload, Namespacing + Dependencies */
require_once 'config.php';

include __DIR__ . '/autoload.php';

foreach ( glob( __DIR__ . '/includes/helpers/*.php' ) as $filename ) {
    include $filename;
}

/**
 * Booking form submission - Directs users to Opentable form with pre population
 */
if ( isset( $_POST['booking-submit'] ) ) {
    redirect_to_booking();
}

/**
 * Enquiry form submission - Directs users to Design my night form with pre population
 */
if ( isset( $_POST['enquiry-submit'] ) ) {
    redirect_to_event_enquiries();
}

/** 1.0 Aubaine */

/** 1.1 Environment Functions */

/**
 * Gets the environment from the APP_ENV variable. This variable can either be
 * set in the Apache httpd.conf file or the .htaccess file in the form of:
 *
 *     SetEnv APP_ENV production
 *
 * If no environment variable is defined, this function will always assume a
 * production environment.
 *
 * @return string The name of the current environment
 */
function get_environment() {
    return getenv( 'APP_ENV' ) ?: 'production';
}

/**
 * Returns true if the current environment is development, and false if not.
 *
 * @return boolean Whether we're in a development environment
 */
function is_development() {
    return ( 'development' === get_environment() );
}

/**
 * Returns true if the current environment is staging, and false if not.
 *
 * @return boolean Whether we're in a staging environment
 */
function is_staging() {
    return ( 'staging' === get_environment() );
}

/**
 * Returns true if the current environment is production, and false if not.
 * This method is best used in an if statement around production only scripts
 * such as Google Analytics or Google Tag Manager tracking code.
 *
 * @return boolean Whether we're in a production environment
 */
function is_production() {
    return ( 'production' === get_environment() );
}

/** 1.2 URI + Versioning Functions */

$revision_hash = false;

/**
 * Gets the current revision hash.
 *
 * The revision hash is only based on the current deployment (on production)
 * which is created by DeployBot. This can be used to clear caches using query
 * strings based on the deployment only. Please bear in mind, the usage of this
 * function should be carefully thought about since individual files should use
 * their specific hash, based on the modification date using `get_commit_hash()`.
 *
 * @return string The current revision hash
 */
function get_revision_hash() {
    global $revision_hash;
    $revision_path = get_template_directory() . '/.revision';
    if ( ! is_production() ) return '';
    if ( ! file_exists( $revision_path ) ) return '';
    if ( false === $revision_hash ) $revision_hash = file_get_contents( $revision_path, null, null, 0, 7 );
    return $revision_hash;
}

/**
 * Returns the revision hash for a particular file, from its meta data.
 *
 * This function is a last resort to add a version, if both the commit hash and
 * the revision hash from DeployBot are not present. This function takes the
 * modification date fom the file meta data, which will always change for every
 * file upon deployment, because deploying to containers requires an upload of
 * all assets from the repository. Only use as a last resort, and sparingly.
 */
function get_file_hash( $uri ) {
    $uri = get_template_directory() . $uri;
    return file_exists( $uri ) ? filemtime( $uri ) : '';
}

/**
 * Returns the revision hash for a particular file.
 *
 * The hash that's returned is based on the modification timestamp of the file,
 * entered via the $uri parameter. This timestamp comes from the Git repository,
 * not the file meta data. The reason for this is because deploying to
 * containers requires an upload of all assets from the repository so, that
 * modification date will be on that particular occasion (when the files are
 * uploaded to the container), which will be incorrect. If this file does not
 * exist, the function will return an empty string.
 *
 * @param  string $uri The URI of the asset (relative to the theme directory)
 * @return string      The file revision hash
 */
function get_commit_hash( $uri ) {
    $revision = '';
    $assets_path = get_template_directory() . '/assets.json';
    if ( file_exists( $assets_path ) ) {
        $assets = json_decode( file_get_contents( $assets_path ), true );
        if ( isset( $assets[$uri] ) ) $revision = substr( md5( $assets[$uri] ), 0, 7 );
    }
    return $revision;
}

/**
 * Returns the actual revision string for use as a query variable.
 *
 * @param  string $uri      The relative URI of the file (which is used to
 *                          display a hash specific for that file)
 * @param  string $revision The string of a desired custom revision (if not the
 *                          revision hash of the file itself)
 * @return string           The revision string which can be used as a query var
 */
function get_revision_string( $uri = '', $revision = '' ) {
    if ( $revision ) return $revision;

    if ( $uri ) {
        if ( $revision = get_commit_hash( $uri ) ) {
            return $revision;
        }
    }
    $revision = get_revision_hash();
    return $revision ?: get_file_hash( $uri );
}

/**
 * Returns the revision query that can be used directly after a URI.
 *
 * @param  string $uri      The relative URI of the file (which is used to
 *                          display a hash specific for that file)
 * @param  string $revision The string of a desired custom revision (if not the
 *                          revision hash of the file itself)
 * @return string           The revision query string which can be appended to
 *                          a URI
 */
function get_revision_query( $uri = '', $revision = '' ) {
    return '?v=' . get_revision_string( $uri, $revision );
}

/**
 * Gets the build URI for a particular file.
 *
 * @param  string $uri The URI of the file (relative to the theme)
 * @return string      The full build path of the URI entered (with query
 *                     strings prepended if necessary)
 */
function get_build_uri( $uri ) {
    return get_build_directory_uri() . $uri . get_revision_query( $uri );
}

/**
 * Returns the general build directory URI.
 *
 * This will usually be the relative ``/build` directory from the theme but, if
 * we're in production and a CDN constant is defined in the `wp-config.php` file
 * such as:
 *
 *     define( 'WP_CDN', 'https://a1b2c3d4e5f6g7.cloudfront.net/build' );
 *
 * then this will be returned instead.
 *
 * @return string The build directory
 */
function get_build_directory_uri() {
   if ( 'production' === get_environment() && defined( 'WP_CDN' ) ) return WP_CDN;
   return get_template_directory_uri() . '/build';
}

/**
 * Returns the absolute directory for partials for the theme
 *
 * @return string The directory for all partials
 */
function get_partials_directory_uri() {
    return get_template_directory() . '/templates/partials';
}

/**
 * Returns the absolute directory for flexible content for the theme
 *
 * @return string The directory for all partials
 */
function get_flexible_content_directory_uri() {
    return get_template_directory() . '/templates/flexible';
}

/** 1.4 Theme Setup */
function theme_scripts() {
    wp_enqueue_script( 'fonts', '//fast.fonts.net/jsapi/cc1bfd02-cbed-4a48-93d2-d7c420db9cb6.js', [], false, false );
//     wp_enqueue_script( 'googlemapAPI', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCBEW6Ww788OVzLqg3uyePEvJeDq5qSJqo', [], false, false );
    wp_enqueue_style( 'style', get_build_directory_uri() . '/css/style.css', [],
        get_revision_string( '/build/css/style.css' ) );
    wp_enqueue_script( 'vendor', get_build_directory_uri() . '/js/vendor.js', [],
        '', true );
    wp_enqueue_style( 'fontAwesome', 'https://use.fontawesome.com/releases/v5.0.9/css/all.css', [], false, false );
    wp_enqueue_script( 'global', get_build_directory_uri() . '/js/global.js', [],
        get_revision_string( '/build/js/global.js' ), true );
}
add_action( 'wp_enqueue_scripts', 'theme_scripts' );

add_filter( 'allow_dev_auto_core_updates', '__return_true' );
add_filter( 'allow_minor_auto_core_updates', '__return_true' );
add_filter( 'auto_update_plugin', '__return_true' );
add_filter( 'auto_update_theme', '__return_true' );

function location_category_taxonomy() {

    $labels = [
        'name' => __( 'Categories' ),
        'singular_name' => __( 'Category' ),
        'search_items' =>  __( 'Search Categories' ),
        'all_items' => __( 'All Categories' ),
        'parent_item' => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item' => __( 'Edit Category' ),
        'update_item' => __( 'Update Category' ),
        'add_new_item' => __( 'Add New Category' ),
        'new_item_name' => __( 'New Category Name' ),
        'menu_name' => __( 'Categories' ),
    ];

    register_taxonomy( 'location_categories', 'locations', [
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
    ]);
}

function locations_post_type() {
    $labels = [
        'name'                => __( 'Locations' ),
        'singular_name'       => __( 'Location' ),
        'menu_name'           => __( 'Locations' ),
        'all_items'           => __( 'All Locations' ),
        'view_item'           => __( 'View Location'),
        'add_new_item'        => __( 'Add New Location'),
        'add_new'             => __( 'Add New' ),
        'edit_item'           => __( 'Edit Location' ),
        'update_item'         => __( 'Update Location' ),
        'search_items'        => __( 'Search Locations' ),
    ];

    $args = [
        'label'               => __( 'locations' ),
        'description'         => __( 'Restaurant Locations' ),
        'labels'              => $labels,
        'supports'            => [ 'title', 'comments', 'revisions' ],
        'taxonomies'          => ['location_categories'],
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 25,
        'menu_icon'           => 'dashicons-location',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'rewrite'             => [
            'slug'  =>  'locations',
            'with_front' => false,
        ],
    ];

    register_post_type( 'locations', $args );
}

function menus_post_type() {
    $labels = [
        'name'                => __( 'Menus' ),
        'singular_name'       => __( 'Menu' ),
        'menu_name'           => __( 'Menus' ),
        'all_items'           => __( 'All Menus' ),
        'view_item'           => __( 'View Menu'),
        'add_new_item'        => __( 'Add New Menu'),
        'add_new'             => __( 'Add New' ),
        'edit_item'           => __( 'Edit Menu' ),
        'update_item'         => __( 'Update Menu' ),
        'search_items'        => __( 'Search Menu' ),
    ];

    $args = [
        'label'               => __( 'menus' ),
        'description'         => __( 'Location based menus' ),
        'labels'              => $labels,
        'supports'            => [ 'title', 'comments', 'revisions' ],
        'taxonomies'          => [],
        'hierarchical'        => false,
        'public'              => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => false,
        'show_in_admin_bar'   => true,
        'menu_position'       => 25,
        'menu_icon'           => 'dashicons-editor-alignleft',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'capability_type'     => 'page',
    ];

    register_post_type( 'menus', $args );
}

function offers_post_type() {
    $labels = [
        'name'                => __( 'Offers' ),
        'singular_name'       => __( 'Offer' ),
        'menu_name'           => __( 'Offers' ),
        'all_items'           => __( 'All Offers' ),
        'view_item'           => __( 'View Offer'),
        'add_new_item'        => __( 'Add New Offer'),
        'add_new'             => __( 'Add New' ),
        'edit_item'           => __( 'Edit Offer' ),
        'update_item'         => __( 'Update Offer' ),
        'search_items'        => __( 'Search Offers' ),
    ];

    $args = [
        'label'               => __( 'Offers' ),
        'description'         => __( 'Restaurant Offers' ),
        'labels'              => $labels,
        'supports'            => [ 'title', 'comments', 'revisions' ],
        'taxonomies'          => ['offers_categories'],
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 25,
        'menu_icon'           => 'dashicons-money',
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'rewrite'             => [
            'slug'  =>  'offers',
            'with_front' => false,
        ],
    ];

    register_post_type( 'offers', $args );
}

add_action( 'init', 'location_category_taxonomy', 0);
add_action( 'init', 'locations_post_type', 0 );
add_action( 'init', 'offers_post_type', 0 );
add_action( 'init', 'menus_post_type', 0 );

function add_query_vars_filter( $vars ) {
  return array_merge( $vars, get_menu_query_vars() );
}

add_filter( 'query_vars', 'add_query_vars_filter' );

/** 1.5 ACF export */

$acf_export_file = __DIR__ . '/acf/export.php';
if ( file_exists( $acf_export_file ) ) include $acf_export_file;

/** 1.6 Register menus */

/**
 * Registers all custom menus for Wordpress
 * @author Alex Shortt <alex@hexdigital.com>
 */
function register_my_menus() {
    register_nav_menus([
        'top-menu' => __( 'Top level menu' ),
        'footer-menu' => __( 'Footer menu' ),
        'sidebar-menu-slot-one' => __( 'Sidebar menu slot one' ),
        'sidebar-menu-slot-two' => __( 'Sidebar menu slot two' ),
    ]);
}
add_action( 'init', 'register_my_menus' );

/** 1.7 Get menus for templates */

/**
 * Remove wordpress menu args from template files
 * @param  string $location The location of wp_menu
 * @author Alex Shortt <alex@hexdigital.com>
 */
function get_wp_menu( $location, $container_classes = '', $list_classes = '' ) {
    $args = [
        'theme_location' => $location,
        'container_class' => $container_classes,
        'menu_class' => $list_classes
    ];
    wp_nav_menu( $args );
}

/** 1.7 Page type */

/**
 * Return type of Aubaine page
 *
 * @return string - Page type
 * @author Alex Shortt <alex@hexdigital.com>
 * @author Jamie Warburton <jamie@hexdigital.com>
 * @todo Refactor this method as more pages now required image heros
 */
function get_page_type() {
    if ( is_front_page() ) return 'home';
    if ( is_page( 'events' ) ||
         is_page_template( 'templates/seasonal.php' ) ||
         is_singular( 'locations' ) ) return 'no-hero';
    if ( is_singular( 'post' ) ) return 'single';
    return 'default';
}

/** 1.8 Component class modifiers */

/**
 * Some components require modifying classnames based on the page type.
 * This function returns that classname based on component param and
 * page type
 *
 * @param  string $component The name of component
 * @return string            Modifying classname
 * @author Alex Shortt <alex@hexdigital.com>
 */
function component_modifier($component) {
    $page_type = get_page_type();
    if ( $page_type === 'default' ) return;
    return ' c-' . $component . '--' . $page_type;
}

/**
 * Add relevent modifying classes to the navbar
 * based on current page type
 * @return string Modifying classname
 */
function nav_modifier() {
    $classes = component_modifier('navbar');
    $page_type = get_page_type();
    $template_type = get_page_template_slug();

    if ( $page_type === 'home' ) {
        $classes .= ' c-navbar--up';
    }

    if ( 'templates/subscribe.php' === $template_type ) {
        $classes .= ' c-navbar--menu c-navbar--peek u-bg-color--green-dark';
    }
    return $classes;
}

/** 1.9 Hero section */

/**
 * Display hero based on page type
 * @author Alex Shortt <alex@hexdigital.com>
 */
function include_hero() {
    $template_type = get_page_template_slug();

    if ( get_page_template_slug() !== 'templates/subscribe.php' && file_exists( get_partials_directory_uri() . '/heroes/hero-' . get_page_type() . '.php' ) ) {
        include get_partials_directory_uri() . '/heroes/hero-' . get_page_type() . '.php';
    }
}

function hero_title() {
    if ( is_404() ) return '404 - Page not found';
    if ( is_home() ) return 'Blog';
    return get_the_title();
}

/**
 * Directs users to Opentable external page (new tab)
 * with params to pre populate the Opentable form
 * @author Alex Shortt <alex@hexdigital.com>
 */
function redirect_to_booking() {

    $location = $_POST['booking-location'];

    $has_ref = strstr( $location, '?restref' );

    $covers = $_POST['booking-covers'];
    $date = ( isset( $_POST['booking-date'] ) ) ? date( 'Y-m-d', strtotime( $_POST['booking-date'] ) ) : date( 'Y-m-d' );
    $time = ( isset( $_POST['booking-time'] ) ) ? $_POST['booking-time'] : '19:00';

    $covers_delimiter = ( $has_ref ) ? '&' : '?';

    wp_redirect(
        $location .
        $covers_delimiter . 'covers=' . $covers .
        '&datetime=' . $date .
        'T' . $time
    );
    exit;
}

function redirect_to_booking_offers() {

    $location = $_POST['booking-location'];

    $has_ref = strstr( $location, '?restref' );

    $covers = $_POST['booking-covers'];
    $date = ( isset( $_POST['booking-date'] ) ) ? date( 'Y-m-d', strtotime( $_POST['booking-date'] ) ) : date( 'Y-m-d' );
    $time = ( isset( $_POST['booking-time'] ) ) ? $_POST['booking-time'] : '19:00';

    $covers_delimiter = ( $has_ref ) ? '&' : '?';

    wp_redirect(
        $location .
        $covers_delimiter . 'covers=' . $covers .
        '&datetime=' . $date .
        'T' . $time
    );
    exit;
}

/**
 * Directs users to Design my night external page (new tab)
 * with params to pre populate the enquiry form
 * @return null
 */
function redirect_to_event_enquiries() {

    $base_url = 'http://www.designmynight.com/book';
    $date = ( isset( $_POST['enquiry-date'] ) ) ? date( 'Y-m-d', strtotime( $_POST['enquiry-date'] ) ) : date( 'Y-m-d' );
    $time = ( isset( $_POST['enquiry-time'] ) ) ? $_POST['enquiry-time'] : '19:00';
    wp_redirect(
        $base_url .
        '?venue_id=' . $_POST['enquiry-venue'] .
        '&num_people=' . $_POST['enquiry-size'] .
        '&type=' . $_POST['enquiry-type'] .
        '&time=' . $time .
        '&date=' . $date
    );
    exit;
}

/**
 * Return the page id based on the page slug
 * @param  string $slug Slug of page ID requested
 * @return int          ID of the requested page
 */
function get_page_id( $slug ) {
    switch ( $slug ) {
        case 'our-restaurants':
            return 9;
        case 'our-menus':
            return 11;
        case 'book-a-table':
            return 13;
        case 'events':
            return 15;
        case 'contact-us':
            return 21;
        case 'careers':
            return 23;
    }
}

/**
 * Returns the href of each social platform
 * @author Alex Shortt <alex@hexdigital.com>
 * @param  string $platform The social platform for required href
 * @return string           Social platform href of requested platform
 */
function get_social_link( $platform ) {
    $hrefs = [
        'facebook' => 'https://www.facebook.com/aubainerestaurants/',
        'instagram' => 'https://www.instagram.com/aubaineuk/',
        'twitter' => 'https://twitter.com/AubaineUK',
        'pinterest' => 'https://uk.pinterest.com/aubaine/',
    ];
    return $hrefs[ $platform ];
}

/**
 * Returns social sharing url
 * @author Mike Joda <mike@hexdigital.com>
 * @param  string $title  The pre-populated tweet to be shared
 * @param  string $handle The twitter handle to attribute tweet to (without @ sign)
 * @param  string $url    The url of content to be shared on Twitter
 * @return string         Relative social network sharing url
 */
function get_share_link( $type, $url, $title = '', $handle = '' ) {
    switch ( $type ) {
        case 'facebook':
            return 'https://www.facebook.com/sharer.php?u=' . $url;
        case 'twitter':
            return 'https://twitter.com/intent/tweet?text=' . $title . ' via @' . $handle . ' -&url=' . $url;
    }
}

function my_acf_google_map_api( $api ){
	$api['key'] = 'AIzaSyCBEW6Ww788OVzLqg3uyePEvJeDq5qSJqo';
	return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

function remove_wpseo(){
    if (is_page(7451) || is_page(11)) {
        global $wpseo_front;
            if(defined($wpseo_front)){
                remove_action('wp_head',array($wpseo_front,'head'),1);
            }
            else {
              $wp_thing = WPSEO_Frontend::get_instance();
              remove_action('wp_head',array($wp_thing,'head'),1);
            }
    }
}
add_action('template_redirect','remove_wpseo');


if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

}