function Events() {
    var self = this;
    /**
     * Initialise the object
     */
    function init() {
        instantiateObjects();
        addHandlers();
    }
    /**
     * Set up the initial classes to use across the events object
     */
    function instantiateObjects() {
        self.eventsDropdown = 'js-events-dropdown';
        self.eventsDescriptions = 'js-location-event-descriptions';
        self.eventsDetails = 'js-location-event-details';
        self.eventsInitialMessage = 'js-events-initial-message';
        self.swiper = 'js-carousel--locations-block';
        self.swiperItem = 'swiper-slide';
    }
    /**
     * By default, all location event content is hidden. This removes that
     * hiding class when user requests to view event content of location
     */
    function showEventsContent($section, location_id) {
        $section
            .children()
                .addClass('u-hide');
        $section
            .find('[data-location="' + location_id + '"]')
                .removeClass('u-hide');
    }
    /**
     * An initial message is shown on load to guide the user to select a
     * location from the dropdown. This hides that message
     */
    function hideInitialMessage() {
        $('.' + self.eventsInitialMessage).addClass('u-hide');
    }
    /**
     * If a carousel has been added to the template then if
     * available, slide carousel to relevent location events image
     */
    function slideToLocationImage(location_id) {
        var swiper = $('.' + self.swiper);
        if (!swiper.length) return;

        var slideIndex = swiper
            .find('.' + self.swiperItem + '[data-location="' + location_id + '"]')
                .index();

        if (slideIndex < 0) return;

        window.locationsBlockSwiper.slideTo(slideIndex);
    }
    /**
     * Bind event handlers across the objects to handle interaction with events
     */
    function addHandlers() {
        $('.' + self.eventsDropdown).on('change', function(event) {
            var locationId = $(this).val();
            hideInitialMessage();
            showEventsContent($('.' + self.eventsDescriptions), locationId);
            showEventsContent($('.' + self.eventsDetails), locationId);
            slideToLocationImage(locationId);
        });
    }
    init();
}
