<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Hex Digital
 * @subpackage Aubaine
 * @since 2017
 */

get_header() ?>

    <div class="o-wrapper">
        <div class="u-margin-top-huge u-margin-bottom-huge u-text-center">
            <a class="c-btn c-btn--primary" href="/">Return home</a>
        </div>
    </div>

<?php get_footer(); ?>
