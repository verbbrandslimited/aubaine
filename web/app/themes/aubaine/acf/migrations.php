<?php

$migrations = new Migrations;

$migrations->addFieldGroup( 'Dynamic Content', [
        [
            [
                'param' => 'page_template',
                'operator' => '==',
                'value' => 'templates/homepage.php',
            ]
        ],
        [
            [
                'param' => 'page_template',
                'operator' => '==',
                'value' => 'templates/seasonal.php',
            ]
        ],
    ], [
        'menu_order' => 1,
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
    ] )
    ->addField( 'flexible_content', 'Flexible Content Blocks', [
        'button_label' => 'Add Block'
    ] )
        ->addLayout( 'content_blocks', 'Content Blocks' )
            ->addSubField( 'text', 'Block ID', 0, [
                'instructions' => 'Add a hypen separated identifier to allow links across the page and site to direct users to this block.',
            ] )
            ->addSubField( 'checkbox', 'Reversed', 0, [
                'choices' => [
                    1 => 'Reversed',
                ],
                'instructions' => 'Tick to have Panel One appear on the right, and Two and Three on the left. Has no effect on Single Panels.',
                'layout' => 'horizontal',
                'required' => 0,
                'wrapper' => [
                    'width' => 50,
                ],
            ] )

            ->addSubField( 'repeater', 'Panels', 0, [
                'button_label' => 'Add Panel',
                'collapsed' => 'field_0c8389e',
                'instructions' => 'Add up to three panels to display in this block',
                'layout' => 'block',
                'max' => 3,
            ] )
                ->addSubField( 'message', 'Background Settings --', 1 )
                ->addSubField( 'select', 'Background', 1, [
                    'choices' => [
                        'none' => 'None',
                        'colour' => 'Background Colour',
                        'decoration' => 'Background Colour and Decoration',
                        'motif' => 'Background Colour and Motif',
                        'image' => 'Background Image',
                    ],
                    'default_value' => [
                        'none' => 'none',
                    ],
                    'instructions' => 'Choose the background for the Panel',
                    'required' => 1,
                ] )
                ->addSubField( 'select', 'Background Colour', 1, [
                    'required' => 1,
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'field_62217f2',
                                'operator' => '==',
                                'value' => 'colour',
                            ],
                        ],
                        [
                            [
                                'field' => 'field_62217f2',
                                'operator' => '==',
                                'value' => 'decoration',
                            ],
                        ],
                        [
                            [
                                'field' => 'field_62217f2',
                                'operator' => '==',
                                'value' => 'motif',
                            ],
                        ],
                    ],
                    'wrapper' => [
                        'width' => 50,
                    ],
                    'choices' => [
                        'pink-dark' => 'Dark Pink',
                        'blue-dark' => 'Dark Blue',
                        'green' => 'Green',
                        'yellow' => 'Yellow',
                        'white' => 'White',
                        'purple' => 'Purple',
                    ],
                    'default_value' => [
                        'white',
                    ],
                ] )
                ->addSubField( 'image', 'Background Image', 1, [
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'field_62217f2',
                                'operator' => '==',
                                'value' => 'image',
                            ],
                        ],
                    ],
                    'preview_size' => 'thumbnail',
                    'required' => 1,
                    'return_format' => 'url',
                ] )
                ->addSubField( 'message', 'Content Settings --', 1 )
                ->addSubField( 'select', 'Content Position', 1, [
                    'choices' => [
                        'center' => 'Centered',
                        'bottom' => 'Bottom',
                    ],
                    'default_value' => [
                        'center' => 'center',
                    ],
                    'instructions' => 'Select where you\'d like the content to appear in the panel',
                    'required' => 1,
                    'wrapper' => [
                        'width' => 50,
                    ],
                ] )
                ->addSubField( 'select', 'Content Type', 1, [
                    'choices' => [
                        'text' => 'Text',
                        'newsletter' => 'Newsletter Signup',
                    ],
                    'default_value' => [
                        'text' => 'text',
                    ],
                    'instructions' => 'Select what filler content you\'d like in the Panel',
                    'required' => 1,
                    'wrapper' => [
                        'width' => 50,
                    ],
                ] )
                ->addSubField( 'message', 'Panel Content --', 1 )
                ->addSubField( 'text', 'Panel Title', 1, [
                    'instructions' => 'The title to display in the panel.',
                    'placeholder' => 'Modern France',
                ] )
                ->addSubField( 'checkbox', 'Panel Title First Line Arched', 1, [
                    'choices' => [
                        1 => 'Arched',
                    ],
                    'default_value' => [
                        0 => 0,
                    ],
                    'instructions' => 'If enabled, the first title line will become arched.',
                ] )
                ->addSubField( 'text', 'Panel Title Second Line', 1, [
                    'instructions' => 'If required, add a second line to the title. This will also add a X separator',
                    'placeholder' => 'London',
                ] )
                ->addSubField( 'wysiwyg', 'Panel Text', 1, [
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'field_27cc38b',
                                'operator' => '==',
                                'value' => 'text',
                            ],
                        ],
                    ],
                    'default_value' => '',
                    'media_upload' => 0,
                    'tabs' => 'all',
                    'toolbar' => 'basic',
                ] )
                ->addSubField( 'text', 'Newsletter Text', 1, [
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'field_27cc38b',
                                'operator' => '==',
                                'value' => 'newsletter',
                            ],
                        ],
                    ],
                    'default_value' => 'Enter Email for our latest News & Offers',
                    'instructions' => 'The text to display inside the Newsletter input',
                ] )
                ->addSubField( 'repeater', 'Panel Buttons', 1, [
                    'button_label' => 'Add Button',
                    'layout' => 'table',
                ] )
                    ->addSubField( 'text', 'URL', 2, [
                        'placeholder' => '/our-menus/',
                        'required' => 1,
                    ] )
                    ->addSubField( 'text', 'Text', 2, [
                        'placeholder' => 'View Menus',
                        'required' => 1,
                    ] )

        ->addLayout( 'offers_block', 'Offers block' )
            ->addSubField( 'text', 'Block ID', 0, [
                'instructions' => 'Add a hypen separated identifier to allow links across the page and site to direct users to this block.',
            ] )
            ->addSubField( 'text', 'Offers block title', 0 )
            ->addSubField( 'repeater', 'Offer Items', 0, [
                'button_label' => 'Add Offer',
                'instructions' => 'Add up to three offers to display in this block',
                'layout' => 'block',
                'max' => 3,
            ] )
                ->addSubField( 'text', 'Offer Title', 1, [
                    'instructions' => 'The title to display in the offer.',
                    'placeholder' => 'Offer one',
                ] )
                ->addSubField( 'wysiwyg', 'Offer Description', 1, [
                    'default_value' => '',
                    'media_upload' => 0,
                    'toolbar' => 'basic',
                ] )
                ->addSubField( 'repeater', 'Offer buttons', 1, [
                    'button_label' => 'Add Button',
                    'layout' => 'table',
                    'max' => 2,
                ] )
                    ->addSubField( 'text', 'URL', 2, [
                        'placeholder' => '/our-menus/',
                        'required' => 1,
                    ] )
                    ->addSubField( 'text', 'Text', 2, [
                        'placeholder' => 'View Menus',
                        'required' => 1,
                    ] )
                ->addSubField( 'repeater', 'Offer Secondary buttons', 1, [
                    'button_label' => 'Add Button',
                    'layout' => 'table',
                    'max' => 2,
                ] )
                    ->addSubField( 'text', 'Secondary URL', 2, [
                        'placeholder' => '#terms',
                        'required' => 1,
                    ] )
                    ->addSubField( 'text', 'Secondary Text', 2, [
                        'placeholder' => 'Terms & Conditions',
                        'required' => 1,
                    ] )

        ->addLayout( 'locations_block', 'Locations block' )
            ->addSubField( 'text', 'Block ID', 0, [
                'instructions' => 'Add a hypen separated identifier to allow links across the page and site to direct users to this block.',
            ] )
            ->addSubField( 'text', 'Locations block title', 0 )
            ->addSubField( 'wysiwyg', 'Locations block description', 0, [
                'default_value' => '',
                'media_upload' => 0,
                'toolbar' => 'basic',
            ] )

        ->addLayout( 'wysiwyg_block', 'WYSIWYG Block' )
            ->addSubField( 'text', 'Block ID', 0, [
                'instructions' => 'Add a hypen separated identifier to allow links across the page and site to direct users to this block.',
            ] )
            ->addSubField( 'wysiwyg', 'WYSIWYG Content', 0 );


$migrations->addFieldGroup( 'Home Hero', [
        'page_template',
        '==',
        'templates/homepage.php'
    ], [
        'menu_order' => 0,
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
    ] )
    ->addField( 'tab', 'Hero Section', [
        'placement' => 'left',
    ])
    ->addField( 'repeater', 'Hero carousel', [
        'layout' => 'block',
        'button_label' => 'Add item',
    ] )
        ->addSubField ( 'image', 'Background image' )
        ->addSubField ( 'text', 'First line text' )
        ->addSubField ( 'text', 'Second line text' )
        ->addSubField ( 'text', 'Third line text' );

// Commented out until 'position' bug is fixed -- see acf-migrations github issues
// https://github.com/hex-digital/acf-migrations/issues/7
// $migrations->addFieldGroup( 'Menu Order', [
//         'post_type',
//         '==',
//         'menus',
//     ], [
//         'position' => 'side',
//     ] )
//     ->addField( 'number', 'Menu Position', [
//         'instructions' => 'Higher number shows first from left. Default 0. Same numbers sorted alphabetically by title.',
//         'required' => 1,
//     ] );

$migrations->addFieldGroup( 'Menus', [
        'post_type',
        '==',
        'menus',
    ] )
    ->addField( 'repeater', 'Menus', [
        'button_label' => 'Add Menu',
        'collapsed' => 'field_8f97eca',
        'layout' => 'block',
    ] )
        ->addSubField( 'text', 'Name', 0, [
            'required' => 1,
            'placeholder_text' => 'Menu name',
        ] )
        ->addSubField( 'repeater', 'Menu Headings', 0, [
            'button_label' => 'Add Heading',
            'collapsed' => 'field_e135625',
            'layout' => 'block',
            'min' => 1,
            'required' => 1,
        ] )
            ->addSubField( 'text', 'Heading', 1, [
                'placeholder_text' => 'Heading Text',
            ] )
            ->addSubField( 'checkbox', 'Heading Arch', 1, [
                'choices' => [
                    1 => 'Arched',
                ],
                'default_value' => [
                    0 => 0,
                ],
                'instructions' => 'If enabled, heading will be arched.',
            ] )
            ->addSubField( 'text', 'Sub Heading', 1, [
                'instructions' => 'Optional small text underneath a menu heading',
                'placeholder_text' => '',
            ] )
            ->addSubField( 'repeater', 'Menu Items', 1, [
                'button_label' => 'Add Item',
                'collapsed' => 'field_9d5ace6',
                'layout' => 'row',
                'min' => 1,
                'required' => 1,
            ] )
                ->addSubField( 'text', 'Title', 2, [
                    'required' => 1,
                ] )
                ->addSubField( 'text', 'Description', 2 )
                ->addSubField( 'text', 'Price', 2 )
                ->addSubField( 'checkbox', 'Filters', 2, [
                    'choices' => [
                        'vegetarian' => 'Vegetarian',
                        'vegan' => 'Vegan',
                        'gluten-free' => 'Gluten Free',
                        'crustacean-free' => 'Crustacean Free',
                        'nut-free' => 'Nut Free',
                        'dairy-free' => 'Dairy Free',
                    ],
                    'layout' => 'vertical',
                    'required' => 0,
                ] );

$migrations->addFieldGroup( 'Locations', [
        'post_type',
        '==',
        'locations',
    ] )
    ->addField( 'tab', 'Location Header', [
        'placement' => 'left',
    ] )
    ->addField( 'wysiwyg', 'Header Content', [
        'media_upload' => '0',
        'tabs' => 'visual',
        'toolbar' => 'basic',
    ] )
    ->addField( 'repeater', 'Header Links', [
        'button_label' => 'Add Link',
        'instructions' => 'You can link to other pages here - just enter a URL and button text.',
        'layout' => 'row',
    ] )
        ->addSubField( 'text', 'URL', 0, [
            'placeholder' => '/our-menus/',
            'required' => 1,
        ] )
        ->addSubField( 'text', 'Text', 0, [
            'placeholder' => 'View Menus',
            'required' => 1,
        ] )
    ->addField( 'image', 'Header Background', [
        'return_format' => 'url',
        'preview_size' => 'medium',
    ] )

    ->addField( 'tab', 'Book a Table', [
        'placement' => 'left',
    ] )
    ->addField( 'checkbox', 'Book Table Enabled', [
        'choices' => [
            1 => 'Enabled',
        ],
        'default_value' => [
            1 => 1,
        ],
        'instructions' => 'If disabled, will display a custom message and not show the booking widget.',
        'layout' => 'horizontal',
    ] )
    ->addField( 'text', 'Book Table Disabled Text', [
        'conditional_logic' => [
            [
                [
                    'field' => 'field_717f53a',
                    'operator' => '!=',
                    'value' => '1',
                ],
            ],
        ],
        'instructions' => 'Displayed in the Book a Table block when disabled',
    ] )
    ->addField( 'url', 'URL Prefix', [
        'conditional_logic' => [
            [
                [
                    'field' => 'field_717f53a',
                    'operator' => '==',
                    'value' => '1',
                ],
            ],
        ],
        'instructions' => 'Enter the URL you want to prepend to the booking query parameters',
        'placeholder' => 'https://www.opentable.co.uk/r/aubaine-broadgate-reservations-london',
    ] )

    ->addField( 'tab', 'Gift card', [
        'placement' => 'left',
    ] )
    ->addField( 'true_false', 'Gift Card Enabled', [
        'label' => 'Enable Gift card section',
        'default_value' => 0
    ] )
    ->addField( 'text', 'Gift Card Text' )
    ->addField( 'url', 'Gift Card URL' )

    ->addField( 'tab', 'Capacity', [
        'placement' => 'left',
    ] )
    ->addField( 'checkbox', 'Capacity Enabled', [
        'choices' => [
            1 => 'Enabled',
        ],
        'default_value' => [
            1 => 1,
        ],
        'instructions' => 'If disabled, will display a custom message and not show the location capacity information.',
        'layout' => 'horizontal',
    ] )
    ->addField( 'message', 'About capacity section', [
        'message' => 'Content added in this section will show on the Events page'
    ] )
    ->addField( 'text', 'Capacity Disabled Text', [
        'conditional_logic' => [
            [
                [
                    'field' => 'field_38f15fb',
                    'operator' => '!=',
                    'value' => '1',
                ],
            ],
        ],
        'instructions' => 'Displayed in the Capacity block when disabled',
    ] )
    ->addField( 'repeater', 'Capacity venue areas', [
        'button_label' => 'Add area',
        'instructions' => 'Add capacity information about areas of the location.',
        'layout' => 'row',
        'conditional_logic' => [
            [
                [
                    'field' => 'field_38f15fb',
                    'operator' => '==',
                    'value' => '1',
                ],
            ],
        ],
    ] )
        ->addSubField( 'text', 'Venue area', 0, [
            'instructions' => 'Description of an area within the venue.',
            'placeholder' => 'Private dining room'
        ] )
        ->addSubField( 'text', 'Capacity information', 0, [
            'instructions' => 'Capacity of area.',
            'placeholder' => 'Seated 100  |  Standing 120'
        ] )

    ->addField( 'tab', 'Opening Hours', [
        'placement' => 'left',
    ] )
    ->addField( 'checkbox', 'Opening Hours Enabled', [
        'choices' => [
            1 => 'Enabled',
        ],
        'default_value' => [
            1 => 1,
        ],
        'instructions' => 'If disabled, will display a custom message instead.',
        'layout' => 'horizontal',
    ] )
    ->addField( 'text', 'Opening Hours Disabled Text', [
        'conditional_logic' => [
            [
                [
                    'field' => 'field_2e3ddaf',
                    'operator' => '!=',
                    'value' => '1',
                ],
            ],
        ],
        'instructions' => 'Displayed in the Opening Hours block when disabled',
    ] )
    ->addField( 'repeater', 'Opening Times', [
        'button_label' => 'Add Row',
        'conditional_logic' => [
            [
                [
                    'field' => 'field_2e3ddaf',
                    'operator' => '==',
                    'value' => '1',
                ],
            ],
        ],
        'instructions' => 'Opening hours are displayed in a list of rows.',
        'layout' => 'table',
    ] )
        ->addSubField( 'text', 'Day', 0, [
            'placeholder' => 'Mon-Fri',
        ] )
        ->addSubField( 'text', 'Time', 0, [
            'placeholder' => '7am-10.30pm',
        ] )

    ->addField( 'tab', 'Contact', [
        'placement' => 'left',
    ] )
    ->addField( 'checkbox', 'Contact Enabled', [
        'choices' => [
            1 => 'Enabled',
        ],
        'default_value' => [
            1 => 1,
        ],
        'instructions' => 'If disabled, will display a custom message instead',
        'layout' => 'horizontal',
    ] )
    ->addField( 'text', 'Contact Disabled Text', [
        'conditional_logic' => [
            [
                [
                    'field' => 'field_5a1d513',
                    'operator' => '!=',
                    'value' => '1',
                ],
            ],
        ],
        'instructions' => 'Displayed in the Contact block when disabled',
    ] )
    ->addField( 'repeater', 'Contact Items', [
        'button_label' => 'Add Row',
        'conditional_logic' => [
            [
                [
                    'field' => 'field_5a1d513',
                    'operator' => '==',
                    'value' => '1',
                ],
            ],
        ],
        'instructions' => 'Contact information will be displayed in a list of rows.',
        'layout' => 'table',
    ] )
        ->addSubField( 'select', 'Type', 0, [
            'allow_null' => 0,
            'choices' => [
                'address' => 'Address',
                'phone' => 'Phone',
                'email' => 'Email',
                'nearest-tube' => 'Nearest Tube',
                'events-phone' => 'Events Phone',
                'events-email' => 'Events Email',
            ],
            'default_value' => [
                'address' => 'Address',
            ],
            'required' => '1',
            'wrapper' => [
                'width' => 35,
            ],
        ] )
        ->addSubField( 'select', 'Icon', 0, [
            'allow_null' => 0,
            'choices' => [
                'location-marker' => 'Location Marker',
                'email' => 'Email',
                'telephone' => 'Telephone',
                'letter' => 'Letter',
                'underground' => 'Underground',
            ],
            'default_value' => [
                'location' => 'location',
            ],
            'required' => '1',
            'wrapper' => [
                'width' => 35,
            ],
        ] )
        ->addSubField( 'text', 'Text', 0, [
            'placeholder' => '020 7368 0955',
            'required' => '1',
        ] )
    ->addField( 'repeater', 'Extra Links', [
        'button_label' => 'Add Link',
        'conditional_logic' => [
            [
                [
                    'field' => 'field_5a1d513',
                    'operator' => '==',
                    'value' => '1',
                ],
            ],
        ],
        'instructions' => 'You can link to external sources such as Google Maps here - just enter a URL and button text.',
        'layout' => 'row',
    ] )
        ->addSubField( 'text', 'URL', 0, [
            'placeholder' => 'https://www.google.co.uk/maps/search/Aubaine+Broadgate+EC2M+1QS',
            'required' => 1,
        ] )
        ->addSubField( 'text', 'Text', 0, [
            'placeholder' => 'Google Maps',
            'required' => 1,
        ] )

    ->addField( 'tab', 'Menus Tab', [
        'placement' => 'left',
    ] )
    ->addField( 'checkbox', 'Menus Enabled', [
        'choices' => [
            1 => 'Enabled',
        ],
        'default_value' => [
            1 => 1,
        ],
        'instructions' => 'If disabled, will display a custom message instead',
        'layout' => 'horizontal',
    ] )
    ->addField( 'text', 'Menus Disabled Text', [
        'conditional_logic' => [
            [
                [
                    'field' => 'field_9de47b8',
                    'operator' => '!=',
                    'value' => '1',
                ],
            ],
        ],
        'instructions' => 'Displayed in the Menus block when disabled',
    ] )
    ->addField( 'wysiwyg', 'Menus Text', [
        'conditional_logic' => [
            [
                [
                    'field' => 'field_9de47b8',
                    'operator' => '==',
                    'value' => '1',
                ],
            ],
        ],
        'media_upload' => '0',
        'tabs' => 'visual',
        'toolbar' => 'basic',
    ] )
    ->addField( 'repeater', 'Menus', [
        'button_label' => 'Add Menu',
        'conditional_logic' => [
            [
                [
                    'field' => 'field_9de47b8',
                    'operator' => '==',
                    'value' => '1',
                ],
            ],
        ],
        'instructions' => 'You can link to specific menus (or anything) here - just enter a URL and button text.',
        'layout' => 'table',
    ] )
        ->addSubField( 'text', 'URL', 0, [
            'placeholder' => '/our-menus/general-locations/dinner',
            'required' => 1,
        ] )
        ->addSubField( 'text', 'Text', 0, [
            'placeholder' => 'Dinner',
            'required' => 1,
            'wrapper' => [
                'width' => 35,
            ],
        ] )

    ->addField( 'tab', 'Delivery Tab', [
        'placement' => 'left',
    ] )
    ->addField( 'checkbox', 'Delivery Enabled', [
        'choices' => [
            1 => 'Enabled',
        ],
        'default_value' => [
            1 => 1,
        ],
        'instructions' => 'If disabled, will display a custom message instead',
        'layout' => 'horizontal',
    ] )
    ->addField( 'text', 'Delivery Disabled Text', [
        'conditional_logic' => [
            [
                [
                    'field' => 'field_9feb6b5',
                    'operator' => '!=',
                    'value' => '1',
                ],
            ],
        ],
        'instructions' => 'Displayed in the Delivery block when disabled',
    ] )
    ->addField( 'wysiwyg', 'Delivery Text', [
        'conditional_logic' => [
            [
                [
                    'field' => 'field_9feb6b5',
                    'operator' => '==',
                    'value' => '1',
                ],
            ],
        ],
        'media_upload' => '0',
        'tabs' => 'visual',
        'toolbar' => 'basic',
    ] )
    ->addField( 'repeater', 'Delivery Links', [
        'button_label' => 'Add Link',
        'conditional_logic' => [
            [
                [
                    'field' => 'field_9feb6b5',
                    'operator' => '==',
                    'value' => '1',
                ],
            ],
        ],
        'instructions' => 'You can link to specific delivery methods here - just enter a URL and button text.',
        'layout' => 'row',
    ] )
        ->addSubField( 'text', 'URL', 0, [
            'placeholder' => 'https://deliveroo.co.uk/',
            'required' => 1,
        ] )
        ->addSubField( 'select', 'Type', 0, [
            'choices' => [
                'deliveroo' => 'Deliveroo',
            ],
            'default_value' => [
                'deliveroo' => 'Deliveroo',
            ],
            'required' => 1,
        ] )
        ->addSubField( 'text', 'Text', 0, [
            'placeholder' => 'Deliveroo',
            'required' => 1,
        ] )

    ->addField( 'tab', 'Gallery', [
        'placement' => 'left',
    ] )
    ->addField( 'checkbox', 'Gallery Enabled', [
        'choices' => [
            1 => 'Enabled',
        ],
        'default_value' => [
            1 => 1,
        ],
        'instructions' => 'If disabled, will display a custom message instead',
        'layout' => 'horizontal',
    ] )
    ->addField( 'gallery', 'Gallery Images', [
        'conditional_logic' => [
            [
                [
                    'field' => 'field_cc1ebc0',
                    'operator' => '==',
                    'value' => '1',
                ],
            ],
        ],
        'instructions' => 'Upload images to display in the pages gallery',
        'library' => 'all',
        'preview_size' => 'thumbnail',
    ] )

    ->addField( 'tab', 'Newsletter', [
        'placement' => 'left',
    ] )
    ->addField( 'checkbox', 'Newsletter Signup', [
        'choices' => [
            1 => 'Enabled',
        ],
        'default_value' => [
            1 => 1,
        ],
        'instructions' => 'If disabled, will not display Our Newsletter section or signup form',
        'layout' => 'horizontal',
    ] )

    ->addField( 'tab', 'Events', [
        'placement' => 'left',
    ] )
    ->addField( 'message', 'About events section', [
        'message' => 'Content added in this section will also show on the Events page.'
    ] )
    ->addField( 'checkbox', 'Events Enabled', [
        'choices' => [
            1 => 'Enabled',
        ],
        'default_value' => [
            1 => 1,
        ],
        'instructions' => 'If disabled, will display a custom message instead',
        'layout' => 'horizontal',
    ] )
    ->addField( 'wysiwyg', 'Events information', [
        'conditional_logic' => [
            [
                [
                    'field' => 'field_c2910b5', // events_enabled
                    'operator' => '==',
                    'value' => '1',
                ],
            ],
        ],
        'instructions' => 'Information about the venue.'
    ] )
    ->addField( 'text', 'Design my night venue ID', [
        'conditional_logic' => [
            [
                [
                    'field' => 'field_c2910b5', // events_enabled
                    'operator' => '==',
                    'value' => '1',
                ],
            ],
        ],
        'instructions' => 'Design my night venue ID.'
    ] )

    ->addField( 'tab', 'Locations Block', [
        'placement' => 'left',
    ] )
    ->addField( 'image', 'Locations Block Image', [
        'instructions' => 'Add a image to be used in the locations block.',
        'preview_size' => 'thumbnail',
    ] );

$migrations->addFieldGroup( 'Events', [
        'page_template',
        '==',
        'templates/events.php',
    ] )
    ->addField( 'tab', 'Events Header', [
        'placement' => 'left',
    ] )
    ->addField( 'wysiwyg', 'Header Content', [
        'media_upload' => '0',
        'tabs' => 'visual',
        'toolbar' => 'basic',
    ] )
    ->addField( 'repeater', 'Header Links', [
        'button_label' => 'Add Link',
        'instructions' => 'You can link to other pages here - just enter a URL and button text.',
        'layout' => 'row',
    ] )
        ->addSubField( 'text', 'URL', 0, [
            'placeholder' => '/our-menus/',
            'required' => 1,
        ] )
        ->addSubField( 'text', 'Text', 0, [
            'placeholder' => 'View Menus',
            'required' => 1,
        ] )
    ->addField( 'image', 'Header Background', [
        'return_format' => 'url',
        'preview_size' => 'medium',
    ] )

    ->addField( 'tab', 'Events Gallery', [
        'placement' => 'left',
    ] )
    ->addField( 'checkbox', 'Gallery Enabled', [
        'choices' => [
            1 => 'Enabled',
        ],
        'default_value' => [
            1 => 1,
        ],
        'instructions' => 'If disabled, will display a custom message instead',
        'layout' => 'horizontal',
    ] )
    ->addField( 'gallery', 'Gallery Images', [
        'conditional_logic' => [
            [
                [
                    'field' => 'field_bf1d31c',
                    'operator' => '==',
                    'value' => '1',
                ],
            ],
        ],
        'instructions' => 'Upload images to display in the pages gallery',
        'library' => 'all',
        'preview_size' => 'thumbnail',
    ] )

    ->addField( 'tab', 'Events Food Menus', [
        'placement' => 'left',
    ] )
    ->addField( 'file', 'Events Menu', [
        'instructions' => 'Sample menu for events.'
    ] );

$migrations->addFieldGroup( 'Basic Template Pages', [
        'page_template',
        '==',
        'templates/basic.php',
    ], [
        'menu_order' => 1,
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
    ] )
    ->addField( 'select', 'Background Colour', [
        'instructions' => 'Select a background colour.',
        'choices' => [
            'white-dark' => 'Light Grey',
            'white' => 'White',
        ],
        'default_value' => [
            'white' => 'White',
        ],
    ] );

$migrations->addFieldGroup( 'Blog posts', [
        'post_type',
        '==',
        'post',
    ] )
    ->addField( 'tab', 'Blog post hero', [
        'placement' => 'left',
    ] )
    ->addField( 'select', 'Blog post hero colour', [
        'choices' => [
            'pink-dark' => 'Pink',
            'blue-dark' => 'Blue',
            'green-dark' => 'Green',
            'grey' => 'Grey',
        ],
        'default_value' => [
            'grey' => 'Grey',
        ],
    ] )
    ->addField( 'tab', 'Blog post tile imagery', [
        'placement' => 'left',
    ] )
    ->addField( 'image', 'Tile image square', [
        'return_format' => 'url',
        'preview_size' => 'medium',
    ] )
    ->addField( 'image', 'Tile image thin', [
        'return_format' => 'url',
        'preview_size' => 'medium',
    ] )

    ->addField( 'tab', 'Blog post content', [
        'placement' => 'left',
    ] )
    ->addField( 'flexible_content', 'Flexible Blog Blocks', [
        'button_label' => 'Add Block'
    ] )
    ->addLayout( 'blog_image_block', 'Blog image' )
        ->addSubField( 'image', 'Full image', 0, [
            'preview_size' => 'thumbnail',
        ] )
    ->addLayout( 'blog_text_block', 'Blog text' )
        ->addSubField( 'wysiwyg', 'Text', 0 )
    ->addLayout( 'blog_links_block', 'Blog links' )
        ->addSubField( 'repeater', 'Links', 0, [
            'button_label' => 'Add Link',
            'layout' => 'row',
        ] )
            ->addSubField( 'text', 'URL', 1, [
                'required' => 1,
            ] )
            ->addSubField( 'text', 'Text', 1, [
                'required' => 1,
            ] );

$migrations->addFieldGroup( 'Menus page', [
        'page_template',
        '==',
        'templates/our-menus.php',
    ], [
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
    ] )

    ->addField( 'tab', 'Sticky Booking Tab', [
        'placement' => 'left',
    ] )
    ->addField( 'checkbox', 'Booking tab enabled', [
        'choices' => [
            1 => 'Enabled',
        ],
        'default_value' => [
            1 => 1,
        ],
        'instructions' => 'Untick to remove sticky booking tab',
        'layout' => 'horizontal',
    ] )
    ->addField( 'text', 'Booking Tab Copy', [
        'instructions' => 'This text will be displayed on the sticky booking tab. If nothing is written here, it will default to "See something you like?"',
        'placeholder' => 'See something you like?',
    ] )

    ->addField( 'tab', 'Private Dining', [
        'placement' => 'left',
    ] )
    ->addField( 'text', 'Menus Private Dining Title', [
        'instructions' => 'Please add section title.',
        'placeholder' => 'Private Dining',
    ] )
    ->addField( 'wysiwyg', 'Menus Private Dining Text', [
        'media_upload' => 0,
        'tabs' => 'all',
        'toolbar' => 'basic',
    ] )
    ->addField( 'repeater', 'Menus Private Dining Links', [
        'button_label' => 'Add Link',
        'instructions' => 'You can link to other pages here - just enter a URL and button text.',
        'layout' => 'row',
    ] )
        ->addSubField( 'text', 'URL', 0, [
            'placeholder' => '/our-menus/',
            'required' => 1,
        ] )
        ->addSubField( 'text', 'Text', 0, [
            'placeholder' => 'View Menus',
            'required' => 1,
        ] );

$migrations->addFieldGroup( 'Seasonal', [
        'page_template',
        '==',
        'templates/seasonal.php',
    ] )
    ->addField( 'tab', 'Seasonal Header', [
        'placement' => 'left',
    ] )
    ->addField( 'wysiwyg', 'Header Content', [
        'media_upload' => '0',
        'tabs' => 'visual',
        'toolbar' => 'basic',
    ] )
    ->addField( 'repeater', 'Header Links', [
        'button_label' => 'Add Link',
        'instructions' => 'You can link to other pages here - just enter a URL and button text.',
        'layout' => 'row',
    ] )
        ->addSubField( 'text', 'URL', 0, [
            'placeholder' => '/our-menus/',
            'required' => 1,
        ] )
        ->addSubField( 'text', 'Text', 0, [
            'placeholder' => 'View Menus',
            'required' => 1,
        ] )
    ->addField( 'image', 'Header Background', [
        'return_format' => 'url',
        'preview_size' => 'medium',
    ] )
    ->addField( 'select', 'Header Content Background Colour', [
        'choices' => [
            'grey' => 'Grey - Default',
            'pink-dark' => 'Dark Pink',
            'purple' => 'Purple',
            'blue-dark' => 'Dark Blue',
            'green' => 'Green',
            'yellow' => 'Yellow',
        ],
        'default_value' => [
            'grey',
        ],
    ] )
    ->addField( 'tab', 'Seasonal Menus', [
        'placement' => 'left',
    ] )
    ->addField( 'file', 'Seasonal Menu', [
        'return_format' => 'url',
    ] );
