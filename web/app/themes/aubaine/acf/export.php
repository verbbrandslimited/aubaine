<?php

function acf_migrations_add_local_field_groups() {

    acf_add_local_field_group( [
        "key" => "group_3c4e0a6",
        "title" => "Dynamic Content",
        "fields" => [
            [
                "key" => "field_9eba4bf",
                "label" => "Flexible Content Blocks",
                "name" => "flexible_content_blocks",
                "type" => "flexible_content",
                "button_label" => "Add Block",
                "layouts" => [
                    [
                        "key" => "d16ca9c",
                        "name" => "content_blocks",
                        "label" => "Content Blocks",
                        "display" => "block",
                        "sub_fields" => [
                            [
                                "key" => "field_40eb5a4",
                                "label" => "Block ID",
                                "name" => "block_id",
                                "type" => "text",
                                "instructions" => "Add a hyphen separated identifier to allow links across the page and site to direct users to this block."
                            ],
                            [
                                "key" => "field_782d530",
                                "label" => "Reversed",
                                "name" => "reversed",
                                "type" => "checkbox",
                                "choices" => [
                                    1 => "Reversed"
                                ],
                                "instructions" => "Tick to have Panel One appear on the right, and Two and Three on the left. Has no effect on Single Panels.",
                                "layout" => "horizontal",
                                "required" => 0,
                                "wrapper" => [
                                    "width" => 50
                                ]
                            ],
                            [
                                "key" => "field_d5e6e10",
                                "label" => "Panels",
                                "name" => "panels",
                                "type" => "repeater",
                                "button_label" => "Add Panel",
                                "collapsed" => "field_0c8389e",
                                "instructions" => "Add up to three panels to display in this block",
                                "layout" => "block",
                                "max" => 3,
                                "sub_fields" => [
                                    [
                                        "key" => "field_c3b7262",
                                        "label" => "Background Settings   ",
                                        "name" => "background_settings___",
                                        "type" => "message"
                                    ],
                                    [
                                        "key" => "field_62217f2",
                                        "label" => "Background",
                                        "name" => "background",
                                        "type" => "select",
                                        "choices" => [
                                            "none" => "None",
                                            "colour" => "Background Colour",
                                            "decoration" => "Background Colour and Decoration",
                                            "motif" => "Background Colour and Motif",
                                            "image" => "Background Image"
                                        ],
                                        "default_value" => [
                                            "none" => "none"
                                        ],
                                        "instructions" => "Choose the background for the Panel",
                                        "required" => 1
                                    ],
                                    [
                                        "key" => "field_942bb88",
                                        "label" => "Background Colour",
                                        "name" => "background_colour",
                                        "type" => "select",
                                        "required" => 1,
                                        "conditional_logic" => [
                                            [
                                                [
                                                    "field" => "field_62217f2",
                                                    "operator" => "==",
                                                    "value" => "colour"
                                                ]
                                            ],
                                            [
                                                [
                                                    "field" => "field_62217f2",
                                                    "operator" => "==",
                                                    "value" => "decoration"
                                                ]
                                            ],
                                            [
                                                [
                                                    "field" => "field_62217f2",
                                                    "operator" => "==",
                                                    "value" => "motif"
                                                ]
                                            ]
                                        ],
                                        "wrapper" => [
                                            "width" => 50
                                        ],
                                        "choices" => [
                                            "pink-dark" => "Dark Pink",
                                            "blue-dark" => "Dark Blue",
                                            "green" => "Green",
                                            "yellow" => "Yellow",
                                            "white" => "White",
                                            "purple" => "Purple"
                                        ],
                                        "default_value" => [
                                            "white"
                                        ]
                                    ],
                                    [
                                        "key" => "field_70a0ca3",
                                        "label" => "Background Image",
                                        "name" => "background_image",
                                        "type" => "image",
                                        "conditional_logic" => [
                                            [
                                                [
                                                    "field" => "field_62217f2",
                                                    "operator" => "==",
                                                    "value" => "image"
                                                ]
                                            ]
                                        ],
                                        "preview_size" => "thumbnail",
                                        "required" => 1,
                                        "return_format" => "url"
                                    ],
                                    [
                                        "key" => "field_cf89830",
                                        "label" => "Content Settings   ",
                                        "name" => "content_settings___",
                                        "type" => "message"
                                    ],
                                    [
                                        "key" => "field_3d52ac7",
                                        "label" => "Content Position",
                                        "name" => "content_position",
                                        "type" => "select",
                                        "choices" => [
                                            "center" => "Centered",
                                            "bottom" => "Bottom"
                                        ],
                                        "default_value" => [
                                            "center" => "center"
                                        ],
                                        "instructions" => "Select where you'd like the content to appear in the panel",
                                        "required" => 1,
                                        "wrapper" => [
                                            "width" => 50
                                        ]
                                    ],
                                    [
                                        "key" => "field_27cc38b",
                                        "label" => "Content Type",
                                        "name" => "content_type",
                                        "type" => "select",
                                        "choices" => [
                                            "text" => "Text",
                                            "newsletter" => "Newsletter Signup"
                                        ],
                                        "default_value" => [
                                            "text" => "text"
                                        ],
                                        "instructions" => "Select what filler content you'd like in the Panel",
                                        "required" => 1,
                                        "wrapper" => [
                                            "width" => 50
                                        ]
                                    ],
                                    [
                                        "key" => "field_bd5d1f3",
                                        "label" => "Panel Content   ",
                                        "name" => "panel_content___",
                                        "type" => "message"
                                    ],
                                    [
                                        "key" => "field_0c8389e",
                                        "label" => "Panel Title",
                                        "name" => "panel_title",
                                        "type" => "text",
                                        "instructions" => "The title to display in the panel.",
                                        "placeholder" => "Modern France"
                                    ],
                                    [
                                        "key" => "field_b8b17da",
                                        "label" => "Panel Title First Line Arched",
                                        "name" => "panel_title_first_line_arched",
                                        "type" => "checkbox",
                                        "choices" => [
                                            1 => "Arched"
                                        ],
                                        "default_value" => [
                                            0
                                        ],
                                        "instructions" => "If enabled, the first title line will become arched."
                                    ],
                                    [
                                        "key" => "field_0db9685",
                                        "label" => "Panel Title Second Line",
                                        "name" => "panel_title_second_line",
                                        "type" => "text",
                                        "instructions" => "If required, add a second line to the title. This will also add a X separator",
                                        "placeholder" => "London"
                                    ],
                                    [
                                        "key" => "field_a47a515",
                                        "label" => "Panel Text",
                                        "name" => "panel_text",
                                        "type" => "wysiwyg",
                                        "conditional_logic" => [
                                            [
                                                [
                                                    "field" => "field_27cc38b",
                                                    "operator" => "==",
                                                    "value" => "text"
                                                ]
                                            ]
                                        ],
                                        "default_value" => "",
                                        "media_upload" => 0,
                                        "tabs" => "all",
                                        "toolbar" => "basic"
                                    ],
                                    [
                                        "key" => "field_27cd129",
                                        "label" => "Newsletter Text",
                                        "name" => "newsletter_text",
                                        "type" => "text",
                                        "conditional_logic" => [
                                            [
                                                [
                                                    "field" => "field_27cc38b",
                                                    "operator" => "==",
                                                    "value" => "newsletter"
                                                ]
                                            ]
                                        ],
                                        "default_value" => "Enter Email for our latest News & Offers",
                                        "instructions" => "The text to display inside the Newsletter input"
                                    ],
                                    [
                                        "key" => "field_d176519",
                                        "label" => "Panel Buttons",
                                        "name" => "panel_buttons",
                                        "type" => "repeater",
                                        "button_label" => "Add Button",
                                        "layout" => "table",
                                        "sub_fields" => [
                                            [
                                                "key" => "field_2a3db80",
                                                "label" => "URL",
                                                "name" => "url",
                                                "type" => "text",
                                                "placeholder" => "/our-menus/",
                                                "required" => 1
                                            ],
                                            [
                                                "key" => "field_6575aa8",
                                                "label" => "Text",
                                                "name" => "text",
                                                "type" => "text",
                                                "placeholder" => "View Menus",
                                                "required" => 1
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        "key" => "2c84e59",
                        "name" => "offers_block",
                        "label" => "Offers Block",
                        "display" => "block",
                        "sub_fields" => [
                            [
                                "key" => "field_2758bce",
                                "label" => "Block ID",
                                "name" => "block_id",
                                "type" => "text",
                                "instructions" => "Add a hypen separated identifier to allow links across the page and site to direct users to this block."
                            ],
                            [
                                "key" => "field_d4ca87c",
                                "label" => "Offers Block Title",
                                "name" => "offers_block_title",
                                "type" => "text"
                            ],
                            [
                                "key" => "field_90877e5",
                                "label" => "Offer Items",
                                "name" => "offer_items",
                                "type" => "repeater",
                                "button_label" => "Add Offer",
                                "instructions" => "Add up to three offers to display in this block",
                                "layout" => "block",
                                "max" => 3,
                                "sub_fields" => [
                                    [
                                        "key" => "field_7f035ec",
                                        "label" => "Offer Title",
                                        "name" => "offer_title",
                                        "type" => "text",
                                        "instructions" => "The title to display in the offer.",
                                        "placeholder" => "Offer one"
                                    ],
                                    [
                                        "key" => "field_209cc3a",
                                        "label" => "Offer Description",
                                        "name" => "offer_description",
                                        "type" => "wysiwyg",
                                        "default_value" => "",
                                        "media_upload" => 0,
                                        "toolbar" => "basic"
                                    ],
                                    [
                                        "key" => "field_72d2c05",
                                        "label" => "Offer Buttons",
                                        "name" => "offer_buttons",
                                        "type" => "repeater",
                                        "button_label" => "Add Button",
                                        "layout" => "table",
                                        "max" => 2,
                                        "sub_fields" => [
                                            [
                                                "key" => "field_2c48488",
                                                "label" => "URL",
                                                "name" => "url",
                                                "type" => "text",
                                                "placeholder" => "/our-menus/",
                                                "required" => 1
                                            ],
                                            [
                                                "key" => "field_d997295",
                                                "label" => "Text",
                                                "name" => "text",
                                                "type" => "text",
                                                "placeholder" => "View Menus",
                                                "required" => 1
                                            ]
                                        ]
                                    ],
                                    [
                                        "key" => "field_286e662",
                                        "label" => "Offer Secondary Buttons",
                                        "name" => "offer_secondary_buttons",
                                        "type" => "repeater",
                                        "button_label" => "Add Button",
                                        "layout" => "table",
                                        "max" => 2,
                                        "sub_fields" => [
                                            [
                                                "key" => "field_994cf19",
                                                "label" => "Secondary URL",
                                                "name" => "secondary_url",
                                                "type" => "text",
                                                "placeholder" => "#terms",
                                                "required" => 1
                                            ],
                                            [
                                                "key" => "field_d619404",
                                                "label" => "Secondary Text",
                                                "name" => "secondary_text",
                                                "type" => "text",
                                                "placeholder" => "Terms & Conditions",
                                                "required" => 1
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        "key" => "b6468bc",
                        "name" => "locations_block",
                        "label" => "Locations Block",
                        "display" => "block",
                        "sub_fields" => [
                            [
                                "key" => "field_1c022a7",
                                "label" => "Block ID",
                                "name" => "block_id",
                                "type" => "text",
                                "instructions" => "Add a hypen separated identifier to allow links across the page and site to direct users to this block."
                            ],
                            [
                                "key" => "field_f592acf",
                                "label" => "Locations Block Title",
                                "name" => "locations_block_title",
                                "type" => "text"
                            ],
                            [
                                "key" => "field_01271eb",
                                "label" => "Locations Block Description",
                                "name" => "locations_block_description",
                                "type" => "wysiwyg",
                                "default_value" => "",
                                "media_upload" => 0,
                                "toolbar" => "basic"
                            ]
                        ]
                    ],
                    [
                        "key" => "03dae38",
                        "name" => "wysiwyg_block",
                        "label" => "Wysiwyg Block",
                        "display" => "block",
                        "sub_fields" => [
                            [
                                "key" => "field_c894ab6",
                                "label" => "Block ID",
                                "name" => "block_id",
                                "type" => "text",
                                "instructions" => "Add a hypen separated identifier to allow links across the page and site to direct users to this block."
                            ],
                            [
                                "key" => "field_75edf4a",
                                "label" => "WYSIWYG Content",
                                "name" => "wysiwyg_content",
                                "type" => "wysiwyg"
                            ]
                        ]
                    ]
                ]
            ]
        ],
        "location" => [
            [
                [
                    "param" => "page_template",
                    "operator" => "==",
                    "value" => "templates/homepage.php"
                ]
            ],
            [
                [
                    "param" => "page_template",
                    "operator" => "==",
                    "value" => "templates/seasonal.php"
                ]
            ]
        ],
        "options" => [
            "position" => "normal",
            "hide_on_screen" => ""
        ],
        "menu_order" => 0,
        "style" => "default",
        "label_placement" => "top",
        "instruction_placement" => "label"
    ] );

    acf_add_local_field_group( [
        "key" => "group_8974563",
        "title" => "Home Hero",
        "fields" => [
            [
                "key" => "field_45f933a",
                "label" => "Hero Section",
                "name" => "hero_section",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_9d27bee",
                "label" => "Hero Carousel",
                "name" => "hero_carousel",
                "type" => "repeater",
                "layout" => "block",
                "button_label" => "Add item",
                "sub_fields" => [
                    [
                        "key" => "field_09ba089",
                        "label" => "Background Image",
                        "name" => "background_image",
                        "type" => "image"
                    ],
                    [
                        "key" => "field_bdb9181",
                        "label" => "First Line Text",
                        "name" => "first_line_text",
                        "type" => "text"
                    ],
                    [
                        "key" => "field_455b602",
                        "label" => "Second Line Text",
                        "name" => "second_line_text",
                        "type" => "text"
                    ],
                    [
                        "key" => "field_7cfe2bc",
                        "label" => "Third Line Text",
                        "name" => "third_line_text",
                        "type" => "text"
                    ]
                ]
            ]
        ],
        "location" => [
            [
                [
                    "param" => "page_template",
                    "operator" => "==",
                    "value" => "templates/homepage.php"
                ]
            ]
        ],
        "options" => [
            "position" => "normal",
            "hide_on_screen" => ""
        ],
        "menu_order" => 0,
        "style" => "default",
        "label_placement" => "top",
        "instruction_placement" => "label"
    ] );

    acf_add_local_field_group( [
        "key" => "group_81ca0b7",
        "title" => "Menus",
        "fields" => [
            [
                "key" => "field_08bb052",
                "label" => "Menus",
                "name" => "menus",
                "type" => "repeater",
                "button_label" => "Add Menu",
                "collapsed" => "field_8f97eca",
                "layout" => "block",
                "sub_fields" => [
                    [
                        "key" => "field_8f97eca",
                        "label" => "Name",
                        "name" => "name",
                        "type" => "text",
                        "required" => 1,
                        "placeholder_text" => "Menu name"
                    ],
                    [
                        "key" => "field_9fdeae7",
                        "label" => "Menu Headings",
                        "name" => "menu_headings",
                        "type" => "repeater",
                        "button_label" => "Add Heading",
                        "collapsed" => "field_e135625",
                        "layout" => "block",
                        "min" => 1,
                        "required" => 1,
                        "sub_fields" => [
                            [
                                "key" => "field_e135625",
                                "label" => "Heading",
                                "name" => "heading",
                                "type" => "text",
                                "placeholder_text" => "Heading Text"
                            ],
                            [
                                "key" => "field_a0a5948",
                                "label" => "Heading Arch",
                                "name" => "heading_arch",
                                "type" => "checkbox",
                                "choices" => [
                                    1 => "Arched"
                                ],
                                "default_value" => [
                                    0
                                ],
                                "instructions" => "If enabled, heading will be arched."
                            ],
                            [
                                "key" => "field_843413a",
                                "label" => "Sub Heading",
                                "name" => "sub_heading",
                                "type" => "text",
                                "instructions" => "Optional small text underneath a menu heading",
                                "placeholder_text" => ""
                            ],
                            [
                                "key" => "field_e318f6f",
                                "label" => "Menu Items",
                                "name" => "menu_items",
                                "type" => "repeater",
                                "button_label" => "Add Item",
                                "collapsed" => "field_9d5ace6",
                                "layout" => "row",
                                "min" => 1,
                                "required" => 1,
                                "sub_fields" => [
                                    [
                                        "key" => "field_9d5ace6",
                                        "label" => "Title",
                                        "name" => "title",
                                        "type" => "text",
                                        "required" => 1
                                    ],
                                    [
                                        "key" => "field_0b2d2e8",
                                        "label" => "Description",
                                        "name" => "description",
                                        "type" => "text"
                                    ],
                                    [
                                        "key" => "field_0193df6",
                                        "label" => "Price",
                                        "name" => "price",
                                        "type" => "text"
                                    ],
                                    [
                                        "key" => "field_9104113",
                                        "label" => "Filters",
                                        "name" => "filters",
                                        "type" => "checkbox",
                                        "choices" => [
                                            "vegetarian" => "Vegetarian",
                                            "vegan" => "Vegan",
                                            "gluten-free" => "Gluten Free",
                                            "crustacean-free" => "Crustacean Free",
                                            "nut-free" => "Nut Free",
                                            "dairy-free" => "Dairy Free"
                                        ],
                                        "layout" => "vertical",
                                        "required" => 0
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
        "location" => [
            [
                [
                    "param" => "post_type",
                    "operator" => "==",
                    "value" => "menus"
                ]
            ]
        ],
        "options" => [
            "position" => "normal",
            "hide_on_screen" => ""
        ],
        "menu_order" => 0
    ] );

    acf_add_local_field_group( [
        "key" => "group_b4c0b6c",
        "title" => "Locations",
        "fields" => [
            [
                "key" => "field_60c87f0",
                "label" => "Location Header",
                "name" => "location_header",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_0c5561c",
                "label" => "Header Content",
                "name" => "header_content",
                "type" => "wysiwyg",
                "media_upload" => "0",
                "tabs" => "visual",
                "toolbar" => "basic"
            ],
            [
                "key" => "field_825f7f8",
                "label" => "Header Links",
                "name" => "header_links",
                "type" => "repeater",
                "button_label" => "Add Link",
                "instructions" => "You can link to other pages here - just enter a URL and button text.",
                "layout" => "row",
                "sub_fields" => [
                    [
                        "key" => "field_4f3c10a",
                        "label" => "URL",
                        "name" => "url",
                        "type" => "text",
                        "placeholder" => "/our-menus/",
                        "required" => 1
                    ],
                    [
                        "key" => "field_6bcff18",
                        "label" => "Text",
                        "name" => "text",
                        "type" => "text",
                        "placeholder" => "View Menus",
                        "required" => 1
                    ]
                ]
            ],
            [
                "key" => "field_f947b7b",
                "label" => "Header Background",
                "name" => "header_background",
                "type" => "image",
                "return_format" => "url",
                "preview_size" => "medium"
            ],
            [
                "key" => "field_jh523969",
                "label" => "Address",
                "name" => "address",
                "type" => "tab",
                "placement" => "left"
            ],
            [
	            'key' => 'field_5ab4d86733418',
				'label' => 'Address',
				'name' => 'address',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => 5,
				'new_lines' => '',
				'readonly' => 0,
				'disabled' => 0,
            ],
            [
                "key" => "field_95ff978",
                "label" => "Book A Table",
                "name" => "book_a_table",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_717f53a",
                "label" => "Book Table Enabled",
                "name" => "book_table_enabled",
                "type" => "checkbox",
                "choices" => [
                    1 => "Enabled"
                ],
                "default_value" => [
                    1 => 1
                ],
                "instructions" => "If disabled, will display a custom message and not show the booking widget.",
                "layout" => "horizontal"
            ],
            [
                "key" => "field_a526255",
                "label" => "Book Table Disabled Text",
                "name" => "book_table_disabled_text",
                "type" => "text",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_717f53a",
                            "operator" => "!=",
                            "value" => "1"
                        ]
                    ]
                ],
                "instructions" => "Displayed in the Book a Table block when disabled"
            ],
            [
                "key" => "field_05acb5b",
                "label" => "URL Prefix",
                "name" => "url_prefix",
                "type" => "url",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_717f53a",
                            "operator" => "==",
                            "value" => "1"
                        ]
                    ]
                ],
                "instructions" => "Enter the URL you want to prepend to the booking query parameters",
                "placeholder" => "https://www.opentable.co.uk/r/aubaine-broadgate-reservations-london"
            ],
            [
                "key" => "field_08d588f",
                "label" => "Gift Card",
                "name" => "gift_card",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_770557d",
                "label" => "Gift Card Enabled",
                "name" => "gift_card_enabled",
                "type" => "true_false",
                "default_value" => 0
            ],
            [
                "key" => "field_e2f53ae",
                "label" => "Gift Card Text",
                "name" => "gift_card_text",
                "type" => "text"
            ],
            [
                "key" => "field_f12bf5c",
                "label" => "Gift Card URL",
                "name" => "gift_card_url",
                "type" => "url"
            ],
            [
                "key" => "field_4ea4d4a",
                "label" => "Capacity",
                "name" => "capacity",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_38f15fb",
                "label" => "Capacity Enabled",
                "name" => "capacity_enabled",
                "type" => "checkbox",
                "choices" => [
                    1 => "Enabled"
                ],
                "default_value" => [
                    1 => 1
                ],
                "instructions" => "If disabled, will display a custom message and not show the location capacity information.",
                "layout" => "horizontal"
            ],
            [
                "key" => "field_fef20c0",
                "label" => "About Capacity Section",
                "name" => "about_capacity_section",
                "type" => "message",
                "message" => "Content added in this section will show on the Events page"
            ],
            [
                "key" => "field_4db3f79",
                "label" => "Capacity Disabled Text",
                "name" => "capacity_disabled_text",
                "type" => "text",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_38f15fb",
                            "operator" => "!=",
                            "value" => "1"
                        ]
                    ]
                ],
                "instructions" => "Displayed in the Capacity block when disabled"
            ],
            [
                "key" => "field_e203715",
                "label" => "Capacity Venue Areas",
                "name" => "capacity_venue_areas",
                "type" => "repeater",
                "button_label" => "Add area",
                "instructions" => "Add capacity information about areas of the location.",
                "layout" => "row",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_38f15fb",
                            "operator" => "==",
                            "value" => "1"
                        ]
                    ]
                ],
                "sub_fields" => [
                    [
                        "key" => "field_652ae42",
                        "label" => "Venue Area",
                        "name" => "venue_area",
                        "type" => "text",
                        "instructions" => "Description of an area within the venue.",
                        "placeholder" => "Private dining room"
                    ],
                    [
                        "key" => "field_669a1f2",
                        "label" => "Capacity Information",
                        "name" => "capacity_information",
                        "type" => "text",
                        "instructions" => "Capacity of area.",
                        "placeholder" => "Seated 100  |  Standing 120"
                    ]
                ]
            ],
            [
                "key" => "field_f513d8a",
                "label" => "Opening Hours",
                "name" => "opening_hours",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_2e3ddaf",
                "label" => "Opening Hours Enabled",
                "name" => "opening_hours_enabled",
                "type" => "checkbox",
                "choices" => [
                    1 => "Enabled"
                ],
                "default_value" => [
                    1 => 1
                ],
                "instructions" => "If disabled, will display a custom message instead.",
                "layout" => "horizontal"
            ],
            [
                "key" => "field_81e3c6a",
                "label" => "Opening Hours Disabled Text",
                "name" => "opening_hours_disabled_text",
                "type" => "text",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_2e3ddaf",
                            "operator" => "!=",
                            "value" => "1"
                        ]
                    ]
                ],
                "instructions" => "Displayed in the Opening Hours block when disabled"
            ],
            [
                "key" => "field_b1c7c4a",
                "label" => "Opening Times",
                "name" => "opening_times",
                "type" => "repeater",
                "button_label" => "Add Row",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_2e3ddaf",
                            "operator" => "==",
                            "value" => "1"
                        ]
                    ]
                ],
                "instructions" => "Opening hours are displayed in a list of rows.",
                "layout" => "table",
                "sub_fields" => [
                    [
                        "key" => "field_083c3e5",
                        "label" => "Day",
                        "name" => "day",
                        "type" => "text",
                        "placeholder" => "Mon-Fri"
                    ],
                    [
                        "key" => "field_572afa1",
                        "label" => "Time",
                        "name" => "time",
                        "type" => "text",
                        "placeholder" => "7am-10.30pm"
                    ]
                ]
            ],
            [
                "key" => "field_e27bbc6",
                "label" => "Contact",
                "name" => "contact",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_5a1d513",
                "label" => "Contact Enabled",
                "name" => "contact_enabled",
                "type" => "checkbox",
                "choices" => [
                    1 => "Enabled"
                ],
                "default_value" => [
                    1 => 1
                ],
                "instructions" => "If disabled, will display a custom message instead",
                "layout" => "horizontal"
            ],
            [
                "key" => "field_481a8eb",
                "label" => "Contact Disabled Text",
                "name" => "contact_disabled_text",
                "type" => "text",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_5a1d513",
                            "operator" => "!=",
                            "value" => "1"
                        ]
                    ]
                ],
                "instructions" => "Displayed in the Contact block when disabled"
            ],
            [
                "key" => "field_32b0444",
                "label" => "Contact Items",
                "name" => "contact_items",
                "type" => "repeater",
                "button_label" => "Add Row",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_5a1d513",
                            "operator" => "==",
                            "value" => "1"
                        ]
                    ]
                ],
                "instructions" => "Contact information will be displayed in a list of rows.",
                "layout" => "table",
                "sub_fields" => [
                    [
                        "key" => "field_41f5eec",
                        "label" => "Type",
                        "name" => "type",
                        "type" => "select",
                        "allow_null" => 0,
                        "choices" => [
                            "address" => "Address",
                            "phone" => "Phone",
                            "email" => "Email",
                            "nearest-tube" => "Nearest Tube",
                            "events-phone" => "Events Phone",
                            "events-email" => "Events Email"
                        ],
                        "default_value" => [
                            "address" => "Address"
                        ],
                        "required" => "1",
                        "wrapper" => [
                            "width" => 35
                        ]
                    ],
                    [
                        "key" => "field_d7a4ebc",
                        "label" => "Icon",
                        "name" => "icon",
                        "type" => "select",
                        "allow_null" => 0,
                        "choices" => [
                            "location-marker" => "Location Marker",
                            "email" => "Email",
                            "telephone" => "Telephone",
                            "letter" => "Letter",
                            "underground" => "Underground"
                        ],
                        "default_value" => [
                            "location" => "location"
                        ],
                        "required" => "1",
                        "wrapper" => [
                            "width" => 35
                        ]
                    ],
                    [
                        "key" => "field_4383a44",
                        "label" => "Text",
                        "name" => "text",
                        "type" => "text",
                        "placeholder" => "020 7368 0955",
                        "required" => "1"
                    ]
                ]
            ],
            [
                "key" => "field_ea4eda4",
                "label" => "Extra Links",
                "name" => "extra_links",
                "type" => "repeater",
                "button_label" => "Add Link",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_5a1d513",
                            "operator" => "==",
                            "value" => "1"
                        ]
                    ]
                ],
                "instructions" => "You can link to external sources such as Google Maps here - just enter a URL and button text.",
                "layout" => "row",
                "sub_fields" => [
                    [
                        "key" => "field_3a462a1",
                        "label" => "URL",
                        "name" => "url",
                        "type" => "text",
                        "placeholder" => "https://www.google.co.uk/maps/search/Aubaine+Broadgate+EC2M+1QS",
                        "required" => 1
                    ],
                    [
                        "key" => "field_73355f4",
                        "label" => "Text",
                        "name" => "text",
                        "type" => "text",
                        "placeholder" => "Google Maps",
                        "required" => 1
                    ]
                ]
            ],
            [
                "key" => "field_53eec4e",
                "label" => "Menus Tab",
                "name" => "menus_tab",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_9de47b8",
                "label" => "Menus Enabled",
                "name" => "menus_enabled",
                "type" => "checkbox",
                "choices" => [
                    1 => "Enabled"
                ],
                "default_value" => [
                    1 => 1
                ],
                "instructions" => "If disabled, will display a custom message instead",
                "layout" => "horizontal"
            ],
            [
                "key" => "field_6b5a566",
                "label" => "Menus Disabled Text",
                "name" => "menus_disabled_text",
                "type" => "text",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_9de47b8",
                            "operator" => "!=",
                            "value" => "1"
                        ]
                    ]
                ],
                "instructions" => "Displayed in the Menus block when disabled"
            ],
            [
                "key" => "field_0863dab",
                "label" => "Menus Text",
                "name" => "menus_text",
                "type" => "wysiwyg",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_9de47b8",
                            "operator" => "==",
                            "value" => "1"
                        ]
                    ]
                ],
                "media_upload" => "0",
                "tabs" => "visual",
                "toolbar" => "basic"
            ],
            [
                "key" => "field_8185152",
                "label" => "Menus",
                "name" => "menus",
                "type" => "repeater",
                "button_label" => "Add Menu",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_9de47b8",
                            "operator" => "==",
                            "value" => "1"
                        ]
                    ]
                ],
                "instructions" => "You can link to specific menus (or anything) here - just enter a URL and button text.",
                "layout" => "table",
                "sub_fields" => [
                    [
                        "key" => "field_b157eff",
                        "label" => "URL",
                        "name" => "url",
                        "type" => "text",
                        "placeholder" => "/our-menus/general-locations/dinner",
                        "required" => 1
                    ],
                    [
                        "key" => "field_21fc545",
                        "label" => "Text",
                        "name" => "text",
                        "type" => "text",
                        "placeholder" => "Dinner",
                        "required" => 1,
                        "wrapper" => [
                            "width" => 35
                        ]
                    ]
                ]
            ],
            [
                "key" => "field_425098e",
                "label" => "Delivery Tab",
                "name" => "delivery_tab",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_9feb6b5",
                "label" => "Delivery Enabled",
                "name" => "delivery_enabled",
                "type" => "checkbox",
                "choices" => [
                    1 => "Enabled"
                ],
                "default_value" => [
                    1 => 1
                ],
                "instructions" => "If disabled, will display a custom message instead",
                "layout" => "horizontal"
            ],
            [
                "key" => "field_16c0db9",
                "label" => "Delivery Disabled Text",
                "name" => "delivery_disabled_text",
                "type" => "text",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_9feb6b5",
                            "operator" => "!=",
                            "value" => "1"
                        ]
                    ]
                ],
                "instructions" => "Displayed in the Delivery block when disabled"
            ],
            [
                "key" => "field_b36de08",
                "label" => "Delivery Text",
                "name" => "delivery_text",
                "type" => "wysiwyg",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_9feb6b5",
                            "operator" => "==",
                            "value" => "1"
                        ]
                    ]
                ],
                "media_upload" => "0",
                "tabs" => "visual",
                "toolbar" => "basic"
            ],
            [
                "key" => "field_9d9c88c",
                "label" => "Delivery Links",
                "name" => "delivery_links",
                "type" => "repeater",
                "button_label" => "Add Link",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_9feb6b5",
                            "operator" => "==",
                            "value" => "1"
                        ]
                    ]
                ],
                "instructions" => "You can link to specific delivery methods here - just enter a URL and button text.",
                "layout" => "row",
                "sub_fields" => [
                    [
                        "key" => "field_5bab961",
                        "label" => "URL",
                        "name" => "url",
                        "type" => "text",
                        "placeholder" => "https://deliveroo.co.uk/",
                        "required" => 1
                    ],
                    [
                        "key" => "field_487384d",
                        "label" => "Type",
                        "name" => "type",
                        "type" => "select",
                        "choices" => [
                            "deliveroo" => "Deliveroo"
                        ],
                        "default_value" => [
                            "deliveroo" => "Deliveroo"
                        ],
                        "required" => 1
                    ],
                    [
                        "key" => "field_561caaf",
                        "label" => "Text",
                        "name" => "text",
                        "type" => "text",
                        "placeholder" => "Deliveroo",
                        "required" => 1
                    ]
                ]
            ],
            [
                "key" => "field_7f7bdee",
                "label" => "Gallery",
                "name" => "gallery",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_cc1ebc0",
                "label" => "Gallery Enabled",
                "name" => "gallery_enabled",
                "type" => "checkbox",
                "choices" => [
                    1 => "Enabled"
                ],
                "default_value" => [
                    1 => 1
                ],
                "instructions" => "If disabled, will display a custom message instead",
                "layout" => "horizontal"
            ],
            [
                "key" => "field_6d7ff31",
                "label" => "Gallery Images",
                "name" => "gallery_images",
                "type" => "gallery",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_cc1ebc0",
                            "operator" => "==",
                            "value" => "1"
                        ]
                    ]
                ],
                "instructions" => "Upload images to display in the pages gallery",
                "library" => "all",
                "preview_size" => "thumbnail"
            ],
            [
                "key" => "field_3ca547a",
                "label" => "Newsletter",
                "name" => "newsletter",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_6403b2f",
                "label" => "Newsletter Signup",
                "name" => "newsletter_signup",
                "type" => "checkbox",
                "choices" => [
                    1 => "Enabled"
                ],
                "default_value" => [
                    1 => 1
                ],
                "instructions" => "If disabled, will not display Our Newsletter section or signup form",
                "layout" => "horizontal"
            ],
            [
                "key" => "field_7a02c4b",
                "label" => "Events",
                "name" => "events",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_bcfced1",
                "label" => "About Events Section",
                "name" => "about_events_section",
                "type" => "message",
                "message" => "Content added in this section will also show on the Events page."
            ],
            [
                "key" => "field_c2910b5",
                "label" => "Events Enabled",
                "name" => "events_enabled",
                "type" => "checkbox",
                "choices" => [
                    1 => "Enabled"
                ],
                "default_value" => [
                    1 => 1
                ],
                "instructions" => "If disabled, will display a custom message instead",
                "layout" => "horizontal"
            ],
            [
                "key" => "field_653e558",
                "label" => "Events Information",
                "name" => "events_information",
                "type" => "wysiwyg",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_c2910b5",
                            "operator" => "==",
                            "value" => "1"
                        ]
                    ]
                ],
                "instructions" => "Information about the venue."
            ],
            [
                "key" => "field_779593d",
                "label" => "Design My Night Venue ID",
                "name" => "design_my_night_venue_id",
                "type" => "text",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_c2910b5",
                            "operator" => "==",
                            "value" => "1"
                        ]
                    ]
                ],
                "instructions" => "Design my night venue ID."
            ],
            [
                "key" => "field_79ebc8a",
                "label" => "Locations Block",
                "name" => "locations_block",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_49a67d9",
                "label" => "Locations Block Image",
                "name" => "locations_block_image",
                "type" => "image",
                "instructions" => "Add a image to be used in the locations block.",
                "preview_size" => "thumbnail"
            ],
            [
                "key" => "field_79ebc8weffea",
                "label" => "Map",
                "name" => "map",
                "type" => "tab",
                "placement" => "left"
            ],
            [
	            "key" => "field_79ebc8a3edf",
                "label" => "Map Location",
                "name" => "map_location",
                "type" => "google_map",
                "instructions" => "Please do not add locations that are outside of the UK."
            ]
        ],
        "location" => [
            [
                [
                    "param" => "post_type",
                    "operator" => "==",
                    "value" => "locations"
                ]
            ]
        ],
        "options" => [
            "position" => "normal",
            "hide_on_screen" => ""
        ],
        "menu_order" => 0
    ] );

    acf_add_local_field_group( [
        "key" => "group_16908b0",
        "title" => "Events",
        "fields" => [
            [
                "key" => "field_41a223d",
                "label" => "Events Header",
                "name" => "events_header",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_b2a5d79",
                "label" => "Header Content",
                "name" => "header_content",
                "type" => "wysiwyg",
                "media_upload" => "0",
                "tabs" => "visual",
                "toolbar" => "basic"
            ],
            [
                "key" => "field_4a001de",
                "label" => "Header Links",
                "name" => "header_links",
                "type" => "repeater",
                "button_label" => "Add Link",
                "instructions" => "You can link to other pages here - just enter a URL and button text.",
                "layout" => "row",
                "sub_fields" => [
                    [
                        "key" => "field_ca907f7",
                        "label" => "URL",
                        "name" => "url",
                        "type" => "text",
                        "placeholder" => "/our-menus/",
                        "required" => 1
                    ],
                    [
                        "key" => "field_a783fe3",
                        "label" => "Text",
                        "name" => "text",
                        "type" => "text",
                        "placeholder" => "View Menus",
                        "required" => 1
                    ]
                ]
            ],
            [
                "key" => "field_46f50a3",
                "label" => "Header Background",
                "name" => "header_background",
                "type" => "image",
                "return_format" => "url",
                "preview_size" => "medium"
            ],
            [
                "key" => "field_36e54c6",
                "label" => "Events Gallery",
                "name" => "events_gallery",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_bf1d31c",
                "label" => "Gallery Enabled",
                "name" => "gallery_enabled",
                "type" => "checkbox",
                "choices" => [
                    1 => "Enabled"
                ],
                "default_value" => [
                    1 => 1
                ],
                "instructions" => "If disabled, will display a custom message instead",
                "layout" => "horizontal"
            ],
            [
                "key" => "field_8296a41",
                "label" => "Gallery Images",
                "name" => "gallery_images",
                "type" => "gallery",
                "conditional_logic" => [
                    [
                        [
                            "field" => "field_bf1d31c",
                            "operator" => "==",
                            "value" => "1"
                        ]
                    ]
                ],
                "instructions" => "Upload images to display in the pages gallery",
                "library" => "all",
                "preview_size" => "thumbnail"
            ],
            [
                "key" => "field_bf39a94",
                "label" => "Events Food Menus",
                "name" => "events_food_menus",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_8925e01",
                "label" => "Events Menu",
                "name" => "events_menu",
                "type" => "file",
                "instructions" => "Sample menu for events."
            ]
        ],
        "location" => [
            [
                [
                    "param" => "page_template",
                    "operator" => "==",
                    "value" => "templates/events.php"
                ]
            ]
        ],
        "options" => [
            "position" => "normal",
            "hide_on_screen" => ""
        ],
        "menu_order" => 0
    ] );

    acf_add_local_field_group( [
        "key" => "group_2861966",
        "title" => "Basic Template Pages",
        "fields" => [
            [
                "key" => "field_53189c9",
                "label" => "Background Colour",
                "name" => "background_colour",
                "type" => "select",
                "instructions" => "Select a background colour.",
                "choices" => [
                    "white-dark" => "Light Grey",
                    "white" => "White"
                ],
                "default_value" => [
                    "white" => "White"
                ]
            ]
        ],
        "location" => [
            [
                [
                    "param" => "page_template",
                    "operator" => "==",
                    "value" => "templates/basic.php"
                ]
            ]
        ],
        "options" => [
            "position" => "normal",
            "hide_on_screen" => ""
        ],
        "menu_order" => 0,
        "style" => "default",
        "label_placement" => "top",
        "instruction_placement" => "label"
    ] );

    acf_add_local_field_group( [
        "key" => "group_674b0ae",
        "title" => "Blog Posts",
        "fields" => [
            [
                "key" => "field_b86222e",
                "label" => "Blog Post Hero",
                "name" => "blog_post_hero",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_5b61ec7",
                "label" => "Blog Post Hero Colour",
                "name" => "blog_post_hero_colour",
                "type" => "select",
                "choices" => [
                    "pink-dark" => "Pink",
                    "blue-dark" => "Blue",
                    "green-dark" => "Green",
                    "grey" => "Grey"
                ],
                "default_value" => [
                    "grey" => "Grey"
                ]
            ],
            [
                "key" => "field_b0ec38d",
                "label" => "Blog Post Tile Imagery",
                "name" => "blog_post_tile_imagery",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_6774347",
                "label" => "Tile Image Square",
                "name" => "tile_image_square",
                "type" => "image",
                "return_format" => "url",
                "preview_size" => "medium"
            ],
            [
                "key" => "field_c8b54d4",
                "label" => "Tile Image Thin",
                "name" => "tile_image_thin",
                "type" => "image",
                "return_format" => "url",
                "preview_size" => "medium"
            ],
            [
                "key" => "field_d75c175",
                "label" => "Blog Post Content",
                "name" => "blog_post_content",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_1647934",
                "label" => "Flexible Blog Blocks",
                "name" => "flexible_blog_blocks",
                "type" => "flexible_content",
                "button_label" => "Add Block",
                "layouts" => [
                    [
                        "key" => "013a55d",
                        "name" => "blog_image_block",
                        "label" => "Blog Image Block",
                        "display" => "block",
                        "sub_fields" => [
                            [
                                "key" => "field_afc1518",
                                "label" => "Full Image",
                                "name" => "full_image",
                                "type" => "image",
                                "preview_size" => "thumbnail"
                            ]
                        ]
                    ],
                    [
                        "key" => "c1acce3",
                        "name" => "blog_text_block",
                        "label" => "Blog Text Block",
                        "display" => "block",
                        "sub_fields" => [
                            [
                                "key" => "field_61151cd",
                                "label" => "Text",
                                "name" => "text",
                                "type" => "wysiwyg"
                            ]
                        ]
                    ],
                    [
                        "key" => "8b1584a",
                        "name" => "blog_links_block",
                        "label" => "Blog Links Block",
                        "display" => "block",
                        "sub_fields" => [
                            [
                                "key" => "field_c934f09",
                                "label" => "Links",
                                "name" => "links",
                                "type" => "repeater",
                                "button_label" => "Add Link",
                                "layout" => "row",
                                "sub_fields" => [
                                    [
                                        "key" => "field_25c9653",
                                        "label" => "URL",
                                        "name" => "url",
                                        "type" => "text",
                                        "required" => 1
                                    ],
                                    [
                                        "key" => "field_e35286e",
                                        "label" => "Text",
                                        "name" => "text",
                                        "type" => "text",
                                        "required" => 1
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
        "location" => [
            [
                [
                    "param" => "post_type",
                    "operator" => "==",
                    "value" => "post"
                ]
            ]
        ],
        "options" => [
            "position" => "normal",
            "hide_on_screen" => ""
        ],
        "menu_order" => 0
    ] );

    acf_add_local_field_group( [
        "key" => "group_0bcf4b5",
        "title" => "Menus Page",
        "fields" => [
            [
                "key" => "field_4749080",
                "label" => "Sticky Booking Tab",
                "name" => "sticky_booking_tab",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_afc024d",
                "label" => "Booking Tab Enabled",
                "name" => "booking_tab_enabled",
                "type" => "checkbox",
                "choices" => [
                    1 => "Enabled"
                ],
                "default_value" => [
                    1 => 1
                ],
                "instructions" => "Untick to remove sticky booking tab",
                "layout" => "horizontal"
            ],
            [
                "key" => "field_9379f05",
                "label" => "Booking Tab Copy",
                "name" => "booking_tab_copy",
                "type" => "text",
                "instructions" => "This text will be displayed on the sticky booking tab. If nothing is written here, it will default to \"See something you like?\"",
                "placeholder" => "See something you like?"
            ],
            [
                "key" => "field_bd74a06",
                "label" => "Private Dining",
                "name" => "private_dining",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_83481c4",
                "label" => "Menus Private Dining Title",
                "name" => "menus_private_dining_title",
                "type" => "text",
                "instructions" => "Please add section title.",
                "placeholder" => "Private Dining"
            ],
            [
                "key" => "field_5c89d64",
                "label" => "Menus Private Dining Text",
                "name" => "menus_private_dining_text",
                "type" => "wysiwyg",
                "media_upload" => 0,
                "tabs" => "all",
                "toolbar" => "basic"
            ],
            [
                "key" => "field_d9d0330",
                "label" => "Menus Private Dining Links",
                "name" => "menus_private_dining_links",
                "type" => "repeater",
                "button_label" => "Add Link",
                "instructions" => "You can link to other pages here - just enter a URL and button text.",
                "layout" => "row",
                "sub_fields" => [
                    [
                        "key" => "field_3e37716",
                        "label" => "URL",
                        "name" => "url",
                        "type" => "text",
                        "placeholder" => "/our-menus/",
                        "required" => 1
                    ],
                    [
                        "key" => "field_2aecc71",
                        "label" => "Text",
                        "name" => "text",
                        "type" => "text",
                        "placeholder" => "View Menus",
                        "required" => 1
                    ]
                ]
            ]
        ],
        "location" => [
            [
                [
                    "param" => "page_template",
                    "operator" => "==",
                    "value" => "templates/our-menus.php"
                ]
            ]
        ],
        "options" => [
            "position" => "normal",
            "hide_on_screen" => ""
        ],
        "menu_order" => 0,
        "style" => "default",
        "label_placement" => "top",
        "instruction_placement" => "label"
    ] );

    acf_add_local_field_group( [
        "key" => "group_11ee057",
        "title" => "Seasonal",
        "fields" => [
            [
                "key" => "field_a20f47e",
                "label" => "Seasonal Header",
                "name" => "seasonal_header",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_07d9395",
                "label" => "Header Content",
                "name" => "header_content",
                "type" => "wysiwyg",
                "media_upload" => "0",
                "tabs" => "visual",
                "toolbar" => "basic"
            ],
            [
                "key" => "field_a589beb",
                "label" => "Header Links",
                "name" => "header_links",
                "type" => "repeater",
                "button_label" => "Add Link",
                "instructions" => "You can link to other pages here - just enter a URL and button text.",
                "layout" => "row",
                "sub_fields" => [
                    [
                        "key" => "field_9d7ee26",
                        "label" => "URL",
                        "name" => "url",
                        "type" => "text",
                        "placeholder" => "/our-menus/",
                        "required" => 1
                    ],
                    [
                        "key" => "field_d4bccd7",
                        "label" => "Text",
                        "name" => "text",
                        "type" => "text",
                        "placeholder" => "View Menus",
                        "required" => 1
                    ]
                ]
            ],
            [
                "key" => "field_902ae8e",
                "label" => "Header Background",
                "name" => "header_background",
                "type" => "image",
                "return_format" => "url",
                "preview_size" => "medium"
            ],
            [
                "key" => "field_ba30bf8",
                "label" => "Header Content Background Colour",
                "name" => "header_content_background_colour",
                "type" => "select",
                "choices" => [
                    "grey" => "Grey - Default",
                    "pink-dark" => "Dark Pink",
                    "purple" => "Purple",
                    "blue-dark" => "Dark Blue",
                    "green" => "Green",
                    "yellow" => "Yellow"
                ],
                "default_value" => [
                    "grey"
                ]
            ],
            [
                "key" => "field_bef8d66",
                "label" => "Seasonal Menus",
                "name" => "seasonal_menus",
                "type" => "tab",
                "placement" => "left"
            ],
            [
                "key" => "field_82301f3",
                "label" => "Seasonal Menu",
                "name" => "seasonal_menu",
                "type" => "file",
                "return_format" => "url"
            ]
        ],
        "location" => [
            [
                [
                    "param" => "page_template",
                    "operator" => "==",
                    "value" => "templates/seasonal.php"
                ]
            ],
            [
                [
                    "param" => "post_type",
                    "operator" => "==",
                    "value" => "offers"
                ]
            ]
        ],
        "options" => [
            "position" => "normal",
            "hide_on_screen" => ""
        ],
        "menu_order" => 0
    ] );

}

add_action( 'acf/init', 'acf_migrations_add_local_field_groups' );
