<?php
/**
 * The template for displaying our singular location pages for
 * location post type
 *
 * @package Hex Digital
 * @subpackage Aubaine
 * @since 2017
 */

$header_background_style = acf_get_background_style( get_field( 'header_background' ) );
$header_content = get_field( 'header_content' );
$header_links = get_field( 'header_links' );

get_header() ?>

    <?php while ( have_posts() ) : the_post() ?>
        <div class="c-detail-page">
            <div class="c-detail-page__header c-detail-header c-detail-header--loading">
                <?php include get_partials_directory_uri() . '/detail-pages/detail-header.php' ?>
            </div>
            <div class="o-section">
                <div class="o-wrapper">
                    <div class="o-flex o-flex--wrap">
                        <div class="c-content-block c-detail-page__block-content">
                            <div class="c-content-block__header c-content-block__header--arc">
                                <span class="u-arc">Book a Table</span>
                            </div>
                            <div class="c-content-block__body">
                                <?php include_restaurant_block( 'book-table' ) ?>
                            </div>
                        </div>
                        <div class="c-content-block c-detail-page__block-content">
                            <div class="c-content-block__header">
                                Opening Hours
                            </div>
                            <div class="c-content-block__body">
                                <?php include_restaurant_block( 'opening-hours' ) ?>
                            </div>
                        </div>
                        <div class="c-content-block c-detail-page__block-content">
                            <div class="c-content-block__header">
                                Contact
                            </div>
                            <div class="c-content-block__body">
                                <?php include_restaurant_block( 'contact' ) ?>
                            </div>
                        </div>
                        <div class="c-content-block c-detail-page__block-content">
                            <div class="c-content-block__header">
                                Menus
                            </div>
                            <div class="c-content-block__body">
                                <?php include_restaurant_block( 'menus' ) ?>
                            </div>
                        </div>
                        <div class="c-content-block c-detail-page__block-content">
                            <div class="c-content-block__header">
                                Delivery
                            </div>
                            <div class="c-content-block__body">
                                <?php include_restaurant_block( 'delivery' ) ?>
                            </div>
                        </div>
                        <div class="c-content-block c-detail-page__block-content">
                            <div class="c-content-block__header">
                                Events
                            </div>
                            <div class="c-content-block__body">
                                <?php include_restaurant_block( 'events' ) ?>
                            </div>
                        </div>
                    </div>
                    <?php if ( $gallery_enabled = (int) acf_single_value_checkbox(
                                                         get_field( 'gallery_enabled' )
                                                     ) ) : ?>
                        <div class="c-content-block">
                            <div class="c-content-block__header u-text-center">
                                Gallery
                            </div>
                            <div class="c-content-block__body c-detail-page__gallery">
                                <?php include_restaurant_block( 'gallery' ) ?>
                            </div>
                        </div>
                    <?php endif ?>
                    <?php if ( $newsletter_signup = (int) acf_single_value_checkbox(
                                                           get_field( 'newsletter_signup' )
                                                       ) ) : ?>
                        <div class="c-content-block c-content-block--no-separator">
                            <div class="c-content-block__body u-text-center">
                                <style>
                                .locations-template-default .o-wrapper .static-newsletter{
                                    text-align: center;
                                }
                                .locations-template-default .o-wrapper .static-newsletter h2{
                                    font-size: 24px;
                                    color: #AE902F;
                                    letter-spacing: 2.61px;
                                    text-transform: uppercase;
                                }
                                .locations-template-default .o-wrapper .static-newsletter p{
                                    color: #000000;
                                    font-size: 16px;
                                }
                                .locations-template-default .o-wrapper .static-newsletter .newsletter-button{
                                    display: inline-block;
                                    width: 230px;
                                    height: 51px;
                                    background-color: #A3BEA7;
                                    color: #FFFFFF;
                                    font-size: 12px;
                                    text-transform: uppercase;
                                    line-height: 51px;
                                }
                                </style>
                                <div class="static-newsletter">
                                    <h2>SIGN-UP TO OUR NEWSLETTER</h2>
                                    <p>Sign up to our email newsletter to be the first to hear about our latest offers, seasonal menus and upcoming events</p>
                                    <a href="<?php echo get_site_url(); ?>/subscribe/"><div class="newsletter-button">Sign Up Now</div></a>
                                </div>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            </div>
            <?php if ( get_field( 'gift_card_enabled' ) ) : ?>
                <div class="o-section u-padding-bottom-none u-padding-top-none">
                    <div class="o-wrapper">
                        <?php $panel = gift_card_panel() ?>
                        <?php include get_partials_directory_uri() . '/panel.php' ?>
                    </div>
                </div>
            <?php endif ?>
        </div>

    <?php endwhile ?>

<?php get_footer() ?>
